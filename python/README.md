<!--
Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU
Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Formation Python 3.x

Ce projet contient un ensemble de documents et exercices pour l'apprentissage du langage de programmation Python.
* [Support de cours](https://gitlab.com/pascal.rapaz/teaching/blob/master/cheatsheet/python.md)
* [Énoncés des exercices](https://gitlab.com/pascal.rapaz/teaching/blob/master/python/exercises.md)
* [Solution des exercices](https://gitlab.com/pascal.rapaz/teaching/tree/master/python/solutions)
