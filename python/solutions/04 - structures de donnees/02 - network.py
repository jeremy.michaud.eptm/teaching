#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

##
# Créez une base de données au format suivant:
#
#    Interface  IP                  status
# 1	 lo         127.0.0.1/8         up
# 2	 eth0       192.168.43.205/24   down
# 3	 eth1       192.168.110.1/24    up
# 4	 wlp2s0     172.16.0.38         up
#
# - Affichez la liste de toutes les interfaces "up"
# - Affichez la liste de toutes les interfaces "down"
##

interfaces = {1: ["lo", "127.0.0.1/8", "up"],
              2: ["eth0", "192.168.43.205/24", "down"],
              3: ["eth1", "192.168.110.1/24", "up"],
              4: ["wlp2s0", "172.16.0.38", "up"],
             }

print("Interfaces up")
for key, value in interfaces.items():
  if "up" in value:
    print(str(key), "\t", "\t".join(value))
  #endIf
#endFor

print("\nInterfaces down")
for key, value in interfaces.items():
  if "down" in value:
    print(str(key), "\t", "\t".join(value))
  #endIf
#endFor
