#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

##
# Écrire une fonction qui reçoit 2 listes en paramètre et retourne une liste contenant tous les éléments uniques
# issus de la fusion des 2 listes.
#
# Exemple:
#   l1 = [1, 2, 3, 3, 4, 5]
#   l2 = [1, 4, 5, 7, 8]
#   res = [1, 2, 3, 4, 5, 7, 8]
##

## Utilisation de 2 objets sets
def unique(a, b):
  s1 = set(a)
  s2 = set(b)

  return list(s1 | s2)
#endDef

## Concatenation des listes pour la création du set
def unique2(a, b):
  s = set(a + b)
  return list(s)
#endDef

l1 = [1, 2, 3, 3, 4, 5]
l2 = [1, 4, 5, 7, 8]

print(unique(l1, l2))
print(unique2(l1, l2))
