#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

##
# Créez les classes `Animaux()` et` Chien()`
# 
# La classe `Animaux()` contiendra la liste des chiens et aura les méthodes et attributs suivants :
#   - attributs
#     * chiens -> liste vide
#   - méthodes
#     * etat() -> parcours les éléments de la liste et, pour chaque chien, appel la méthode `dors()`
# 
# La classe `Chien()` représentera un animal et aura les méthodes et attributs suivants :
#   - attributs
#     * nom
#     * age
#     * race
#     * is_sleeping
#   - méthodes
#     * dors()
#       retourne : "[nom] est en train de dormir !" ou "[nom] ne dors pas !" en fonction de l'état de l'attribut `is_sleeping`
# 
# Une fois les 2 classes créées :
#   - Créez une liste de chiens (minimum 3):
#     mes_chiens = [Chien("Beethoven", 8, "Saint-Bernard", False), ...]
#   - Instanciez la classe animaux avec la liste de chiens
#   - Affichez le status des chiens avec la méthode `etat()`
# 
# Résultat attendu:
#   Beethoven ne dors pas !
#   Leika est en train de dormir !
#   McQueen est en train de dormir !
##

class Animaux:
  chiens = []

  def __init__(self, chiens):
    """Gère une liste d'animaux

    Arguments:
    chiens -- liste de chiens
    """
    self.chiens = chiens
  #endDef

  def etat(self):
    for c in self.chiens:
      print(c.dors())
    #endFor
  #endDef
#endClass

class Chien():

  def __init__(self, nom, age, race, is_sleeping=False):
    """Cree un objet Chien

    Arguments:
    nom -- nom du chien
    age -- age du chien
    race -- race du chien
    is-sleeping -- True si le chien dors, sinon False
    """
    self.__nom = nom
    self.__age = age
    self.__race = race
    self.__is_sleeping = is_sleeping
  #endDef

  def dors(self):
    if (self.__is_sleeping):
      return f"{self.__nom} est en train de dormir !"
    
    return f"{self.__nom} ne dors pas !"
  #endDef

  @property
  def nom(self):
    return self.__nom
  #endDef

  @nom.setter
  def nom(self, n):
    self.__nom = n
  #endDef

  @property
  def age(self):
    return self.__age
  #endDef

  @age.setter
  def age(self, a):
    self.__age = a
  #endDef

  @property
  def race(self):
    return self.__race
  #endDef

  @race.setter
  def race(self, r):
    self.__race = r
  #endDef

  @property
  def is_sleeping(self):
    return self.__is_sleeping
  #endDef

  @is_sleeping.setter
  def is_sleeping(self, s):
    self.__is_sleeping = s
  #endDef

  def __repr__(self):
    return f"Chien(nom='{self.__nom}', age={self.__age:d}, race='{self.__race}', is_sleeping={self.__is_sleeping})"
  #endDef
#endClass

# Creation de la liste de chiens
mes_chiens = [Chien("Beethoven", 8, "Saint-Bernard", False),
              Chien("Leika", 1, "Yorkshire", True), 
              Chien("McQueen", 3, "Bouledogue Français", True),
             ]

# Creation d'une instance d'animaux
mes_animaux = Animaux(mes_chiens)

# Affiche le status des chiens
mes_animaux.etat()