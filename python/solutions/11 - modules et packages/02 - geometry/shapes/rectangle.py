#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

class Rectangle:

  def __init__(self, longueur, largeur):
    """Cree un objet Rectangle

    Arguments:
    longueur -- longueur du cote du rectangle
    largeur -- largeur du cote du rectangle
    """
    self.__longueur = longueur
    self.__largeur = largeur
  #endDef

  @property
  def longueur(self):
    return self.__longueur
  #endDef

  @longueur.setter
  def longueur(self, v):
    self.longueur = v
  #endDef
  
  @property
  def largeur(self):
    return self.__largeur
  #endDef

  @largeur.setter
  def largeur(self, v):
    self.largeur = v
  #endDef
#endClass