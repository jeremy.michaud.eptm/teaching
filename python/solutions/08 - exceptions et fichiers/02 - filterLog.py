#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

##
# Adaptez l'application [DisplayLog](#13---DisplayLog) afin d'afficher uniquement les 
# informations de debug et levez une exception si le fichier n'en contient pas
#
# Bonus :
#   Faites en sorte qu'il n'y ai pas de ligne vides lors de l'affichage
##

has_debug = False

try: 
  with open("execution.log", "r") as f:
    for l in f:
      if 'DEBUG' in l:
        has_debug = True
        print(l[:-1])
      #endIf
    #endFor
  #endWith
except OSError as ose:
  print(ose)
#endTry

if not has_debug:
  raise Exception("Aucune lignes de debug dans le fichier")
#endIf