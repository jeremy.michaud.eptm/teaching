#!/usr/bin/env python
# -*- coding: utf-8 -*-

##
# Copyright (C) 2019-2020 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

##
#  Effectuez une boucle jusqu'a 100 et affichez :
#   - baz pour les multiples de 3
#   - inga pour les multiples de 5 
#   - bazinga pour les multiples de 3 et 5
# 
# ex: 1 2 baz 4 inga baz 7 8 baz inga 11 baz 13 14 bazinga 16 17 baz 19 inga baz 22 23 ...
##

res = ""
for i in range(1, 100):
  if i % 3 == 0 and i % 5 == 0:
    res += "bazinga"
  elif i % 3 == 0:
    res += "baz"
  elif i % 5 == 0:
    res += "inga"
  else:
    res += str(i)
  #endIf

  res += " "
#endFor

print(res)
