<!--
Copyright (C) 2015 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.com)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Cheatsheet VMWare Workstation <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [Réduire la taille d’un vmdk](#réduire-la-taille-dun-vmdk)

## Réduire la taille d’un vmdk

1. Supprimer tous les snapshots
2. Sous GNU/Linux

   ```bash
   sudo btrfs filesystem defragment -r -v /
   sudo swapoff -a
   sudo dd if=/dev/zero of=/0bits bs=1024x1024; rm -f /0bits
   sudo swapon
   ```

<br /><p style='text-align: right;'><sub>&copy; 2015 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.ch)</sub></p>
