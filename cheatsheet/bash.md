<!--
Copyright (C) 2010-2023 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.com)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Cheatsheet Bash <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [Manipulation de fichiers](#manipulation-de-fichiers)
  - [Compression](#compression)
  - [Encodage](#encodage)
- [Gestion des utilisateurs et groupes](#gestion-des-utilisateurs-et-groupes)
- [Gestion des process](#gestion-des-process)
- [Recherche](#recherche)
- [Information système](#information-système)
- [Réseau](#réseau)
- [Historique](#historique)
- [Raccourcis](#raccourcis)
- [Redirection des flux](#redirection-des-flux)
- [Planification de tâche](#planification-de-tâche)
- [Divers](#divers)
- [Références](#références)

## Manipulation de fichiers

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `ls`
  </td>
  <td>

  [*`list`*]
  
  Liste le contenu d'un dossier
  
  ```bash
  # Liste tous les fichiers y compris les fichiers cachés
  ls -alF
    ou      
  ll   
  ```

  </td>
</tr>
<tr>
  <td>

  `cd`
  </td>
  <td>

  [*`change directory`*]

  Change le répertoire courant
  
  ```bash
  # Accède au répertoire `<folder>` en mode absolu ou relatif
  cd <folder>

  # Accède au répertoire `home` de l'utilisateur    
  cd
    ou
  cd ~   
  ```

  </td>
</tr>
<tr>
  <td>

  `pwd`
  </td>
  <td>
  
  [*`print working directory`*]

  Affiche le chemin absolu du répertoire courant
  </td>
</tr>
<tr>
  <td>

  `mkdir <folder>`
  </td>
  <td>

  [*`make directory`*]

  Crée le répertoire `<folder>`

  ```bash
  # Crée le répertoire `<folder>` et ses parents s'ils n'existent pas
  mkdir -p <folder>
  ```

  </td>
</tr>
<tr>
  <td>

  `rm <file/folder>`
  </td>
  <td>
  
  [*`remove`*]

  Supprime le fichier `<file>` ou le `<folder>`

  > :pencil2: ***NOTES***
  >
  > Si le fichier est très volumineux, il sera préférable de le vider au préalable :
  >
  > ```bash
  > > <file>    # vide le contenu du fichier
  > rm <file>   # supprime le fichier
  > ```

  ```bash
  # Force la suppression récursive du répertoire `<folder>`
  rm <folder> -rf
  ```

  </td>
</tr>
<tr>
  <td>

  `cp <src> <dst>`
  </td>
  <td>

  [*`copy`*]
  
  Copie le fichier `<src>` vers la destinations `<dst>`

  ```bash
  # Copie récursivement le dossier `<src>` vers `<dst>` ; crée le dossier `<dst>` s'il n'existe pas
  cp -r <src> <dst>
  ```
  
  </td>
</tr>
<tr>
  <td>

  `mv <src> <dst>`
  </td>
  <td>

  [*`move`*]

  Renomme ou déplace le fichier `<src>` vers `<dst>`
  </td>
</tr>
<tr>
  <td>

  `ln -s <file/folder> <link>`
  </td>
  <td>
  
  [*`link`*]

  Crée un lien symbolique sur le fichier `<file>` ou le dossier `<folder>`
  </td>
</tr>
<tr>
  <td>

  `touch <file>`
  </td>
  <td>
  
  Crée le fichier `<file>` ou mets à jour sa date
  </td>
</tr>
<tr>
  <td>

  `cat <file>`
  </td>
  <td>
  
  Affiche le contenu de `<file>` dans la sortie standard (`stdout`)
  </td>
</tr>
<tr>
  <td>

  `more <file>`
  </td>
  <td>
  
  Affiche le contenu de `<file>` écran par écran
  </td>
</tr>
<tr>
  <td>

  `less <file>`
  </td>
  <td>
  
  Affiche le contenu de `<file>` (plus évolué que `more`)

  ```bash
  # Affiche le contenu de `<file>` en continu
  # Permet de surveiller la fin d'un fichier qui s'agrandit pendant qu'il est lu (similaire à `tail -f`)
  less +F <file>
  ```

  </td>
</tr>
<tr>
  <td>

  `head <file>`
  </td>
  <td>
  
  Affiche les 10 premières lignes du fichier `<file>`
  </td>
</tr>
<tr>
  <td>

  `tail <file>`
  </td>
  <td>
  
  Affiche les 10 dernières lignes du fichier `<file>`

  ```bash
  # Affiche le contenu de `<file>` en continu 
  # Permet de surveiller la fin d'un fichier qui s'agrandit pendant qu'il est lu (similaire à `less +F`)
  tail -f <file>
  ```

  </td>
</tr>
<tr>
  <td>

  `diff <file1> <file2>`
  </td>
  <td>
  
  Affiche les différences entre `<file1>` et `<file2>`

  ```bash
  # Affiche les différences au format `patch`
  diff -u <file1> <file2>

  # Affiche les différences avec un contexte de 5 lignes
  diff -C 5 <file1> <file2>
  ```

  </td>
</tr>
<tr>
  <td>

  `patch`
  </td>
  <td>
  
  Permet d'appliquer un `patch` généré avec diff

  ```bash
  # Crée le fichier <patchfile.patch> avec les différences de <origialFile> et <updatedFile> 
  diff <origialFile> <updatedFile> > <patchfile.patch>

  # Applique le <patchfile.patch> sur le fichier <origialFile> dans le fichier <newFile>
  patch <origialFile> -i <patchfile.patch> -o <newFile>
  ```

  </td>
</tr>
<tr>
  <td>

  `cmp <file1> <file2>`
  </td>
  <td>
  
  Affiche les différences entre 2 fichiers binaires `<file1>` et `<file2>`
  </td>
</tr>
<tr>
  <td>

  `chmod <octal/symbolic> <file/folder>`
  </td>
  <td>
  
  [*`change mode`*]

  Change les permissions sur `<file>` (ou `<folder>`) : lecture `r`, écriture `w` et exécution `x`.

  > :pencil2: ***NOTES***
  >
  > - Pour un fichier, le flag exécution `x` indique que le fichier est exécutable.
  > - Pour un dossier, le flag exécution `x` indique qu'il est possible d'accéder au dossier.

 **Mode `octal`**

 Les permissions sont spécifiées séparément pour les utilisateurs (`u`), groupes (`g`) et autres (`o`).

  ```text
    <user>        <group>        <other>

  4   2   1      4   2   1      4   2   1
  r   w   x      r   w   x      r   w   x
  ```

  Pour calculer les permissions en `octal`, il suffit d'additionner les éléments actifs pour chaque groupe :

  ```text
    <user>        <group>        <other>

  4   2   1      4   2   1      4   2   1
  r   w   -      r   w   x      r   -   x


    4 + 2        4 + 2 + 1        4 + 1
      6              7              5        ==>   chmod 675 <file>
  ```

  **Mode `symbolic`**

  Les permissions sont spécifiées pour les utilisateurs (`u`), les groupes (`g`), les autres (`o`) ou tous (`a`)

  `+` - ajoute un droit

  `-` - supprime un droit

  ```text
  chmod u+rw-x,g+rwx,o+rx-w <file>           ==>   rw-rwxr-x  <file>
  ```

  </td>
</tr>
<tr>
  <td>

  `chown <user>[:group] <file/folder>`
  </td>
  <td>

  [*`change owner`*]
  
  Change le propriétaire et/ou le groupe de `<file>` ou `<folder>`
  </td>
</tr>
<tr>
<tr>
  <td>

  `chgrp <group> <file/folder>`
  </td>
  <td>

  [*`change group`*]
  
  Change le groupe propriétaire de `<file>` ou `<folder>`
  </td>
</tr>
<tr>
  <td>

  `wc`
  </td>
  <td>

  [*`word count`*]
  
  Afficher le nombre d'octets, de mots et de lignes d'un fichier
  </td>
</tr>
<tr>
  <td>

  `sort <file>`
  </td>
  <td>
  
  Trie le contenu de `<file>`
  </td>
</tr>
<tr>
  <td>

  `uniq <sorted_file>`
  </td>
  <td>
  
  Supprime les doublons dans un fichier **trié**
  </td>
</tr>
<tr>
  <td>

  `md5sum <file>`
  </td>
  <td>
  
  Calcul un checksum, sorte « d'empreinte digitale », de `<file>` au format 128 bit
  </td>
</tr>
<tr>
  <td>

  `lsof`
  </td>
  <td>

  [*`list open files`*]
  
  Liste les fichiers actuellement ouvert sur le système

  ```bash
  # Tue le processus en cours d'exécution sur un port
  lsof -t -i:<port> | xargs kill
  ```

  </td>
</tr>
<tr>
  <td>

  `mkfs`
  </td>
  <td>

  [*`make file system`*]
  
  Prépare un système de fichier

  ```bash
  # effectue un formatage rapide ntfs-3g
  mkfs -t ntfs -f -L "LABEL" /dev/sdxx
  ```

  </td>
</tr>
</table>

### Compression

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `zip/unzip`
  </td>
  <td>
  
  Permet de gérer les fichiers compressés au format `.zip`
  
  ```bash
  # Compresse tous les fichier '.txt' de manière récursive depuis le répertoire
  # courant dans le fichier <file.zip>
  zip -r <file.zip> . -i \*.txt

  # Décompresse le fichier <file.zip>
  unzip <file.zip>
  ```

</tr>
<tr>
  <td>

  `tar`
  </td>
  <td>
  
  Permet de gérer les fichiers `.tar` non compressés ou `.tar.gz` compressés
  
  ```bash
  # Compresse un répertoire et son contenu
  tar cvfz <file.tar.gz> <folder>
 
  # Décompresse le fichier <file.tar.gz> dans le dossier <dst>
  tar xvfz <file.tar.gz> -C <dst>
  ```

</tr>
</table>

### Encodage

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `file <file>`
  </td>
  <td>
  
  Permet de vérifier l'encodage utilisé par `<file>`
</tr>
<tr>
  <td>

  `iconv <file>`
  </td>
  <td>
  
  Permet convertir l'encodage utilisé par `<file>`

  ```bash
  # Converti <src> du format ISO-8859-1 en UTF-8 vers <dst>
  iconv -f ISO-8859-1 -t UTF-8 < <src> > <dst>
  ```

</tr>
</table>

## Gestion des utilisateurs et groupes

| Commande                               | Description                                                                                   |
| -----------------------                | --------------------------------------------------------------------------------------------- |
| `adduser` / `useradd`                  | Ajouter un utilisateur en mode interactif (`adduser`) ou non (`useradd`)                      |
| `addgroup` / `groupadd`                | Créer un nouveau groupe en mode interactif (`addgroup`) ou non (`groupadd`)                   |
| `gpasswd`                              | Permet d’administrer les groupes                                                              |
| `usermod -a -G <groupe> <utilisateur>` | Ajoute un utilisateur dans un groupe                                                          |
| `groups`                               | Affiche les groupes auxquels l’utilisateur appartient                                         |
| `deluser` / `userdel`                  | Supprimer un utilisateur en mode interactif (`deluser`) ou non (`userdel`)                    |
| `delgroup` / `groupdel`                | Supprimer un groupe en mode interactif (`delgroup`) ou non (`groupdel`)                       |
| `pwck`                                 | [*`password check`*]<br /><br />Vérifie l’intégrité du fichier `/etc/passwd` et `/etc/shadow` |
| `grpck`                                | [*`group check`*]<br /><br />Vérifie l’intégrité du fichier `/etc/group` et `/etc/gshadow`    |

## Gestion des process

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `ps -ef`
  </td>
  <td>
  
  [*`process status`*]
  
  Liste tous les process en cours d'exécution
  </td>
</tr>
<tr>
  <td>

  `pgrep <name>`
  </td>
  <td>
  
  Recherche un process en fonction de son `<name>`
  </td>
</tr>
<tr>
  <td>

  `top`
  </td>
  <td>
  
  Affiche en temps réel la liste des process et les ressources qu'ils utilisent
  </td>
</tr>
<tr>
  <td>

  `killall <process_name>`
  </td>
  <td>
  
  Tue tous les process `<process_name>` (à utiliser avec précaution)
  </td>
</tr>
<tr>
  <td>

  `kill [-signal] <pid>`
  </td>
  <td>
  
  Tue le process en fonction de son `<pid>`

  > :information_source: ***INFO***
  >
  > Les principaux `[-signal]` utilisés sont :
  >
  > | Code  | Signal  | Description                                                    |
  > | :---: | ------- | -------------------------------------------------------------- |
  > |  -9   | SIGKILL | Tue le process immédiatement                                   |
  > |  -15  | SIGTERM | Tue le process proprement (défaut)                             |
  > |  -2   | SIGINT  | Interrompt le process (idem `ctrl+c`)                          |
  > |  -3   | SIGQUIT | Interrompt le process (idem `ctrl+c`) et génère un `core dump` |
  > |  -19  | SIGSTOP | Pause le process (idem `crtl+z` (1er))                         |
  > |  -18  | SIGCONT | Reprend l'exécution d'un process en pause (idem `ctrl+z` (2e)) |
  </td>
</tr>
<tr>
  <td>

  `pkill [-signal] <process_name>`
  </td>
  <td>
  
  Tue le process en fonction de son `<process_name>`

  > :information_source: ***INFO***
  >
  > Les codes `[-signal]` sont identique à ceux de la commande `kill`
  </td>
</tr>
<tr>
  <td>

  `bg`
  </td>
  <td>
  
  [*`background`*]
  
  Bascule le process en arrière plan
  </td>
</tr>
<tr>
  <td>

  `fg`
  </td>
  <td>
  
  [*`foreround`*]
  
  Reprend un process suspendu
  </td>
</tr>
<tr>
  <td>

  `jobs`
  </td>
  <td>
  
  Liste les jobs en cours et leur statut
  </td>
</tr>
<tr>
  <td>

  `kill %<id>`
  </td>
  <td>
  
  Tue le job `<id>`
  </td>
</tr>
<tr>
  <td>

  `ctrl+c`
  </td>
  <td>
  
  Arrête la commande en cours
  </td>
</tr>
<tr>
  <td>

  `ctrl+z`
  </td>
  <td>
  
  Suspend l'exécution d’un process
  
  Utilisez la commande `fg` (`foreground`) ou `bg` (`background`) pour reprendre l'exécution du process
  </td>
</tr>
</table>

## Recherche

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `grep <pattern> <files/folder>`
  </td>
  <td>

  [*`global regular expression print`*]
  
  Recherche `<pattern>` dans tous les fichiers `<files>` et/ou `<folder>`

  ```bash
  # ignore la casse
  grep -i <pattern> <files>

  # recherche récursive
  grep -r <pattern> <folder>

  # recherche dans le résultat de la commande précédente
  [command] | grep <pattern>
  ```

  </td>
</tr>
<tr>
  <td>

  `locate <file>`
  </td>
  <td>
  
  Recherche toutes les instances de `<file>`
  </td>
</tr>
<tr>
  <td>

  `updatedb`
  </td>
  <td>
  
  Force la mise à jour la base de données utilisée par la commande `locate`
  </td>
</tr>
<tr>
  <td>

  `find <folder> <parameters>`
  </td>
  <td>
  
  Effectue une recherche de fichiers ou dossiers récursivement depuis `<folder>` en fonction des `<parameters>` :

  ```text
  -type f              fichiers uniquement
  -type d              dossiers uniquement
  -name <pattern>      le nom correspond au <pattern>
  -printf <format>     spécifie le format d'affichage du résultat
  ```

  ```bash
  # Recherche tous les fichiers depuis le répertoire courant ayant pour extension `.md`
  find . -type f -name "*.md"

  # Pour chaque fichier ayant l'extension `.class` trouvé depuis le répertoire `/opt/java`, affiche
  # une ligne formatée `rm <file> -rf`
  find /opt/java -type f -name "*.class" -printf "rm %p -rf\n"

  # Pour chaque élément contenant `.class` trouvé depuis le répertoire `/opt/java`, affiche une ligne
  # formatée `rm <file> -rf` et ÉXECUTE la commande
  find /opt/java -name "*.class" -printf "rm %p -rf\n" | bash

  # Liste tous les fichiers ('-type f') du répertoire courant ('-maxdepth 1') et remplace tous
  # les caractères de fin de ligne; '\r\n' ('Windows') par '\n' ('GNU\Linux')
  find . -maxdepth 1 -type f | xargs perl -pi -e 's/\r\n/\n/g'; ls *.bak | xargs rm

  # Ouvre l'url de tous les PPA qui ne sont pas migrés dans la distribution actuelle de Linux
  find . -type f -not -name "*.save" | xargs grep "# deb " | \
  grep -Eo "(http|https)://[a-zA-Z0-9./?=_-]*" | xargs -t -I{} google-chrome {}/dists/
  ```

  > :pencil2: ***NOTES***
  >
  > **Actions sur le résultat**
  >
  > Il existe deux moyens d'exploiter le résultat d'une recherche soit via la commande `xargs`, soit via l'option `-exec`, toutes deux ayants certains avantages et inconvénients.
  >
  > ***`xargs`***
  >
  > Divise les arguments reçus en paramètre avec des espaces ou des retour-chariots. Si un nom de fichier (ou de chemin) contient des espaces par exemple "mon document.txt", `xargs` va interpréter "mon" et "document.txt" comme étant deux fichiers distincts.
  >
  > La solution pour éviter ce problème consiste à ajouter le paramètre `'-0'` à la commande `xargs` et `'-print0'` comme *dernier* argument de la commande `find`. Ainsi les espaces de fin de chaînes sont remplacés par `null` au lieu de blanc.
  >
  > De plus, dans le cas où le résultat de la commande `find` est vide, `xargs` ne mettra pas fin au traitement et tentera d'exécuter la commande cible ce qui peut occasionner une erreur dans le traitement de cette dernière. Pour éviter ce cas de bord on ajoutera l'option `'-r'` à la commande `xargs`.
  >
  > ```bash
  > # Syntaxe
  > find <where> <criteria> -print0 | xargs -0 -r <command>
  > ```
  >
  > *Avantages*
  >
  >- Le traitement des résultats est plus efficace qu'avec l'option `-exec`
  >- Le résultat de la commande `xargs` peut être réutilisée avec un `pipe` (|)
  >
  > ***`-exec`***
  >
  > L'option `-exec` (à ne pas confondre avec la commande shell `exec`) exécute la commande sur chaque fichier trouvé répondant aux critères de recherche.
  >
  > La paire d'accolade est automatiquement remplacée par le nom du fichier. Si le nom du fichier contient des caractères spéciaux, le `shell` tentera de les interpréter. Pour éviter cette interprétation, les accolades doivent être entourées d'apostrophe (`'{}'`).
  >
  > Enfin, l'instruction à exécuter (ou commande) doit être terminée par un backslash et un point-virgule (`\;`).
  >
  > ```bash
  > # Syntaxe
  > find <where> <criteria> -exec <command> '{}' \;
  > ```
  >
  > ***Exemple***
  >
  > ```bash
  > # Recherche l'instruction `linux` dans tous les fichiers '*.txt' trouvés par la commande find
  > find . -name '*.txt' -print0 | xargs -0 -r grep 'linux'
  > find . -name '*.txt' -exec grep 'linux' '{}' \; -print
  > ```
  >

  </td>
</tr>
</table>

## Information système

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `date`
  </td>
  <td>
  
  Affiche l'heure et la date courante
  </td>
</tr>
<tr>
  <td>

  `cal`
  </td>
  <td>
  
  Affiche le calendrier du mois courant
  </td>
</tr>
<tr>
  <td>

  `uptime`
  </td>
  <td>
  
  Indique le temps depuis lequel la machine tourne sans interruption; valeur remise à zéro lors d’un redémarrage
  </td>
</tr>
<tr>
  <td>

  `w`
  </td>
  <td>
  
  Affiche la liste des utilisateurs connectés et leurs activités en cours
  </td>
</tr>
<tr>
  <td>

  `uname -a`
  </td>
  <td>
  
  Affiche les informations sur le noyau GNU/Linux (`kernel`)
  </td>
</tr>
<tr>
  <td>

  `cat /proc/cpuinfo`
  </td>
  <td>
  
  Affiche les informations des CPU de la machine
  </td>
</tr>
<tr>
  <td>

  `cat /proc/meminfo`
  </td>
  <td>
  
  Affiche les informations sur la mémoire de la machine
  </td>
</tr>
<tr>
  <td>

  `df -h`
  </td>
  <td>
  
  Indique les quantités d'espaces disques utilisées et disponibles sur les systèmes de fichiers
  </td>
</tr>
<tr>
  <td>

  `du -h`
  </td>
  <td>
  
  Fournit les statistiques sur l'utilisation du disque
  </td>
</tr>
<tr>
  <td>

  `duf`
  </td>
  <td>
  
  Affiche l'espace libre sur les disques durs (idem `df -h`)
  </td>
</tr>
<tr>
  <td>

  `fdisk -l <device>`
  </td>
  <td>
  
  Affiche la liste des partitions du `<device>`
  </td>
</tr>
<tr>
  <td>

  `lsblk`
  </td>
  <td>
  
  [*`list block devices`*]

  Affiche les informations sur les dispositifs connectés qui lisent ou écrivent des données sous forme de blocs de taille fixe (disque dur, clef usb, etc.)
  </td>
</tr>
<tr>
  <td>

  `lshw`
  </td>
  <td>
  
  [*`list hardware`*]

  Affiche les informations sur le matériel configuré sur la machine
  </td>
</tr>
<tr>
  <td>

  `free`
  </td>
  <td>
  
  Affiche la mémoire disponible et utilisée (y.c. `swap`)
  </td>
</tr>
<tr>
  <td>

  `whereis <command>`
  </td>
  <td>
  
  Recherche les fichiers exécutables, les sources et la page de man de `<command>`
  </td>
</tr>
<tr>
  <td>

  `which <command>`
  </td>
  <td>
  
  Affiche le chemin d’accès de la `<command>`
  </td>
</tr>
<tr>
  <td>

  `env`
  </td>
  <td>
  
  Affiche les variables d’environnement
  </td>
</tr>
<tr>
  <td>

  `export`
  </td>
  <td>
  
  Définit une variable d’environnement
  </td>
</tr>
<tr>
  <td>

  `systemctl`
  </td>
  <td>

  [*`system control`*]
  
  Permet de gérer les services (`daemons`)

  ```bash
  # Affiche l'état du service ssh
  systemctl status ssh

  # Démarre le service ssh
  systemctl start ssh 

  # Redémarre le service ssh
  systemctl restart ssh

  # Arrête le service ssh
  systemctl stop ssh

  # Active le service ssh au démarrage
  systemctl enable ssh

  # Désactive le service ssh au démarrage
  systemctl disable ssh
  ```

  </td>
</tr>
<tr>
  <td>

  `journalctl`
  </td>
  <td>

  [*`journal control`*]
  
  Permet d'afficher et de rechercher des informations dans les logs du système.
  </td>
</tr>
</table>

## Réseau

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `ping <ip/host>`
  </td>
  <td>
  
  Teste l'accessibilité d'une autre machine à travers un réseau IP
  </td>
</tr>
<tr>
  <td>

  `arp`
  </td>
  <td>
  
  [*`address resolution protocol`*]

  Permet de visualiser ou modifier la table du cache `arp` des interfaces (également possible avec la commande `ip`)
  </td>
</tr>
<tr>
  <td>

  `mtr <ip/host>`
  </td>
  <td>
  
  Combine les fonctionnalités de `traceroute` et `ping` dans une seule commande (remplace les commandes `ping` et `traceroute`)
  </td>
</tr>
<tr>
  <td>

  `dig`
  </td>
  <td>

  [*`domain information groper`*]
  
  Permet d'interroger les serveurs DNS pour obtenir les informations définies pour un domaine déterminé. (remplace la commande `nslookup`)
  </td>
</tr>
<tr>
  <td>

  `resolvectl status`
  </td>
  <td>
  
  Affiche les serveurs `DNS` utilisés
  </td>
</tr>
<tr>
  <td>

  `nmap <options>`
  </td>
  <td>

  Permet de détecter les ports ouverts, d'identifier les services hébergés et d'obtenir des informations sur le système d'exploitation d'un ordinateur distant. (c.f. [Nmap Cheat Sheet](https://hackertarget.com/nmap-cheatsheet-a-quick-reference-guide/))
  </td>
</tr>
<tr>
  <td>

  `nc <host> <port>`
  </td>
  <td>

  Permet de tester la connexion sur un port

  ```bash
  nc -z -v google.com 443

  > Connection to google.com (2a00:1450:400a:801::200e) 443 port [tcp/https] succeeded!
  ```

  </td>
</tr>
<tr>
  <td>

  `ip`
  </td>
  <td>

  Permet de manipuler et configurer les interfaces réseaux

  ```bash
  # Affiche toutes les adresses IP des interfaces réseaux
  ip a

  # Affiche un résumé en couleur des addresses IPv4 des interfaces réseaux
  ip -br -c -4 a

  # Surveille les changements qui surviennent sur une interface
  ip mon

  # Active une interface
  #   /!\ si l'interface reste down, essayez de redémarrer NetworkManager (`sudo nmcli networking on`)
  sudo ip link set <interface> up

  # Désactive une interface
  sudo ip link set <interface> down

  # Supprime la configuration d'une interface
  #   > Exécutée avant d'assigner une adresse permet de partir d'une configuration "propre"
  sudo ip a flush dev <interface>

  # Assigne une adresse IP à une interface
  sudo ip a add <ip>/<mask> dev <interface>

  # Supprime l'adresse IP d'une interface
  sudo ip a del <ip> dev <interface>
  ```

  ```bash
  # Affiche la liste des routes
  ip route

  # Crée une route par défaut
  sudo ip route add default gw <gateway> <interface>

  # Supprime une route par défaut
  sudo ip route del default gw <gateway> <interface>
  ```

  ```bash
  # Affiche le contenu de la table ARP
  ip neigh

  # Ajoute une entrée ARP
  sudo ip neigh add <ip> lladdr <mac> dev <interface>  

  # Supprime une entrée ARP
  sudo ip neigh del <ip> dev <interface>
  ```

  </td>
</tr>
<tr>
  <td>

  `route`
  </td>
  <td>

  Afficher / Manipuler la table de routage IP (également possible avec la commande `ip`)

  ```bash
  # Affiche la liste des routes
  route

  # Crée une route par défaut
  sudo route add default gw <gateway> <interface>

  # Supprime une route par défaut
  sudo route del default gw <gateway> <interface>
  ```
  
  </td>
</tr>
<tr>
  <td>

  `netstat`
  </td>
  <td>

  Affiche les informations du sous-système réseau de GNU/Linux; connexions réseau, tables de routage, statistiques des interfaces, connexions masquées, messages netlink et membres multicast
  </td>
</tr>
<tr>
  <td>

  `ss`
  </td>
  <td>

  [*`socket statistics`*]

  Affiche les sockets actuellement utilisés sur le système. (remplace la commande `netstat`)
  </td>
</tr>
<tr>
  <td>

  `lsof`
  </td>
  <td>

  [*`list open files`*]

  En plus d'afficher la liste des fichiers ouvert sur le système, la commande `lsof` permet également de connaître la liste des ports ouvert :
  
  ```bash
  lsof -i :<port>
  ```

  </td>
</tr>
<tr>
  <td>

  `wget <url>`
  </td>
  <td>

  Permet de récupérer du contenu d'un serveur HTTP, HTTPS ou FTP

  ```bash
  # Continue le téléchargement qui aurait été stoppé
  wget -c <url>

  # Aspire le contenu d'un site web
  wget -r -nc -p -E -kv -np --restrict-file-names=windows \
  --domains markdownguide.org https://www.markdownguide.org/cheat-sheet/
  ```

  </td>
</tr>
<tr>
  <td>

  `curl <src> <dst>`
  </td>
  <td>

  [*`client URL request library`*]

  Permet de transférer des données vers ou depuis un serveur, en utilisant l'un des nombreux protocoles pris en charge : HTTP, HTTPS, FTP, IMAP, POP3, SCP, SFTP, SMTP, TFTP, TELNET, LDAP

  ```bash
  # Affiche le contenu dans la sortie standard (`stdout`)
  curl https://www.markdownguide.org

  # Télécharge une clé gpg et l'ajoute dans les clés apt certifiées
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  ```

  </td>
</tr>
<tr>
  <td>

  `ssh <user>@<address/host>`
  </td>
  <td>

  [*`secure shell`*]

  Permet d'établir une connexion sécurisée sur une machine distante

  ```bash
  # Redirige l'affichage graphique (si autorisé sur le serveur)
  ssh -X <user>@<address/host>
  ```

  </td>
</tr>
<tr>
  <td>

  `scp <src> <dst>`
  </td>
  <td>

  [*`secure copy protocol`*]

  Permet de faire une copie sécurisée d'un fichier ou dossier sur une machine distante

  ```bash
  # Copie le fichier local smb.conf dans le dossier /etc/samba distant
  scp /etc/samba/smb.conf <user>@<address/host>:/etc/samba

  # Copie le fichier distant smb.conf dans le dossier local /etc/samba
  scp <user>@<address/host>:/etc/samba/smb.conf /etc/samba/

  # Utilisation de la commande `rsync` pour reprendre le transfert
  #  ! rsync doit être installé sur les 2 postes (src et dst)
  rsync --partial --progress --rsh=ssh /etc/samba/smb.conf <user>@<address/host>:/etc/samba/
  ```

  </td>
</tr>
<tr>
  <td>

  `tcpdump`
  </td>
  <td>

  Capture tous les paquets qui transitent par une carte réseau

  ```bash
  # Crée le fichier `dump.pcap` contenant la capture de tous les paquets.
  # Ce dernier peut être par la suite analysé avec des outils tels que `Wireshark`
  tcpdump -i <interface> -w dump.pcap
  ```

  </td>
</tr>
</table>

## Historique

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `history`
  </td>
  <td>
  
  Affiche l’historique des commandes

  ```bash
  # Supprime une commande de l'historique
  history -d <id>

  # Supprime tout le contenu de l'historique
  cat /dev/null > ~/.bash_history && history -c
  ```

  </td>
</tr>
<tr>
  <td>

  `ctrl+r`
  </td>
  <td>
  
  Permet de rechercher parmi les commandes récentes
  </td>
</tr>
<tr>
  <td>

  `ctrl+p`
  </td>
  <td>
  
  Affiche la commande précédente
  </td>
</tr>
<tr>
  <td>

  `ctrl+n`
  </td>
  <td>
  
  Affiche la commande suivante
  </td>
</tr>
<tr>
  <td>

  `ctrl+g`
  </td>
  <td>
  
  Permet d'annuler la recherche effectuée avec `ctrl+r`
  </td>
</tr>
<tr>
  <td>

  `!<x>`
  </td>
  <td>
  
  Rappelle la commande `<x>` de l'historique
  </td>
</tr>
<tr>
  <td>

  `!!`
  </td>
  <td>
  
  Répète la dernière commande
  </td>
</tr>
<tr>
  <td>

  `!^`
  </td>
  <td>
  
  Répète le premier argument de la commande précédente
  </td>
</tr>
<tr>
  <td>

  `!$`
  </td>
  <td>
  
  Répète le dernier argument de la commande précédente
  </td>
</tr>
<tr>
  <td>

  `!:<x>`
  </td>
  <td>
  
  Répète l'argument `<x>` de la commande précédente
  </td>
</tr>
<tr>
  <td>

  `!:<x>-<y>`
  </td>
  <td>
  
  Répète les arguments de `<x>` à `<y>` de la commande précédente
  </td>
</tr>
<tr>
  <td>

  `!<string>`
  </td>
  <td>
  
  Exécute la dernière commande commençant par la chaîne de caractères `<string>`
  </td>
</tr>
<tr>
  <td>

  `!?<string>`
  </td>
  <td>
  
  Exécute la dernière commande contenant par la chaîne de caractères `<string>`
  </td>
</tr>
<tr>
  <td>

  `^<old>^<new>`
  </td>
  <td>
  
  Exécute la dernière commande en remplaçant la chaîne de caractères `<old>` par `<new>`
  </td>
</tr>
<tr>
  <td>

  `<cmd> !^`
  </td>
  <td>
  
  Exécute la commande avec le premier argument de la commande précédente
  </td>
</tr>
<tr>
  <td>

  `<cmd> !:<x>-<y>`
  </td>
  <td>
  
  Exécute la commande avec les arguments de `<x>` à `<Y>` de la commande précédente
  </td>
</tr>
<tr>
  <td>

  `<cmd> !:<x>`
  </td>
  <td>
  
  Exécute la commande avec l'argument `<x>` de la commande précédente
  </td>
</tr>
<tr>
  <td>

  `<cmd> !$`
  </td>
  <td>
  
  Exécute la commande avec le dernier argument de la commande précédente
  </td>
</tr>
</table>

## Raccourcis

| **Déplacement** |                                             |
| --------------- | ------------------------------------------- |
| `ctrl+a`        | Déplace le curseur en début de ligne        |
| `ctrl+e`        | Déplace le curseur en fin de ligne          |
| `alt+b`         | Déplace le curseur mot à mot vers l'arrière |
| `alt+f`         | Déplace le curseur mot à mot vers l'avant   |

| **Édition**     |                                                                  |
| --------------- | ---------------------------------------------------------------- |
| `ctrl+k`        | Efface la fin de la ligne à partir du curseur                    |
| `ctrl+u`        | Efface le début de la ligne jusqu'au curseur                     |
| `ctrl+w`        | Coupe le mot qui précède le curseur                              |
| `ctrl+y`        | Colle le mot qui a été coupé avec `ctrl+w`                       |
| `alt+backspace` | Coupe le chaîne avant le curseur mais s'arrête aux `/` et au `.` |
| `alt+d`         | Coupe le chaîne après le curseur                                 |
| `ctrl+t`        | Inverse la position des deux caractères avant le curseur         |
| `alt+t`         | Inverse la position des deux mots avant le curseur               |
| `alt+c`         | Met une lettre en majuscule                                      |
| `alt+l`         | Met un mot en minuscule                                          |
| `alt+u`         | Met un mot en majuscule                                          |
| `alt+.`         | Réécrit le dernier mot de la ligne précédente                    |
| `ctrl+_`        | Annule la dernière modification                                  |

| **Divers** |                                                                    |
| ---------- | ------------------------------------------------------------------ |
| `ctrl+l`   | Efface l'écran de la console (idem commande `clear`)               |
| `ctrl+d`   | Quitte la session en cours   (idem commande `exit`)                |
| `ctrl+s`   | Masque la saisie et suspends le défilement (`ctrl+q` pour revenir) |
| `ctrl+q`   | Fait apparaître la saisie et relance l'affichage                   |
| `Tab`      | Complète automatiquement le nom du fichier ou répertoire           |

## Redirection des flux

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `>`

  ou

  `>>`
  </td>
  <td>

  `cmd > file` ou `cmd >> file`
  
  Redirige la sortie standard (`stdout`) de `cmd` dans `file`

  |       |                             |
  | :---: | --------------------------- |
  |  `>`  | écrase `file` s'il existe   |
  | `>>`  | complète `file` s'il existe |
  </td>
</tr>
<tr>
  <td>

  `1>`

  ou

  `1>>`
  </td>
  <td>

  `cmd 1> file` ou `cmd 1>> file`
  
  Redirige la sortie standard (`stdout`) de `cmd` dans `file` (idem `>` et `>>`)
  </td>
</tr>
<tr>
  <td>

  `2>`

  ou

  `2>>`
  </td>
  <td>

  `cmd 2> file` ou `cmd 2>> file`
  
  Redirige la sortie d'erreur (`stderr`) dans `file`
  </td>
</tr>
<tr>
  <td>

  `2>&1`
  </td>
  <td>

  `cmd > file 2>&1` ou `cmd >> file 2>&1`
  
  Redirige `stderr` vers `stdout` puis dans `file`
  </td>
</tr>
<tr>
  <td>

  `&>`
  </td>
  <td>

  `cmd &> file`
  
  Redirige `stderr` et `stdout` dans `file`
  </td>
</tr>
</table>

> :warning: ***WARNING***
>
> L'ordre d’utilisation des redirecteurs est important
>
> La commande suivante ajoute "Bonjour" au fichier `hello.txt`
>
> ```bash
> echo "Bonjour" 2> hello.txt 1>&2
> ```
>
> Alors que la commande suivante affiche "Bonjour" sur `stdout` sans créer de fichier
>
> ```bash
> echo "Bonjour" 1>&2 2> hello.txt
> ```

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `<`
  </td>
  <td>

  `cmd < file`
  
  Utilise `file` comme entrée standard (`stdin`) de `cmd`
  </td>
</tr>
<tr>
  <td>

  `|`

  *(pipe)*
  </td>
  <td>

  `cmd1 | cmd2`
  
  Envoie la sortie (`stdout`) de `cmd1` comme entrée standard (`stdin`) de `cmd2`
  </td>
</tr>
<tr>
  <td>

  `<<[heredoc]`
  </td>
  <td>

  `cmd <<[heredoc]`
  
  Utilise l'entrée standard (`stdin`) pour `cmd` jusqu'à la chaîne `heredoc` de fermeture

  ```bash
  # Crée le fichier sample.txt contenant les 2 lignes "first line" et "second line"
  $ tee sample.txt <<EOF
  > first line
  > second line
  > EOF
  ```

  </td>
</tr>
</table>

## Planification de tâche

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `at`
  </td>
  <td>
  
  Planifie l'exécution d'une commande
  </td>
</tr>
<tr>
  <td>

  `batch`
  </td>
  <td>
  
  Exécute une commande lorsque la charge du système le permet
  </td>
</tr>
<tr>
  <td>

  `atq`
  </td>
  <td>
  
  Affiche la liste des commandes dans la queue d’exécution
  </td>
</tr>
<tr>
  <td>

  `atrm <job>`
  </td>
  <td>
  
  Supprime un job en fonction de son identifiant
  </td>
</tr>
<tr>
  <td>

  `crontab -e`
  </td>
  <td>
  
  Édite le fichier `crontab` afin de programmer l'exécution d'une commande à interval régulier

- [Cron Vs Anacron: How to Schedule Jobs Using Anacron on Linux](https://www.tecmint.com/cron-vs-anacron-schedule-jobs-using-anacron-on-linux/)

</td>
</tr>
</table>

## Divers

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `man <section> <command>`
  </td>
  <td>
  
  Accèder au manuel (mode d'emploi) des commandes GNU/Linux

  ```bash
  # Permet d'inclure une expression régulière pour rechercher une commande particulière
  man -k <expression>
  ```

  </td>
</tr>
<tr>
  <td>

  `echo`
  </td>
  <td>
  
  Affiche une ligne de texte

  ```bash
  # Affiche le code retour de la commande précédente
  echo $?
  ```

  </td>
</tr>
<tr>
  <td>

  `tee`
  </td>
  <td>
  
  Copier l'entrée standard (`stdin`) sur la sortie standard (`stdout`)

  ```bash
  # Ajoute la ligne `GNU/Linux` dans tous les fichiers `.txt` du répertoire courant
  tee *.txt <<EOF
  GNU/Linux
  EOF
  ```

  </td>
</tr>
<tr>
  <td>

  `sed`
  </td>
  <td>
  
  Éditeur de flux pour le filtrage et la transformation de texte

  ```bash
  # Remplace toutes les occurrences `old` par `new` dans le fichier `sample.txt` et affiche
  # le résultat dans la sortie standard (`stdout`)
  sed "s/old/new/g" sample.txt

  # Met à jour le fichier `sample.txt` en remplaçant toutes les occurrences `old` par `new`
  sed -i "s/old/new/g" sample.txt

  # Met à jour le fichier `sample.txt` en remplaçant toutes les occurrences `old` par `new`
  # pour autant que la ligne commence par le mot `build`
  sed -i "/^build/ s/old/new/g" sample.txt

  # Met à jour le fichier `sample.txt` en remplaçant toutes les occurrences `old` par `new`
  # pour autant que la ligne contienne le mot `build`
  sed -i "/build/ s/old/new/g" sample.txt

  # Effectue un remplacement de masse dans l'ensemble des fichiers texte (non binaire) trouvés avec `grep`.
  # Afin d'éviter les soucis d'espaces, le nom de fichier est mis entre guillemets
  grep -Ilr "old" | sed -e "s/\(.*\)/\"\1\"/" | tr '\n' ' ' | xargs sed -i 's/old/new/g'
  ```

  </td>
</tr>
<tr>
  <td>

  `shuf`
  </td>
  <td>
  
  Génère des permutations aléatoires

  ```bash
  # Génère un nombre aléatoire entre 1 et 15
  shuf -i 1-15 -n1 
  ```

  ```bash
  # Génère un nombre aléatoire entre 41 et 44 puis le converti en caractère unicode entre A et D
  shuf -i 41-44 -n1 | xxd -p -r; echo ""
  ```

  ```bash
  # Génère une liste aléatoire
  shuf <<EOF
  Alexandre
  Bernard
  Denis
  Juliette
  Marie
  EOF
  Marie
  Juliette
  Bernard
  Denis
  Alexandre
  ```

  </td>
</tr>
<tr>
  <td>

  `shutdown`
  </td>
  <td>
  
  Arrête le système

  ```bash
  # Demande un arrêt immédiat du système
  shutdown -h now

  # Demande un redémarrage immédiat du système (similaire à `reboot`)
  shutdown -r now
  ```

  </td>
</tr>
<tr>
  <td>

  `reboot`
  </td>
  <td>
  
  Demande un redémarrage du système (similaire à `shutdown -r now`)
  </td>
</tr>
</table>

## Références

- [commandlinefu.com](http://www.commandlinefu.com/)
- [LinuxCommand.org](https://linuxcommand.org/)

<br /><p style='text-align: right;'><sub>&copy; 2010-2023 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.ch)</sub></p>
