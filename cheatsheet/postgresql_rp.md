<!--
Copyright (C) 2020-2022 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Cheatsheet PostgreSQL <!-- omit in toc -->

***
:pencil2: **_INFO_**

La plupart des exemples de ce document on été réalisés sur la base de données d'exemples [Pagila](https://www.postgresqltutorial.com/postgresql-sample-database/).

Le document [Ubuntu Server - PostgreSQL](https://docs.google.com/document/d/1WpmbE6wustSaN1hh7BqWVk_a-NeRXVGIsTnCa3kzJQg/edit#) vous guidera pour l'installation et la configuration d'une machine virtuelle de test.

Un grand merci à toutes les personnes qui ont participé à l'ajout d'exemples concrets.
***

## Table des matières <!-- omit in toc -->

- [Interrogation des données](#interrogation-des-données)
  - [`SELECT`](#select)
    - [Syntaxe](#syntaxe)
    - [Clause `WHERE`](#clause-where)
      - [Opérateurs](#opérateurs)
      - [Wildcards pour l'instruction `LIKE`](#wildcards-pour-linstruction-like)
    - [`EXPLAIN`](#explain)
      - [Syntaxe](#syntaxe-1)
  - [Instruction `WITH`](#instruction-with)
    - [Syntaxe](#syntaxe-2)
  - [Grouper le résultat de plusieurs `SELECT`](#grouper-le-résultat-de-plusieurs-select)
    - [`UNION` et `UNION ALL`](#union-et-union-all)
    - [`INTERSECT`](#intersect)
    - [`EXCEPT`](#except)
- [Jointures](#jointures)
  - [`INNER JOIN` ou `JOIN`](#inner-join-ou-join)
  - [`LEFT JOIN`](#left-join)
  - [`RIGHT JOIN`](#right-join)
  - [`FULL JOIN`](#full-join)
  - [`NATURAL JOIN`](#natural-join)
- [Sous-requêtes](#sous-requêtes)
  - [Sous-requête basée sur un seul résultat](#sous-requête-basée-sur-un-seul-résultat)
  - [Sous-requête basée sur une colonne](#sous-requête-basée-sur-une-colonne)
  - [Sous-requête basée sur une table](#sous-requête-basée-sur-une-table)
  - [`EXISTS`](#exists)
- [Gestion des tables](#gestion-des-tables)
  - [`CREATE TABLE`](#create-table)
    - [Syntaxe](#syntaxe-3)
      - [Contraintes](#contraintes)
    - [Séquences](#séquences)
      - [Auto-increment (`SMALLSERIAL`, `SERIAL`, `BIGSERIAL`)](#auto-increment-smallserial-serial-bigserial)
      - [`CREATE SEQUENCE`](#create-sequence)
        - [Syntaxe](#syntaxe-4)
      - [`DROP SEQUENCE`](#drop-sequence)
        - [Syntaxe](#syntaxe-5)
  - [`ALTER` table](#alter-table)
    - [Syntaxe](#syntaxe-6)
      - [Actions](#actions)
  - [`DROP` table](#drop-table)
    - [Syntaxe](#syntaxe-7)
- [Modification des données](#modification-des-données)
  - [`INSERT`](#insert)
    - [Syntaxe](#syntaxe-8)
    - [`RETURNING`](#returning)
  - [`UPDATE`](#update)
    - [Syntaxe](#syntaxe-9)
    - [`RETURNING`](#returning-1)
  - [`UPDATE JOIN`](#update-join)
    - [Syntaxe](#syntaxe-10)
  - [`DELETE`](#delete)
    - [Syntaxe](#syntaxe-11)
    - [`RETURNING`](#returning-2)
  - [Upsert (`INSERT ON CONFLICT`)](#upsert-insert-on-conflict)
    - [Syntaxe](#syntaxe-12)
- [Fonctions](#fonctions)
  - [`MAX()`, `MIN()`, `AVG()`, `SUM()`](#max-min-avg-sum)
  - [`COUNT()`](#count)
- [String Functions](#string-functions)
  - [Escape char](#escape-char)
  - [`CONCAT()`](#concat)
  - [`SUBSTRING()`, `RIGHT()`, `LEFT()`](#substring-right-left)
  - [`POSITION()`](#position)
  - [`TRIM()`, `RTRIM()`, `LTRIM()`](#trim-rtrim-ltrim)
  - [`UPPER()`, `LOWER()`](#upper-lower)
  - [`LENGTH()`](#length)
  - [`INITCAP()`](#initcap)
  - [`REPLACE()`, `TRANSLATE()`](#replace-translate)
  - [`RPAD()`, `LPAD()`](#rpad-lpad)
  - [`CHR()`, `ASCII()`](#chr-ascii)
  - [Expressions régulières (`REGEX`)](#expressions-régulières-regex)
- [Types](#types)
  - [Principaux types](#principaux-types)
  - [Les entiers](#les-entiers)
  - [`CAST()`](#cast)
    - [Syntaxe](#syntaxe-13)
  - [Fonctions de formatage](#fonctions-de-formatage)
    - [`TO_CHAR()`](#to_char)
    - [`TO_NUMBER()`](#to_number)
    - [`TO_DATE()` et `TO_TIMESTAMP()`](#to_date-et-to_timestamp)
- [Dates](#dates)
  - [`CURRENT_DATE`, `CURRENT_TIME`, `CURRENT_TIMESTAMP`](#current_date-current_time-current_timestamp)
  - [`NOW()`](#now)
  - [Différence entre 2 dates](#différence-entre-2-dates)
  - [`AGE()`](#age)
  - [`EXTRACT()`, `DATE_PART()`](#extract-date_part)
  - [`INTERVAL`](#interval)
  - [`JUSTIFY_DAYS()`, `JUSTIFY_HOURS()`, `JUSTIFY_INTERVAL()`](#justify_days-justify_hours-justify_interval)
  - [`DATE_TRUNC()`](#date_trunc)
  - [`TIMESTAMP`, `TIMESTAMPTZ`](#timestamp-timestamptz)
    - [Format](#format)
- [Window Functions](#window-functions)
  - [Clause `Over`](#clause-over)
    - [Arguments](#arguments)
  - [`LEAD()`, `LAG()`](#lead-lag)
  - [`ROW_NUMBER()`, `RANK()`, `DENSE_RANK()`](#row_number-rank-dense_rank)
- [Expressions conditionnelles et opérateurs](#expressions-conditionnelles-et-opérateurs)
  - [`CASE`](#case)
    - [Syntaxe](#syntaxe-14)
  - [`COALESCE`](#coalesce)
    - [Syntaxe](#syntaxe-15)
  - [`NULLIF`](#nullif)
    - [Syntaxe](#syntaxe-16)
  - [`MOD()`](#mod)
- [Gestion des index](#gestion-des-index)
  - [`CREATE INDEX`](#create-index)
    - [Syntaxe](#syntaxe-17)
      - [Méthode d'indexation](#méthode-dindexation)
    - [`UNIQUE INDEX`](#unique-index)
    - [Index d'expressions](#index-dexpressions)
    - [Index partiel](#index-partiel)
    - [Index multicolonnes](#index-multicolonnes)
    - [`Hash index`](#hash-index)
  - [`REINDEX`](#reindex)
    - [Syntaxe](#syntaxe-18)
  - [`DROP INDEX`](#drop-index)
    - [Syntaxe](#syntaxe-19)
- [Cas pratiques](#cas-pratiques)
- [DBVisualizer](#dbvisualizer)
  - [Raccourcis](#raccourcis)
- [Google Sheet](#google-sheet)
  - [`QUERY()`](#query)
    - [Syntaxe](#syntaxe-20)
- [Références](#références)

# Interrogation des données

## `SELECT`

### Syntaxe

| Instruction                                    | Description                                                        |
| ---------------------------------------------- | ------------------------------------------------------------------ |
| `SELECT [DISTINCT] nom_champ (ou * pour tous)` | lit les enregistrement d'une table                                 |
| `FROM [nom_shema.]nom_table`                   | table contenant les données à récupérer                            |
| `WHERE condition`                              | condition d'extraction                                             |
| `GROUP BY expression`                          | groupe plusieurs résultats                                         |
| `HAVING condition`                             | filtre le résultat avec une fonction : SUM, COUNT, AVG, MIN ou MAX |
| `LIMIT count`                                  | spécifie le nombre d'enregistrements à récupérer                   |

#### Exemples <!-- omit in toc -->

```sql
SELECT 
  * 
FROM 
  partenaire;
```

```sql
SELECT 
  * 
FROM 
  partner.partenaire;
```

```sql
SELECT 
  nom, 
  numero 
FROM 
  partner.partenaire;
```

```sql
SELECT 
  nom 
FROM 
  partner.partenaire 
ORDER BY 
  classe;
```

```sql
SELECT 
  DISTINCT nom 
FROM 
  partner.partenaire;
```

### Clause `WHERE`

#### Opérateurs

|   Opérateur   | Description                                              |
| :-----------: | -------------------------------------------------------- |
|      `=`      | égal                                                     |
| `!=` ou `<>`  | différent                                                |
|      `>`      | plus grand                                               |
|      `<`      | plus petit                                               |
|     `>=`      | plus grand ou égal                                       |
|     `<=`      | plus petit ou égal                                       |
|     `IN`      | liste de plusieurs valeurs possibles                     |
|   `BETWEEN`   | valeur comprise dans un intervalle donné (<= / >=)       |
|    `LIKE`     | recherche en spécifiant le début, milieu ou fin d'un mot |
|   `IS NULL`   | valeur est nulle                                         |
| `IS NOT NULL` | valeur n'est pas nulle                                   |

##### Exemples <!-- omit in toc -->

```sql
SELECT 
  classe 
FROM 
  partner.partenaire 
WHERE 
  classe = 'AGENCE';
```

```sql
SELECT 
  classe 
FROM 
  partner.partenaire 
WHERE 
  classe != 'AGENCE';
```

```sql
SELECT 
  numero 
FROM 
  partner.partenaire 
WHERE 
  BETWEEN 10 AND 100;
```

```sql
SELECT 
  nom 
FROM 
  partenaire 
WHERE 
  succession_repudiee = true;
```

```sql
SELECT
  nom, 
  dte_log_i
FROM
  partenaire
WHERE
  dte_log_i BETWEEN '2020-01-01' AND '2020-01-31';
```

```sql
SELECT
  numero,
  nom
FROM
  partner.partenaire
WHERE
  numero BETWEEN 10 AND 100
AND 
  (
    nom = 'Bronstein'
  OR nom = 'Nulsen')
AND numero = 11;
```

```sql
SELECT
  numero,
  nom
FROM
  partner.partenaire
WHERE
  numero IN (10, 
             30, 
             45, 
             71, 
             432);
```

```sql
SELECT
  etat_generation,
  SUM(numero)
FROM
  partner.partenaire
GROUP BY
  partenaire.etat_generation
HAVING
  SUM(numero) > 1150000000
ORDER BY 
  etat_generation;
```

```sql
-- Odyssee - Liste des pol_id et mupo_gen qui ont de la PU
SELECT
  mut_pol.pol_id,
  mut_pol.mupo_gen,
  CAST(mut_pol.pol_id AS VARCHAR) || '-' || CAST(mut_pol.mupo_gen AS VARCHAR) AS test
FROM
  odyssee.mut_pol
WHERE
  CAST(pol_id AS VARCHAR) || '-' || CAST(mupo_gen AS VARCHAR) IN
  ( SELECT
      CAST(pol_id AS VARCHAR) || '-' || CAST(mupo_gen AS VARCHAR)
    FROM
      odyssee.prime_unique)
AND mut_pol.pol_id = 484369
ORDER BY
  1,
  2,
  3;
```

#### Wildcards pour l'instruction `LIKE`

| Caractère | Description                                 |
| :-------: | ------------------------------------------- |
|    `%`    | représente zéro, un ou plusieurs caractères |
|    `_`    | représente un seul caractère                |

##### Exemples <!-- omit in toc -->

```sql
SELECT 
  nom 
FROM 
  localite 
WHERE 
  nom LIKE 'A%';
```

```sql
SELECT 
  nom 
FROM 
  localite 
WHERE 
  nom LIKE 'A%ora';
```

```sql
SELECT 
  nom 
FROM 
  localite 
WHERE 
  nom LIKE 'Au_ora';
```

```sql
SELECT 
  nom 
FROM 
  localite 
WHERE 
  nom LIKE 'Au_o%i';
```

```sql
SELECT
  nom
FROM
  partenaire
WHERE 
  UPPER(nom) LIKE 'IM%';
```

### `EXPLAIN`

L'instruction `EXPLAIN` est à utiliser juste avant un `SELECT`. Elle permet d'afficher le plan d'exécution d'une requête ainsi que le coût, en terme de temps, de chaque instruction de la requête.

> :pencil2: **_TIP_**
>
> Le site [PostgreSQL execution plan visualizer](https://explain.dalibo.com/) offre une interface pour l'analyse des plans d'exécution

#### Syntaxe

```sql
EXPLAIN [ ( option [, ...] ) ] sql_statement;
```

| `option`                                 | Description                                                                                                                                                                                                        |
| ---------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `ANALYZE`                                | L'option `ANALYZE` exécute la requête, puis génère des statistiques d'exécution. Si vous souhaitez analyser sans exécuter la requête, vous devez l'exécuter dans une transaction (`BEGIN; EXPLAIN ...; ROLLBACK;`) |
| `VERBOSE`                                | Affiche des informations complémentaires concernant le plan 'exécution                                                                                                                                             |
| `COSTS`                                  | Inclut les coûts de démarrage et totaux estimés de chaque noeud du plan d'exécution, ainsi que le nombre estimé de lignes traitées                                                                                 |
| `BUFFERS`                                | Ajoute des informations sur l'utilisation de la mémoire tampon (`BUFFERS`). Cette option ne peut être utilisée que lorsque `ANALYSE` est activé                                                                    |
| `TIMING`                                 | Ce paramètre détermine le temps de démarrage réel et le temps passé dans chaque noeud du plan d'exécution. Cette option ne peut être utilisée que lorsque `ANALYSE` est activé                                     |
| `SUMMARY`                                | Ajoute des informations récapitulatives sur l'exécution de la requête (p.ex durée totale)                                                                                                                          |
| `FORMAT { TEXT \| XML \| JSON \| YAML }` | Spécifie le format de sortie du plan d'exécution (Défaut: `TEXT`)                                                                                                                                                  |

#### Exemples <!-- omit in toc -->

##### Requête <!-- omit in toc -->

```sql
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
SELECT
  f.film_id,
  title,
  NAME category_name
FROM
  film f
INNER JOIN 
  film_category fc
ON 
  fc.film_id = f.film_id
INNER JOIN 
  category c
ON 
  c.category_id = fc.category_id
ORDER BY
  title;
```

##### Plan d'exécution <!-- omit in toc -->

```less
QUERY PLAN                                                                                                 
---------------------------------------------------------------------------------------------------------- 
Sort  (cost=184.03..186.53 rows=1000 width=51) (actual time=2.344..2.471 rows=1000 loops=1)                
  Output: f.film_id, f.title, c.name                                                                       
  Sort Key: f.title                                                                                        
  Sort Method: quicksort  Memory: 103kB                                                                    
  Buffers: shared hit=62                                                                                   
  ->  Hash Join  (cost=112.92..134.20 rows=1000 width=51) (actual time=0.683..1.763 rows=1000 loops=1)     
        Output: f.film_id, f.title, c.name                                                                 
        Inner Unique: true                                                                                 
        Hash Cond: (fc.category_id = c.category_id)                                                        
        Buffers: shared hit=62                                                                             
        ->  Hash Join  (cost=77.50..96.14 rows=1000 width=23) (actual time=0.653..1.370 rows=1000 loops=1) 
              Output: f.film_id, f.title, fc.category_id                                                   
              Inner Unique: true                                                                           
...
```

##### Schéma d'exécution ([Dalibo](https://explain.dalibo.com/)) <!-- omit in toc -->

![execution plan](assets/images/psql_dalibo.png "Execution plan")

## Instruction `WITH`

En développement, il est courant de décomposer un programme en plus petites unités. Ceci rends votre code réutilisable et améliore sa lisibilité.

En SQL, les procédures stockées, les vues et l'instruction `WITH` offrent également cette possibilité.

L'instruction `WITH` crée une table temporaire appelée `CTE` _(`Common Table Expression`)_ qui peut être utilisée pour effectuer une autre requête de type `SELECT`, `INSERT`, `UPDATE` ou `DELETE`.

>:warning: **_WARNING_**
>
> Le résultat d'une clause `WITH` est traité comme une vue. Bien qu'étant optimisé depuis la version 12 de `PostgreSQL`, ceci peut occasionner des soucis de performance.

### Syntaxe

```sql
WITH cte_name (column_list) AS (
    CTE_query_definition 
)
statement;
```

### Exemples <!-- omit in toc -->

```sql
-- Odyssee PP et PU
WITH
  DS_PU AS
  ( SELECT
      prime_unique.pol_id,
      prime_unique.mupo_gen,
      SUM(prime_unique.pri_an) AS pri_an
    FROM
      odyssee.prime_unique
    WHERE
      prime_unique.pol_id = 484369
    GROUP BY
      1,
      2
  )
SELECT
  prime_periodique.pol_id,
  SUM(prime_periodique.pri_an),
  DS_PU.pri_an
FROM
  odyssee.prime_periodique
LEFT OUTER JOIN
  DS_PU 
ON 
  ( 
    prime_periodique.pol_id = DS_PU.pol_id
  AND prime_periodique.mupo_gen = DS_PU.mupo_gen)
WHERE
  prime_periodique.pol_id = 484369
AND prime_periodique.mupo_gen = 20
GROUP BY
  1,
  3;
```

```sql
-- Odyssee mut max
WITH 
  DS_mupomax AS
  ( SELECT
      MP2.pol_id,
      MAX(MP2.mupo_gen) AS max_mupo_gen
    FROM
      odyssee.mut_pol AS MP2
    WHERE
      MP2.etat != 'SIMU'
    GROUP BY
      MP2.pol_id
  )
SELECT
  MP1.pol_id,
  MP1.mupo_gen
FROM
  odyssee.mut_pol AS MP1
INNER JOIN 
  DS_mupomax 
ON 
  ( 
    DS_mupomax.pol_id = MP1.pol_id 
  AND DS_mupomax.max_mupo_gen = MP1.mupo_gen);
```

## Grouper le résultat de plusieurs `SELECT`

### `UNION` et `UNION ALL`

Concatène les résultats obtenus par 2 requêtes.

Pour l'utiliser, il est nécessaire que chacune des requêtes à concaténer retournes le même nombre de colonnes, avec les mêmes types de données et dans le même ordre.

> `UNION ALL` affiche les doublons ce qui n'est pas le cas avec `UNION`

![union](https://sql.sh/wp-content/uploads/2013/01/sql-ensemble-union-300.png "UNION")

#### Exemples <!-- omit in toc -->

```sql
SELECT
  id,
  nom
FROM
  partner.partenaire
 
UNION
 
SELECT
  id,
  prenom
FROM
  partner.partenaire_physique;
```

```sql
-- Odyssee PP et PU
SELECT
  prime_unique.pol_id,
  prime_unique.mupo_gen,
  SUM(prime_unique.pri_an) AS pri_uni,
  'PU'                     AS type_prime
FROM
  odyssee.prime_unique
WHERE
  prime_unique.pol_id = 484369
AND prime_unique.puni_type = 'CUMU'
GROUP BY
  1,
  2,
  4
 
UNION
 
SELECT
  prime_periodique.pol_id,
  prime_periodique.mupo_gen,
  SUM(prime_periodique.pri_an) AS pri_per,
  'PP'                         AS type_prime
FROM
  odyssee.prime_periodique
WHERE
  prime_periodique.pol_id = 484369
GROUP BY
  1,
  2,
  4
ORDER BY
  1,
  2,
  4;
```

### `INTERSECT`

Permet d'obtenir l'intersection de 2 requêtes. Cela permet de récupérer les enregistrements communs à 2 requêtes ce qui peut s'avérer utile lorsqu'il faut trouver s'il y a des données similaires sur 2 tables distinctes.

Pour l'utiliser, il est nécessaire que chacune des requêtes à concaténer retournes le même nombre de colonnes, avec les mêmes types de données et dans le même ordre.

![intersect](https://sql.sh/wp-content/uploads/2013/02/sql-ensemble-intersect-300.png "INTERSECT")

#### Exemples <!-- omit in toc -->

```sql
SELECT
  id,
  nom
FROM
  partner.partenaire
 
INTERSECT
 
SELECT
  id,
  prenom
FROM
  partner.partenaire_physique;
```

### `EXCEPT`

Récupère les enregistrements de la première instruction sans inclure les résultats de la seconde.

Pour l'utiliser, il est nécessaire que chacune des requêtes à concaténer retournes le même nombre de colonnes, avec les mêmes types de données et dans le même ordre.

![except](https://sql.sh/wp-content/uploads/2013/03/sql-ensemble-minus-300.png "EXCEPT")

#### Exemples <!-- omit in toc -->

```sql
SELECT
  id,
  nom
FROM
  partner.partenaire
 
EXCEPT
 
SELECT
  id,
  prenom
FROM
  partner.partenaire_physique;
```

# Jointures

Permet d'obtenir des résultats qui associent les données de plusieurs tables dans une même requête.

> :pencil2: **_TIP_**
>
> L'instruction de comparaison `ON` peut être remplacée par `USING (nom_champ)` si les colonnes à comparer sont identiques dans les 2 tables.
>
> `ON staff.address_id = address.address_id` == `USING (address_id)`

## `INNER JOIN` ou `JOIN`

Retourne tous les enregistrements qui ont au moins une ligne dans chaque colonne qui correspondent à la condition

![inner join](https://sql.sh/wp-content/uploads/2013/02/sql-ensemble-intersect-300.png "INNER JOIN")

### Exemples <!-- omit in toc -->

```sql
SELECT
  *
FROM
  partner.partenaire
INNER JOIN
  partner.partenaire_physique
ON
  partenaire.id = partenaire_physique.id;
```

```sql
SELECT
  *
FROM
  partner.partenaire
JOIN
  partner.partenaire_physique
USING(id);
```

```sql
SELECT
  *
FROM
  iliade.mut_dossier
JOIN
  iliade.dossier_gen
USING(dossier_gen, no_gen_dossier);
```

## `LEFT JOIN`

Retourne tous les enregistrements de la table de gauche même s'il n'y a pas de correspondance dans la table de droite.

![left join](https://sql.sh/wp-content/uploads/2013/03/sql-left-join-300.png "LEFT JOIN")

### Exemples <!-- omit in toc -->

```sql
SELECT
  *
FROM
  partner.relation_affaire
LEFT JOIN
  produit.prod
ON
  relation_affaire.produit_id = prod.pro_id;
```

## `RIGHT JOIN`

Retourne tous les enregistrements de la table de droite même s'il n'y a pas de correspondance dans la table de gauche.

![right join](https://sql.sh/wp-content/uploads/2013/03/sql-right-join-300.png "RIGHT JOIN")

### Exemples <!-- omit in toc -->

```sql
SELECT
  *
FROM
  partner.relation_affaire
RIGHT JOIN
  produit.prod
ON
  relation_affaire.produit_id = prod.pro_id;
```

## `FULL JOIN`

Combine le résultat des 2 tables. Les éléments non trouvés sont remplacés par des données `null`.

![full join](https://sql.sh/wp-content/uploads/2013/01/sql-ensemble-union-300.png "FULL JOIN")

### Exemples <!-- omit in toc -->

```sql
SELECT
  *
FROM
  partner.relation_affaire
FULL JOIN
  produit.prod
ON
  relation_affaire.produit_id = prod.pro_id;
```

## `NATURAL JOIN`

Effectue une jointure "naturelle" entre 2 tables. Cette jointure s'effectue à la condition qu'il y ai des colonnes du même nom et de même type dans les 2 tables. Le résultat d'une jointure naturelle est la création d'un tableau avec autant de lignes qu'il y a de paires correspondant à l'association des colonnes de même nom.

![natural join](https://sql.sh/wp-content/uploads/2013/02/sql-ensemble-intersect-300.png "NATURAL JOIN")

### Exemples <!-- omit in toc -->

```sql
SELECT
  *
FROM
  partner.partenaire
NATURAL JOIN
  partner.adresse;
```

Le résultat de cette requête s'obtiendrait de la manière suivante avec un `INNER JOIN` :

```sql
SELECT
  *
FROM
  partner.partenaire
INNER JOIN
  partner.adresse
ON
  partenaire.id = adresse.id
AND partenaire.numero = adresse.numero
AND partenaire.version = adresse.version;
```

# Sous-requêtes

Permet d'exécuter une requête à l'intérieur d'une autre requête. (encapsulation)

## Sous-requête basée sur un seul résultat

La sous-requête retourne 1 seul résultat qui est utilisé dans la requête "appelante".

### Exemples <!-- omit in toc -->

``` sql
SELECT
  *
FROM
  odyssee.role_pol
WHERE
  role_pol.part_id =
  ( SELECT
      id
    FROM
      partner.partenaire
    WHERE
      nom = 'Rapaz'
    LIMIT 
      1);
```

## Sous-requête basée sur une colonne

La sous-requête retourne 1 seule colonne qui est utilisée dans la requête "appelante".

### Exemples <!-- omit in toc -->

```sql
SELECT
  id,
  prenom
FROM
  partner.partenaire_physique
WHERE
  id IN
  ( SELECT
      id
    FROM
      partner.partenaire
    WHERE
      nom = 'Rapaz'
    AND prenom = 'Pascal'
    AND partenaire.etat_generation = 'ACTIF');
```

## Sous-requête basée sur une table

La sous-requête retourne une table qui sera ensuite utilisée dans une jointure.

### Exemples <!-- omit in toc -->

```sql
SELECT
  contrat_id,
  statut
FROM
  iliade.mut_contrat
NATURAL JOIN
  (
    -- Retourne une table, appelée 'resultat', contenant 2 colonnes.
    -- Les 2 colonnes doivent porter le même nom que les champs de la table 'mut_contrat' pour
    -- qu'elles puissent être utilisées dans la jointure ('NATURAL JOIN')
    SELECT
      iliade.contrat.contrat_id,
      -- Renomme la colonne 'max' en 'no_gen_contrat' afin qu'elle puisse être utilisée dans la 
      -- jointure
      MAX(iliade.mut_contrat.no_gen_contrat) AS no_gen_contrat
    FROM
      iliade.contrat
    INNER JOIN
      iliade.mut_contrat
    ON
      iliade.contrat.contrat_id = iliade.mut_contrat.contrat_id
    WHERE
      iliade.contrat.pro_id = '86'
    GROUP BY
      -- Nomme la table temporaire 'resultat'
      iliade.contrat.contrat_id) AS resultat
GROUP BY
  contrat_id,
  statut;
```

## `EXISTS`

`EXISTS` est un opérateur booléen qui vérifie l'existence de lignes dans le résultat d'une sous-requête.

Si la sous-requête retourne au moins une ligne, le résultat sera `true`.

```sql
-- Odyssee mupo_gen avec PU
SELECT
  MP.pol_id,
  MP.mupo_gen
FROM
  odyssee.mut_pol AS MP
WHERE
  EXISTS
  ( SELECT
      1
    FROM
      odyssee.prime_unique AS PU
    WHERE
      PU.pol_id = MP.pol_id 
    AND PU.mupo_gen = MP.mupo_gen )
ORDER BY
  1,
  2;
```

```sql
-- Odyssee EXISTS mut max
SELECT
  MP1.pol_id,
  MP1.mupo_gen
FROM
  odyssee.mut_pol AS MP1
WHERE
  EXISTS
  ( SELECT
      1
    FROM
      odyssee.mut_pol AS MP2
    WHERE
      MP1.pol_id = MP2.pol_id
    GROUP BY
      MP2.pol_id
    HAVING
      MAX(MP2.mupo_gen) = MP1.mupo_gen);
```

# Gestion des tables

## `CREATE TABLE`

### Syntaxe

```sql
CREATE TABLE [IF NOT EXISTS] nom_table (
column1 datatype(length) column_contraint,
column2 datatype(length) column_contraint,
...
table_constraints
);
```

| Instruction                                  | Description                                                                                                                                  |
| -------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| `CREATE TABLE [IF NOT EXISTS] nom_table (`   | Si le paramètre `IF NOT EXISTS` n'est pas spécifié, une erreur est levée si une table portant le même nom existe                             |
| `column1 datatype(length) column_contraint,` | Chaque colonne se compose du nom de la colonne, du type de données de la colonne, de la longueur des données et de la contrainte de colonne. |
| `column2 datatype(length) column_contraint,` |                                                                                                                                              |
| `table_constraints`                          | Spécifie les contraintes de table : la clé primaire, la clé étrangère et les contraintes de vérification                                     |
| `);`                                         |                                                                                                                                              |

#### Contraintes

`PostgreSQL` supporte les contraintes suivantes :

| Type          | Description                                                                                                                                                                                                                                                                                                                     |
| ------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `CHECK`       | Une contrainte `CHECK` garantit que les données doivent satisfaire une expression booléenne                                                                                                                                                                                                                                     |
| `NOT NULL`    | Garantit que les valeurs d'une colonne ne peuvent pas être `NULL`                                                                                                                                                                                                                                                               |
| `UNIQUE`      | Garantit que les valeurs d'une colonne sont uniques sur toutes les lignes d'une même table                                                                                                                                                                                                                                      |
| `PRIMARY KEY` | Une colonne de clé primaire identifie de manière unique les lignes d'une table. Une table peut avoir une et une seule clé primaire. La contrainte de clé primaire permet de définir la clé primaire d'une table.                                                                                                                |
| `FOREIGN KEY` | Garantit que les valeurs d'une colonne ou d'un groupe de colonnes d'une table existent dans une colonne ou un groupe de colonnes d'une autre table. Contrairement à la clé primaire, une table peut avoir plusieurs clés étrangères.                                                                                            |
| `EXCLUDE`     | Garantit que si deux lignes sont comparées sur les colonnes ou expressions spécifiées à l'aide des opérateurs spécifiés, au moins une de ces comparaisons d'opérateurs retournera `false` ou `NULL` (cf : [What does exclusion constraint `EXCLUDE USING gist (c WITH &&)` mean?](https://stackoverflow.com/a/51247705/226061)) |

> :pencil2: **_NOTES_**
>
> Les contraintes de table sont similaires aux contraintes de colonne, sauf qu'elles sont appliquées à plusieurs colonnes.

##### Exemples <!-- omit in toc -->

```sql
CREATE TABLE
  accounts
  (
    user_id SERIAL PRIMARY KEY,
    username   VARCHAR ( 50 ) UNIQUE NOT NULL,
    PASSWORD   VARCHAR ( 50 ) NOT NULL,
    email      VARCHAR ( 255 ) UNIQUE NOT NULL,
    age        INT CHECK(age >= 18 ),
    created_on TIMESTAMP NOT NULL,
    last_login TIMESTAMP
  );
```

```sql
CREATE TABLE 
  roles 
  (
    role_id SERIAL PRIMARY KEY,
    role_name VARCHAR (255) UNIQUE NOT NULL
  );
```

```sql
CREATE TABLE 
  account_roles 
  (
    user_id    INT NOT NULL,
    role_id    INT NOT NULL,
    grant_date TIMESTAMP,
    PRIMARY KEY (user_id, role_id),
    FOREIGN KEY (role_id) REFERENCES roles (role_id),
    FOREIGN KEY (user_id) REFERENCES accounts (user_id)
  );
```

```sql
CREATE TABLE 
  products 
  (
    product_no       INT,
    NAME             VARCHAR ( 50 ),
    price            INT CHECK (price > 0),
    discounted_price INT CHECK (discounted_price > 0),
    CHECK (price > discounted_price)
  );
```

```sql
CREATE TABLE
  copy_actor AS
  ( SELECT
      *
    FROM
      actor
  );
```

### Séquences

#### Auto-increment (`SMALLSERIAL`, `SERIAL`, `BIGSERIAL`)

Les mots clés `SMALLSERIAL`, `SERIAL` et `BIGSERIAL` sont souvent utilisés pour générer automatiquement les identifiants uniques des clés primaires d'une table.

Le choix du mot clé dépendra de la taille estimée de la future table :

| Nom           | Espace de stockage | Plage                         |
| ------------- | ------------------ | ----------------------------- |
| `SMALLSERIAL` | 2 bytes            | 1 à 32'767                    |
| `SERIAL`      | 4 bytes            | 1 à 2'147'483'647             |
| `BIGSERIAL`   | 8 bytes            | 1 à 9'223'372'036'854'775'807 |

Lors d'une insertion, vous pouvez soit ignorer la colonne contenant la séquence soit utiliser le mot clé `DEFAULT` comme valeur pour la colonne contenant la séquence. De plus, vous pouvez récupérer le numéro de séquence utilisé lors de l'insertion avec le mot clé `RETURN`.

Enfin, la fonction `pg_get_serial_sequence('table_name', 'column_name')` permet de récupérer le nom d'une séquence définie sur une table, la fonction `currval('sequence_name')` retourne la dernière valeur et la fonction `nextval('sequence_name')` retourne la prochaine valeur d'une séquence.

##### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Action</th>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>
  
  `CREATE TABLE`
  </td>
  <td>

  ```sql
  CREATE TABLE 
    fruits 
    (
      id SERIAL PRIMARY KEY,
      NAME VARCHAR NOT NULL
    );
  ```

  </td>
  <td></td>
</tr>
<tr>
  <td>
  
  `INSERT INTO`
  </td>
  <td>

  ```sql
  INSERT INTO
    fruits
    (
      NAME
    )
    VALUES
    (
      'Orange'
    );

  -- ou

  INSERT INTO
    fruits
    (
      id,
      NAME
    )
    VALUES
    (
      DEFAULT,
      'Orange'
    );
  ```

  </td>
  <td></td>
</tr>
<tr>
  <td>Nom de la séquence</td>
  <td>

  ```sql
  SELECT
    pg_get_serial_sequence('fruits', 'id');
  ```

  </td>
  <td>

  ```less
  pg_get_serial_sequence 
  ---------------------- 
  public.fruits_id_seq   
  ```

  </td>
</tr>
<tr>
  <td>Valeur actuelle de la séquence</td>
  <td>

  ```sql
  SELECT
    currval(pg_get_serial_sequence('fruits', 'id'));
  ```

  </td>
  <td>

  ```less
  currval 
  ------- 
  2       
  ```

  </td>
</tr>
<tr>
  <td>
  Prochaine valeur de la séquence
  
  >:warning: **_WARNING_**
  >
  > `nextval()` consomme le numéro

  </td>
  <td>

  ```sql
  SELECT
    nextval(pg_get_serial_sequence('fruits', 'id'));
  ```

  </td>
  <td>

  ```less
  nextval 
  ------- 
  3       
  ```

  </td>
</tr>
</table>

#### `CREATE SEQUENCE`

Il est également possible de créer vos propre séquences avec l'instruction `CREATE SEQUENCE`.

##### Syntaxe

| Instruction                                       | Description                                                                                                                                                                                                                                                                           |
| ------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `CREATE SEQUENCE [ IF NOT EXISTS ] sequence_name` |                                                                                                                                                                                                                                                                                       |
| `[ AS { SMALLINT \| INT \| BIGINT } ]`            | c.f. section [Les entiers](#les-entiers) (défaut `BIGINT`)                                                                                                                                                                                                                            |
| `[ INCREMENT [ BY ] increment ]`                  | Un nombre positif formera une séquence ascendante tandis qu'un nombre négatif formera une séquence descendante                                                                                                                                                                        |
| `[ MINVALUE minvalue \| NO MINVALUE ]`            |                                                                                                                                                                                                                                                                                       |
| `[ MAXVALUE maxvalue \| NO MAXVALUE ]`            |                                                                                                                                                                                                                                                                                       |
| `[ START [ WITH ] start ]`                        | Définit la valeur de départ de la séquence                                                                                                                                                                                                                                            |
| `[ CACHE cache ]`                                 | Détermine combien de numéros de séquence sont préalloués et stockés en mémoire pour un accès plus rapide. Par défaut, la séquence génère une valeur à la fois, c'est-à-dire aucun cache                                                                                               |
| `[ [ NO ] CYCLE ]`                                | Redémarre le compteur si la limite est atteinte. Le nombre suivant sera la valeur minimale pour la séquence ascendante et la valeur maximale pour la séquence descendante. Si vous utilisez `NO CYCLE` une erreur sera levée si vous tentez d'obtenir une valeur. (Défaut `NO CYCLE`) |
| `[ OWNED BY { table_name.column_name \| NONE } ]` | Permet d'associer la colonne de la table à la séquence de sorte que lorsque vous supprimez la colonne ou la table, `PostgreSQL` supprime automatiquement la séquence associée                                                                                                         |

##### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Action</th>
  <th>Requête</th>
</tr>
<tr>
  <td>Créer une séquence avec un cache</td>
  <td>

  ```sql
  CREATE SEQUENCE mysequence 
  CACHE 1000;
  ```

  </td>
</tr>
<tr>
  <td>Créer une séquence associée à une table</td>
  <td>

  ```sql
  CREATE TABLE 
    order_details 
    (
      order_id SERIAL,
      item_id   INT NOT NULL,
      item_text VARCHAR NOT NULL,
      price     DEC(10,2) NOT NULL,
      PRIMARY KEY(order_id, item_id)
    );

  CREATE SEQUENCE order_item_id
  START 10
  INCREMENT 10
  CACHE 1000
  OWNED BY order_details.item_id;
  ```

  </td>
</tr>
</table>

#### `DROP SEQUENCE`

Il est également possible de supprimer une séquence avec l'instruction `DROP SEQUENCE`.

##### Syntaxe

| Instruction                                         | Description                                                                                                                                                                        |
| --------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `DROP SEQUENCE [ IF EXISTS ] sequence_name [, ...]` |                                                                                                                                                                                    |
| `[ CASCADE \| RESTRICT ];`                          | L'option `CASCADE` supprime de manière récursive les objets qui dépendent de la séquence, et les objets qui dépendent des objets dépendants et ainsi de suite (Defaut: `RESTRICT`) |

## `ALTER` table

### Syntaxe

```sql
ALTER TABLE table_name action;
```

| Instruction                      | Description                                                     |
| -------------------------------- | --------------------------------------------------------------- |
| `ALTER TABLE table_name action;` | Change la structure d'une table en fonction de l'action choisie |

#### Actions

`PostgreSQL` supporte les actions suivantes :

<table>
<tr>
  <th>Action</th>
  <th>Description</th>
  <th>Syntaxe</th>
</tr>
<tr>
  <td>
  
  `ADD COLUMN`
  </td>
  <td>Ajoute une colonne à la table</td>
  <td>

  ```sql
  ALTER TABLE 
    table_name ADD COLUMN COLUMN_NAME datatype column_constraint;
  ```

  </td>
</tr>
<tr>
  <td>
  
  `DROP COLUMN`
  </td>
  <td>Supprime une colonne à la table</td>
  <td>

  ```sql
  ALTER TABLE 
    table_name DROP COLUMN COLUMN_NAME datatype column_constraint;
  ```
  
  </td>
</tr>
<tr>
  <td>
  
  `RENAME COLUMN`
  </td>
  <td>Renomme une colonne dans la table</td>
  <td>

  ```sql
  ALTER TABLE 
    table_name RENAME COLUMN COLUMN_NAME TO new_column_name;
  ```
  
  </td>
</tr>
<tr>
  <td>
  
  `ALTER COLUMN`
  </td>
  <td>Change le type d'une colonne dans la table</td>
  <td>

  ```sql
  ALTER TABLE 
    table_name ALTER COLUMN COLUMN_NAME TYPE new_data_type; 
  ```

  </td>
</tr>
<tr>
  <td>Valeur par défaut</td>
  <td>Changer ou supprimer une valeur par défaut d'une colonne dans une table</td>
  <td>

  ```sql
  ALTER TABLE 
    table_name ALTER COLUMN COLUMN_NAME SET DEFAULT value;
  ```
  
  ```sql
  ALTER TABLE 
    table_name ALTER COLUMN COLUMN_NAME DROP DEFAULT;
  ```

  </td>
</tr>
<tr>
  <td>Modifier une contrainte</td>
  <td>Changer ou supprimer une contrainte sur les colonnes d'une table</td>
  <td>

  ```sql
  ALTER TABLE 
    table_name ALTER COLUMN COLUMN_NAME SET NOT NULL;
  ```
  
  ```sql
  ALTER TABLE 
    table_name ALTER COLUMN COLUMN_NAME DROP NOT NULL;
  ```

  </td>
</tr>
<tr>
  <td>Modifier une clé</td>
  <td>Ajouter ou supprimer une clé primaire (`PK`) ou un clé étrangère (`FK`)</td>
  <td>

  ```sql
  ALTER TABLE 
    table_name ADD PRIMARY KEY (column_name)

  ALTER TABLE 
    table_name ADD CONSTRAINT fk_name FOREIGN KEY (column_name) 
    REFERENCES fk_table_name(fk_column_name);
  ```
  
  ```sql
  ALTER TABLE 
    table_name DROP CONSTRAINT key_name;
  ```

  </td>
</tr>
<tr>
  <td>
  
  `RENAME TO`
  </td>
  <td>Renommer une table</td>
  <td>

  ```sql
  ALTER TABLE 
    IF EXISTS table_name RENAME TO new_table_name;
  ```
  
  </td>
</tr>
</table>

## `DROP` table

### Syntaxe

```sql
DROP TABLE [IF EXISTS] table_name
[CASCADE | RESTRICT];
```

<table>
<tr>
  <th>Instruction</th>
  <th>Description</th>
</tr>
<tr>
  <td>
  
  `DROP TABLE [IF EXISTS] table_name`
  </td>
  <td>
  
  Si le paramètre `IF EXISTS` n'est pas spécifié, une erreur est levée si la table à supprimer n'existe pas
  </td>
</tr>
<tr>
  <td>
  
  `[CASCADE | RESTRICT];`
  </td>
  <td>

- l'option `CASCADE` permet de supprimer la table et toutes ses dépendances
- l'option `RESTRICT` rejette la suppression si un objet dépend de la table

  > :pencil2: **_INFO_**
  >
  > L'option `RESTRICT` est la valeur par défaut si vous ne la spécifiez pas explicitement dans l'instruction `DROP TABLE`

</td>
</tr>
</table>

# Modification des données

## `INSERT`

L'instruction `INSERT` permet d'insérer des données dans une table.

### Syntaxe

```sql
INSERT INTO table_name(column1, column2, etc.)
VALUES (value1, value2, etc.);
```

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Description</th>
  <th>Requête</th>
</tr>
<tr>
  <td>Insère une ligne unique</td>
  <td>

  ```sql
  INSERT INTO city 
    (city_id, city, country_id, last_update) 
  VALUES 
    (DEFAULT, 'Monthey', 91, now());
  ```

  </td>
</tr>
<tr>
  <td>Insère plusieurs lignes</td>
  <td>

  ```sql
  INSERT INTO city 
    (city_id, city, country_id, last_update) 
  VALUES 
    (DEFAULT, 'Val-d''Illiez', 91, now()),
    (DEFAULT, 'Lausanne', 91, now());
  ```

  </td>
</tr>
<tr>
  <td>

  Insère le résultat d'un `SELECT`</td>
  <td>

  ```sql
  -- Create sample table
  DROP TABLE IF EXISTS organized_info;
  CREATE TABLE organized_info(
      id SERIAL PRIMARY KEY,
      title VARCHAR(100),
      rating VARCHAR(50),
      film_length INT
  );

  -- Insert into select
  INSERT INTO 
    organized_info 
    ( 
      title, 
      rating, 
      film_length 
    ) 
  SELECT 
    title, 
    rating, 
    LENGTH 
  FROM 
    film;
  ```

  </td>
</tr>
</table>

### `RETURNING`

La clause optionnelle `RETURNING` retourne les lignes fraîchement insérées.

#### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Description</th>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>Retourne la ligne insérée</td>
  <td>

  ```sql
  INSERT INTO city
    (city_id, city, country_id, last_update)
  VALUES
    (DEFAULT, 'Monthey', 91, now())
  RETURNING *;
  ```

  </td>
  <td>

  ```less
  city_id city    country_id last_update         
  ------- ------- ---------- ------------------- 
  604     Monthey 91         2021-11-29 16:54:21 
  ```

  </td>
</tr>
<tr>
  <td>Retourne l'identifiant des lignes insérées</td>
  <td>

  ```sql
  INSERT INTO
  city
  (city_id, city, country_id, last_update)
  VALUES
    (DEFAULT, 'Val-d''Illiez', 91, now()),
    (DEFAULT, 'Lausanne', 91, now())
  RETURNING city_id;
  ```

  </td>
  <td>

  ```less
  city_id 
  ------- 
  605
  606     
  ```

  </td>
</tr>
</table>

## `UPDATE`

L'instruction `UPDATE` permet de mettre à jour des données dans une table.

### Syntaxe

```sql
UPDATE table_name
SET column1 = value1,
    column2 = value2,
    ...
WHERE condition;
```

> :skull: **_WARNING_**
>
> La clause `WHERE` est facultative. Si vous l'omettez, l'instruction `UPDATE` mettra à jour **toutes** les lignes de la table.
>
> Afin d'éviter ce type de désagrément, il est fortement conseillé de faire un `SELECT` pour valider le périphérique concerné par l'instruction `UPDATE` avant toute exécution.

### `RETURNING`

La clause optionnelle `RETURNING` retourne les éléments modifiés.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Description</th>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>Mise à jour (sans utilisation du retour)</td>
  <td>

  ```sql
  UPDATE 
    film
  SET 
    release_year = '2021'
  WHERE 
    rental_duration > 5;
  ```

  </td>
  <td></td>
</tr>
<tr>
  <td>
  
  Mise à jour avec utilisation de `RETURNING`</td>
  <td>

  ```sql
  UPDATE
    film
  SET
    release_year = '2019'
  WHERE
    rental_duration <= 5 
  RETURNING film_id;
  ```

  </td>
  <td>

  ```less
  film_id 
  ------- 
  2       
  4       
  6       
  9       
  ...  
  ```
  
  </td>
</tr>
<tr>
  <td>
  
  Création d'une table temporaire puis mise à jour des champs `ID`</td>
  <td>

  ```sql
  CREATE TABLE 
    biext.eq_ext_offres_finale_ayf AS
    ( SELECT
        *
      FROM
        biext.eq_ext_offres_finale_bk20211130
    );

  UPDATE
    biext.eq_ext_offres_finale_ayf
  SET
    produit_id = RA.produit_id,
    offre_id = RA.metier_id,
    numero_offre = RA.numero_metier
  FROM
    partner.relation_affaire AS RA
  WHERE
    biext.eq_ext_offres_finale_ayf.relation_affaire_id > 0
  AND biext.eq_ext_offres_finale_ayf.produit_id = 0
  AND biext.eq_ext_offres_finale_ayf.relation_affaire_id = RA.id 
  RETURNING *;
  ```

  </td>
  <td></td>
</tr>
</table>

## `UPDATE JOIN`

Parfois, vous devez mettre à jour les données d'une table en fonction des valeurs d'une autre table. Dans ce cas, vous pouvez utiliser une [jointure](#jointures).

### Syntaxe

```sql
UPDATE table1
  SET table1.col_1 = new_value
FROM table2
WHERE table1.col_2 = table2.col_2;
```

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  -- Augmente de 30% le montant selon certaines conditions
  UPDATE
    payment
  SET
    amount = amount + amount*0.3
  FROM
    customer
  WHERE
    payment.customer_id = customer.customer_id
  AND payment.amount > 9
  AND last_name LIKE 'A%' 
  RETURNING 
    last_name, 
    amount;
  ```

  </td>
  <td>

  ```less
  last_name amount 
  --------- ------ 
  ANDERSON  12.99  
  ADAMS     12.99  
  ARNOLD    12.99  
  AUSTIN    15.59  
  AUSTIN    14.29  
  ALVAREZ   12.99  
  ...
  ```

  </td>
</tr>
</table>

## `DELETE`

L'instruction `DELETE` permet de supprimer des enregistrements dans une table.

### Syntaxe

```sql
DELETE FROM table_name
WHERE condition;
```

> :skull: **_WARNING_**
>
> La clause `WHERE` est facultative. Si vous l'omettez, l'instruction `DELETE` supprimera **toutes** les lignes de la table.
>
> Afin d'éviter ce type de désagrément, il est fortement conseillé de faire un `SELECT` pour valider le périphérique concerné par l'instruction `DELETE` avant toute exécution.

### `RETURNING`

La clause optionnelle `RETURNING` retourne les lignes supprimées.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Description</th>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>
  
  Suppression avec une clause `WHERE`</td>
  <td>

  ```sql
  DELETE 
  FROM 
    organized_info
  WHERE 
    id BETWEEN 1 AND 15;
  ```

  </td>
  <td></td>
</tr>
<tr>
  <td>
  
  Récupération des données supprimées avec `RETURNING`</td>
  <td>

  ```sql
  DELETE 
  FROM 
    organized_info
  WHERE 
    id = 16
  RETURNING *;
  ```

  </td>
  <td>

  ```less
  id title           rating film_length 
  -- --------------- ------ ----------- 
  16 ALLEY EVOLUTION NC-17  180         
  ```
  
  </td>
</tr>
<tr>
  <td>Suppression de toutes les enregistrements de la table</td>
  <td>

  ```sql
  DELETE 
  FROM 
    organized_info;
  ```

  </td>
  <td></td>
</tr>
<tr>
  <td>
  
  Suppression avec sous `SELECT`</td>
  <td>

  ```sql
  DELETE
  FROM
    payment
  WHERE
    payment.staff_id IN
    ( SELECT
        staff_id
      FROM
        staff
      WHERE
        store_id = 2)
  RETURNING *;
  ```

  </td>
  <td>
  
  ```less
  payment_id customer_id staff_id rental_id amount payment_date        
  ---------- ----------- -------- --------- ------ ------------------- 
  4          1           2        1422      0.99   2005-06-15 18:02:53 
  5          1           2        1476      9.99   2005-06-15 21:08:46 
  8          1           2        2363      0.99   2005-06-18 13:33:59 
  ...
  ```
  
  </td>
</tr>
</table>

## Upsert (`INSERT ON CONFLICT`)

Le concept "Upsert" consiste à insérer un enregistrement s'il n'existe pas ou à le mettre à jour en cas contraire.

### Syntaxe

```sql
INSERT INTO table_name(column_list) 
VALUES(value_list)
ON CONFLICT target action;
```

Les cibles possibles sont les suivantes:

| `target`                        | Description                                                                  |
| ------------------------------- | ---------------------------------------------------------------------------- |
| `(column_name)`                 | Le nom d'une colonne                                                         |
| `ON CONSTRAINT constraint_name` | Le nom d'une constrainte [contraintes](#contraintes) (généralement `UNIQUE`) |
| `WHERE condition`               | Une clause `WHERE`                                                           |

Les actions possibles sont les suivantes:

| `action`                                           | Description                                         |
| -------------------------------------------------- | --------------------------------------------------- |
| `DO NOTHING`                                       | Ne rien faire si la ligne existe déjà dans la table |
| `DO UPDATE SET col_1 = val_1, ... WHERE condition` | Mettre à jour les enregistrements                   |

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Description</th>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>
  
  Insère un nouvel enregistrement s'il n'existe pas</td>
  <td>

  ```sql
  -- Ajoute une contrainte d'unicité sur la colonne 'city'
  ALTER TABLE city
  ALTER COLUMN city set UNIQUE;

  INSERT INTO city 
    (city, country_id, last_update) 
  VALUES 
    ('Monthey', 102, now())
  ON CONFLICT (city)
  DO NOTHING;

  INSERT INTO city 
    (city, country_id, last_update) 
  VALUES 
    ('Monthey', 102, now())
  ON CONFLICT ON CONSTRAINT constraint_city
  DO NOTHING;
  ```

  </td>
  <td>

  `OK`

  `OK. No rows were affected`</td>
</tr>
<tr>
  <td>
  
  Mets à jour l'enregistrement s'il existe et retourne l'élément modifié</td>
  <td>

  ```sql
  INSERT INTO city 
      (city, country_id, last_update) 
  VALUES('Monthey', 91, now()) 
  ON CONFLICT (city) 
  DO 
    UPDATE SET 
      city = 'Val-d''Illiez', 
      country_id = 91
      -- Attention le champs date ne sera pas modifié 
      -- car il n'est pas spécifié dans l'instruction
      -- UPDATE SET
  RETURNING *;
  ```

  </td>
  <td>

  ```less
  city_id city         country_id last_update         
  ------- ------------ ---------- ------------------- 
  610     Val-d'Illiez 91         2021-11-30 10:09:08 
  ```
  
  </td>
</tr>
</table>

# Fonctions

## `MAX()`, `MIN()`, `AVG()`, `SUM()`

Les fonctions `MIN()`, `MAX()`, `AVG()` et `SUM()` travaillent sur les nombres afin de retourner les valeurs suivantes :

| Fonction | Valeur retournée                     |
| -------- | ------------------------------------ |
| `MIN()`  | Valeur minimale d'une colonne        |
| `MAX()`  | Valeur maximale d'une colonne        |
| `AVG()`  | Valeur moyenne d'une colonne         |
| `SUM()`  | Calcul la somme totale d'une colonne |

### Exemples <!-- omit in toc -->

```sql
SELECT 
  MAX(prix_vente_operation)
FROM 
  odyssee.epargne;
```

```sql
SELECT 
  MIN(prix_vente_operation)
FROM 
  odyssee.epargne;
```

```sql
SELECT 
  AVG(prix_vente_operation)
FROM 
  odyssee.epargne;
```

```sql
SELECT 
  SUM(prix_vente_operation)
FROM 
  odyssee.epargne;
```

## `COUNT()`

La fonction `COUNT()` permet de compter le nombre d'enregistrements dans une table.

### Exemples <!-- omit in toc -->

```sql
SELECT
  COUNT(1)
FROM
  odyssee.epargne
WHERE
  epargne.prix_vente_operation = 191.41;
```

# String Functions

## Escape char

L'escape char (ou caractère d'échappement) est un caractère qui déclenche une interprétation alternative du ou des caractères qui le suivent.

<table>
<tr>
  <th>Caractère</th>
  <th>Description</th>
  <th>Exemple</th>
</tr>
<tr>
  <td>
  
  `'`
  </td>
  <td>
  Permet d'échapper une apostrophe dans une chaîne de caractères
  </td>
  <td>

  ```sql
  SELECT 
    *
  FROM 
    commune
  WHERE 
    nom LIKE 'Val-d''Illiez'
  ```

  </td>
  <tr>
  <td>
  
  `\`
  </td>
  <td>

  Permet d'échapper un caractère spécial tel quel `_` ou `\`
  </td>
  <td>

  ```sql
  SELECT 
    COLUMN_NAME
  FROM 
    information_schema.columns
  WHERE 
    COLUMN_NAME LIKE 'is\_verified\_by'
  ```

  </td>
</table>

## `CONCAT()`

La fonction `CONCAT()` ou le "double pipe" (`||`) permettent de concaténer les valeurs de plusieurs colonnes pour ne former qu'une seule chaîne de caractères.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT 
    CONCAT(prenom, ' ', nom_naissance)
  FROM 
    partenaire_physique
  WHERE
    nom_naissance is not null
    and nom_naissance != ''
  ```

  ```sql
  SELECT 
    CONCAT(partenaire_physique.prenom, ' ', partenaire.nom)
  FROM
    partenaire,
    partenaire_physique
  WHERE
    partenaire_physique.id = partenaire.id
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT 
    prenom || ' ' || nom_naissance
  FROM 
    partenaire_physique
  WHERE
    nom_naissance is not null
    and nom_naissance != ''
  ```

  </td>
</tr>
</table>

## `SUBSTRING()`, `RIGHT()`, `LEFT()`

La fonction `SUBSTRING()` (ou `SUBSTR()`) retourne une sous-chaîne de la chaîne courante:

| Syntaxe                              | Valeur retournée                                                                                                      |
| ------------------------------------ | --------------------------------------------------------------------------------------------------------------------- |
| `SUBSTRING(chaîne, début)`           | Chaîne de caractères de `chaîne` à partir de la position définie par `début`                                          |
| `SUBSTRING(chaîne, début, longueur)` | Chaîne de caractères `chaîne` en partant de la position définie par `début` et sur la longueur définie par `longueur` |

La fonction `RIGHT()` retourne une sous-chaîne depuis la droite.

La fonction `LEFT()` retourne une sous-chaîne depuis la gauche.

### Exemples <!-- omit in toc -->

```sql
SELECT
  SUBSTR(epargne.nom_fonds, 2)
FROM
  odyssee.epargne

SELECT
  SUBSTR(epargne.nom_fonds, 2, 5)
FROM
  odyssee.epargne;
```

```sql
SELECT
  RIGHT(epargne.nom_fonds, 5)
FROM
  odyssee.epargne;
```

```sql
SELECT
  LEFT(epargne.nom_fonds, 5)
FROM
  odyssee.epargne;
```

## `POSITION()`

`POSITION()` retourne l'emplacement du 1<sup>er</sup> caractère trouvé dans une chaîne.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    POSITION('ami.e.s' in 'Bonjour les ami.e.s');
  ```

  </td>
  <td>
  
  ```less
  position 
  -------- 
  13        
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    address,
    LEFT(address, POSITION(' ' IN address) - 1)
  FROM
    address;
  ```

  </td>
  <td>
  
  ```less
  address              left 
  -------------------- ---- 
  47 MySakila Drive    47   
  28 MySQL Boulevard   28   
  23 Workhaven Lane    23   
  1411 Lillydale Drive 1411 
  1913 Hanoi Way       1913 
  ...
  ```

  </td>
</tr>
<tr>
  <td>

  Récupération du 2<sup>me</sup> mot d'une adresse.

  Certaines adresses de la table partenaire n'ayant qu'un mot, il faut afiner la recherche.

  ```sql
  -- Cette solution limite aux cas qui ont au minimum 2 espaces
  --   La clause `WHERE` compte le nombre d'espaces dans le texte
  SELECT
    voie,
    SUBSTR(
      voie, 
      POSITION(' ' IN voie) + 1, 
      POSITION(' ' IN SUBSTR(
                        voie, 
                        POSITION(' ' IN voie) + 1)) -1)
  FROM
    adresse
  WHERE
    LENGTH(voie) - LENGTH(REPLACE(voie, ' ', '')) > 1;
    
  -- Cette solution prends en compte tous les cas
  --   La clause `CASE WHEN` test s'il y a plusieurs espaces
  SELECT
    voie,
    SUBSTR(
      voie, 
      POSITION(' ' IN voie) + 1, 
      CASE WHEN(
          POSITION(' ' IN SUBSTR(voie, POSITION(' ' IN voie) + 1)) - 1) > 0 
        THEN
          POSITION(' ' IN SUBSTR(voie, POSITION(' ' IN voie) + 1)) - 1 
        ELSE
          LENGTH(voie)
        END
      )
  FROM
    adresse;
  ```

  </td>
  <td />
</tr>
</table>

## `TRIM()`, `RTRIM()`, `LTRIM()`

La fonction `TRIM()` permet de supprimer des caractères au début et en fin d'une chaîne de caractères.

Le plus souvent la fonction `TRIM()` permet de supprimer les caractères invisibles, c'est-à-dire les caractères tels que l'espace, la tabulation, le retour à la ligne ou bien même le retour chariot.

| Syntaxe   | Valeur retournée                                          |
| --------- | --------------------------------------------------------- |
| `TRIM()`  | Supprime les caractères à gauche et à droite de la chaîne |
| `RTRIM()` | Supprime les caractères un fin de chaîne                  |
| `LTRIM()` | Supprime les caractères un début de chaîne                |

```sql
SELECT
  TRIM(epargne.nom_fonds)
FROM
  odyssee.epargne
```

## `UPPER()`, `LOWER()`

La fonction `UPPER()` permet de transformer tous les caractères en majuscule.

La fonction `LOWER()` permet de transformer tous les caractères en minuscule.

> :warning: **_WARNING_**
>
> Les fonctions `upper()` et `lower()` gardent les accents. Ceci a pour conséquence que vos requêtes doivent en tenir compte !
>
> Pour éviter ce souci, `PostgreSQL` met à disposition le module `unaccent` qui se charge de supprimer tous les accents d'une chaîne de caractères.
>
> ```sql
> -- Import du module
> --   à faire une fois dans la base pour l'activer
> CREATE EXTENSION unaccent;
> ```
>
> ```sql
> SELECT unaccent('Hétérogénéité');
> SELECT upper(unaccent('Hétérogénéité'));
> SELECT lower(unaccent('Hétérogénéité'));
> ```

### Exemples <!-- omit in toc -->

```sql
SELECT
  UPPER(epargne.nom_fonds)
FROM
  odyssee.epargne;
```

```sql
SELECT
  LOWER(epargne.nom_fonds)
FROM
  odyssee.epargne;
```

## `LENGTH()`

`LENGTH()` retourne le nombre de caractères dans une chaîne.

### Exemples <!-- omit in toc -->

```sql
SELECT
  LENGTH(address)
FROM
  address;
```

## `INITCAP()`

`INITCAP()` converti la 1<sup>re</sup> lettre de chaque mot en majuscule et le reste en minuscule.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    INITCAP('JEAN-françois ferdinand');
  ```

  </td>
  <td>
  
  ```less
  initcap                 
  ----------------------- 
  Jean-François Ferdinand    
  ```

  </td>
</tr>
</table>

## `REPLACE()`, `TRANSLATE()`

La fonction `REPLACE()` est utilisée pour remplacer une chaîne de caractères par une autre chaîne de caractères.

La fonction `TRANSLATE()` est utilisée pour remplacer n'importe quel caractère d'une chaîne de caractères par un autre caractère.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  Remplace la chaîne de caractères `elo` par `123`

  ```sql
  SELECT
    REPLACE('penelope', 'elo', '123')
  FROM actor;
  ```
  
  </td>
  <td>

  ```less
  replace  
  -------- 
  pen123pe 
  pen123pe 
  ...
  ```

  </td>
</tr>
<tr>
  <td>

  Remplace les caractères `e` par `1`, `l` par `2` et `o` par `3`

  ```sql
  SELECT
    TRANSLATE('penelope', 'elo', '123')
  FROM actor;
  ```
  
  </td>
  <td>

  ```less
  translate 
  --------- 
  p1n123p1  
  p1n123p1  
  ...
  ```

  </td>
</tr>
</table>

## `RPAD()`, `LPAD()`

Les fonctions `RPAD()` et `LPAD()` complètent un chaîne de caractères par une autre chaîne depuis la gauche (`L`) ou depuis la droite (`R`).

> :pencil2: **_INFO_**
>
> Si la chaîne de caractères est plus grande que la taille spécifiée, elle sera tronquée

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    LPAD(first_name, 5, '*')
  FROM
    actor;
  ```
  
  </td>
  <td>

  ```less
  lpad  
  ----- 
  PENEL 
  *NICK 
  ***ED 
  JENNI 
  JOHNN 
  ...
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    RPAD(first_name, 5, '*')
  FROM
    actor;
  ```
  
  </td>
  <td>

  ```less
  rpad  
  ----- 
  PENEL 
  NICK* 
  ED*** 
  JENNI 
  JOHNN 
  ...
  ```

  </td>
</tr>
</table>

## `CHR()`, `ASCII()`

La fonction `CHR()` convertit un code `ASCII entier` en un caractère `UTF8`.

La fonction `ASCII()` convertit un caractère `UTF8` en code `ASCII entier`.

> :pencil2: **_INFO_**
>
> Quelques caractères spéciaux :
>
> | code | valeur                                            |
> | :--: |---------------------------------------------------|
> |  10  | Nouvelle ligne (_new line_ ou _line feed_ '`\n`') |
> |  13  | Retour de chariot (_carriage return_ '`\r`')      |
> |  9   | Tabulation ('`\t`')                               |
> |  32  | Espace                                            |

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    CHR(65),
    CHR(97);
  ```
  
  </td>
  <td>

  ```less
  chr chr 
  --- --- 
  A   a   
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    ASCII('A'),
    ASCII('a');
  ```
  
  </td>
  <td>

  ```less
  ascii ascii 
  ----- ----- 
  65    97    
  ```

  </td>
</tr>
</table>

## Expressions régulières (`REGEX`)

Les expressions régulières ou `regex` permettent d'examiner ou de manipuler du texte en fonction d'une représentation d'un "modèle".

`PostgreSQL` met à disposition plusieurs fonctions permettant d'utiliser le `regex`:

| Fonction                                                 | Description                                                                                         |
| -------------------------------------------------------- | --------------------------------------------------------------------------------------------------- |
| `regexp_matches()`                                       | Renvoie toutes les sous-chaînes capturées résultant de la correspondance d'une expression régulière |
| `regexp_replace()`                                       | Remplace la ou les sous-chaînes correspondant à une expression régulière                            |
| `regexp_split_to_table()`<br />`regexp_split_to_array()` | Divise une chaîne en utilisant une expression régulière                                             |

> :pencil2: **_TIP_**
>
> Le site [RegExr: Learn, Build, & Test RegEx](https://regexr.com/) offre une interface permettant de tester vos expressions régulières

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  Recherche toutes les occurrences qui commencent par un hastag (`#`)

  ```sql
  SELECT
    REGEXP_MATCHES('Utilisation de #REGEXP_MATCHES avec #PostgreSQL',
                   '#([A-Za-z0-9_]+)',
                   'g');
  ```

  </td>
  <td>

  ```less
  regexp_matches
  ------------------ 
  {"REGEXP_MATCHES"} 
  {"PostgreSQL"}     
  ```

  </td>
</tr>
<tr>
  <td>

  Décompose une ligne en fonction des _whitespaces_ (espaces, tabulations, retours de ligne) dans une table

  ```sql
  SELECT
    REGEXP_SPLIT_TO_TABLE('Utilisation de
                           #REGEXP_MATCHES avec #PostgreSQL',
                          '\s+');
  ```

  </td>
  <td>

  ```less
  regexp_split_to_table
  --------------------- 
  Utilisation         
  de                    
  #REGEXP_MATCHES       
  avec                  
  #PostgreSQL           
  ```

  </td>
</tr>
<tr>
  <td>

  Décompose une ligne en fonction des _whitespaces_ (espaces, tabulations, retours de ligne) dans un tableau

  ```sql
  SELECT
    REGEXP_SPLIT_TO_ARRAY('Utilisation de
                           #REGEXP avec #PostgreSQL',
                          '\s+');

  SELECT
    res[3]
  FROM
    REGEXP_SPLIT_TO_ARRAY ('Utilisation de
                            #REGEXP avec #PostgreSQL',
                           '\s+') AS res;
  ```

  </td>
  <td>

  ```less
  regexp_split_to_array
  --------------------------------------------------- 
  {"Utilisation","de","#REGEXP","avec","#PostgreSQL"} 
  ```

  ```less
  res             
  -------
  #REGEXP
  ```

  </td>
</tr>
<tr>
  <td>Remplace les caractères de fin de ligne et retour de chariot par un espace
  
  ```sql
  SELECT
    REGEXP_REPLACE('Texte
sur plusieurs
lignes', 
  E'[\\n\\r]+', ' ', 'g');
  ```

  ```sql
  SELECT
    REGEXP_REPLACE('Texte
sur plusieurs
lignes', 
  '\s+', ' ', 'g');
  ```

  </td>
  <td>

  ```sql
  SELECT
  regexp_replace
  -------------------------- 
  Texte sur plusieurs lignes 
  ```

  ```sql
  regexp_replace             
  -------------------------- 
  Texte sur plusieurs lignes 
  ```

  </td>
</tr>
</table>

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  Suppression d'un texte entre `"guillemets"`

  ```sql
  SELECT
    REGEXP_REPLACE('Le texte à supprimer est entre '
                   '"guillemets", '
                   '''quotes'' ou en '
                   '/* commentaires */ !', 
                   '".*?"+', 
                   '', 
                   'g');
  ```

  </td>
  <td>

  ```less
  regexp_replace                                                      
  -------------------------------------------------------------------- 
  Le texte à supprimer est entre , 'quotes' ou en /* commentaires */ ! 
  ```

  </td>
</tr>
<tr>
  <td>

  Suppression d'un texte entre `'quotes''`

  ```sql
  SELECT
    REGEXP_REPLACE('Le texte à supprimer est entre '
                   '"guillemets", '
                   '''quotes'' ou en '
                   '/* commentaires */ !', 
                   '''.*?''+', 
                   '', 
                   'g');
  ```

  </td>
  <td>

  ```less
  regexp_replace                                                           
  ------------------------------------------------------------------------ 
  Le texte à supprimer est entre "guillemets",  ou en /* commentaires */ ! 
  ```

  </td>
</tr>
<tr>
  <td>

  Suppression d'un bloc de `/*commentaire"/`

  ```sql
  SELECT
    REGEXP_REPLACE('Le texte à supprimer est entre '
                   '"guillemets", '
                   '''quotes'' ou en '
                   '/* commentaires */ !', 
                   '([\/\*]{2,}.*?[\*\/]{2,})+', 
                   '', 
                   'g');
  ```

  </td>
  <td>

  ```less
  regexp_replace                                                 
  -------------------------------------------------------------- 
  Le texte à supprimer est entre "guillemets", 'quotes' ou en  ! 
  ```

  </td>
</tr>
<tr>
  <td>

  Suppression d'un texte entre `"guillemets"`, `'quotes''` et `/*commentaire"/`

  ```sql
  SELECT
    REGEXP_REPLACE(
      REGEXP_REPLACE(
        REGEXP_REPLACE('Le texte à supprimer est entre '
                       '"guillemets", '
                       '''quotes'' ou en '
                       '/* commentaires */ !', 
                       '([\/\*]{2,}.*?[\*\/]{2,})+', 
                       '', 
                       'g'),
        '".*?"+',  
        '', 
        'g'),
    '''.*?''+', 
    '', 
    'g');
  ```

  </td>
  <td>

  ```less
  regexp_replace                             
  ------------------------------------------ 
  Le texte à supprimer est entre ,  ou en  ! 
  ```

  </td>
</tr>
</table>

# Types

## Principaux types

| Type                          | Description                                                                                                                                                                                                                              |
| ----------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `Boolean`                     | Stock les valeurs `true` ou `false`                                                                                                                                                                                                      |
| `CHAR(n)`                     | Chaîne de caractères à longueur fixe (complétée par des espaces)                                                                                                                                                                         |
| `VARCHAR(n)`                  | Chaîne de caractères à longueur variable mais limitée par une taille                                                                                                                                                                     |
| `TEXT()`                      | Chaîne de caractères à longueur variable sans limite de taille                                                                                                                                                                           |
| `NUMERIC([precision, scale])` | Permet de stocker des grands nombres.<br />La `precision` est le nombre total de chiffres et le `scale` est le nombre de chiffres dans la partie fractionnaire. Par exemple, le nombre `1234.567` a la `precision` `7` et le `scale` `3` |
| `INTEGER`                     | Nombre entier codé sur 4 bytes (de `-2,147,483,648` à `+2,147,483,647`)<br />Au besoin, il est également possible d'utiliser les `SMALLINT` codés sur 2 bytes et le `BIGINT` codés sur 8 bytes                                           |
| `DATE`                        | Stock des valeurs de type `date`                                                                                                                                                                                                         |
| `TIME`                        | Stock des valeurs de type `heure`                                                                                                                                                                                                        |
| `TIMESTAMP`                   | Stock des valeurs de type `heure/date`                                                                                                                                                                                                   |
| `UUID`                        | Stock une valeur unique codée sur 128-bit souvent utilisée comme identifiant                                                                                                                                                             |
| `ARRAY`                       | Stock un tableau de valeurs                                                                                                                                                                                                              |
| `JSON`                        | Stock les données au format `json`                                                                                                                                                                                                       |

## Les entiers

| Type       | Espace de stockage | Min                        | Max                        |
| ---------- | ------------------ | -------------------------- | -------------------------- |
| `SMALLINT` | 2 bytes            | -32'768                    | +32'767                    |
| `INT`      | 4 bytes            | -2'147'483'648             | +2'147'483'647             |
| `BIGINT`   | 8 bytes            | -9'223'372'036'854'775'808 | +9'223'372'036'854'775'807 |

## `CAST()`

La fonction `CAST()` permet de convertir une valeur d'un type en un autre type.

### Syntaxe

Il existe deux syntaxes permettant de faire du casting

```sql
CAST(expression AS type);
```

ou

```sql
expression::type;
```

### Exemples <!-- omit in toc -->

```sql
SELECT
  store_id::DOUBLE PRECISION,
  active::BOOLEAN,
  CAST(activebool AS INTEGER)
FROM
  customer;
```

```sql
SELECT
  '100'::                    INTEGER,
  '10.2'::                   DOUBLE PRECISION,
  10.4::                     INTEGER, -- ! 10.4 = 10, 10.5 = 11
  '01-DEC-2021'::            DATE,
  'T'::                      BOOLEAN,
  'TRUE'::                   BOOLEAN,
  1::                        BOOLEAN, -- 0 false, >= 1 true
  '2021-11-22 11:53:20'::    TIMESTAMP,
  '2021-11-22 11:53:20+01':: TIMESTAMPTZ,
  '15 min'::                 INTERVAL;
```

***
:warning: **_WARNING_**

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>
  
  ```sql
  SELECT
    True = '1';
  ```

  </td>
    <td>
  
  ```less
  ?column? 
  -------- 
  true     
  ```

  </td>
</tr>
<tr>
  <td>
  
  ```sql
  SELECT
    True = 1;
  ```

  </td>
    <td>
  
  ```less
  operator does not exist: boolean = integer
  ```

  </td>
</tr>
</table>

***

## Fonctions de formatage

`PostgreSQL` met à disposition plusieurs fonctions pour simplifier le formatage de données.

> :pencil2: **_INFO_**
>
> La liste complète des `patterns` pour la gestion des dates et des nombres est disponible dans la documentation officielle [9.8. Data Type Formatting Functions](https://www.postgresql.org/docs/14/functions-formatting.html)

### `TO_CHAR()`

La fonction `TO_CHAR()` appliquée sur un type (`INTEGER`, `DATE`, `TIMESTAMP`, etc.) permet de changer le format d'affichage.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Type</th>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>
  
  `TIMESTAMP`
  </td>
  <td>

  ```sql
  SELECT
    TO_CHAR(CURRENT_TIMESTAMP, 'HH24:MI:SS'),
    TO_CHAR(NOW(), 'dd-mm-yyyy hh:MM'),
    TO_CHAR(CURRENT_DATE, 'DD.MM.YYYY'),
    TO_CHAR(AGE(DATE('1975-07-31')), 'YY');
  ```

  </td>
  <td>

  ```less
  to_char  to_char          to_char    to_char 
  -------- ---------------- ---------- ------- 
  16:37:30 24-11-2021 04:11 24.11.2021 46      
  ```

  </td>
</tr>
<tr>
  <td>
  
  `INTERVAL`
  </td>
  <td>
  
  ```sql
  SELECT
    TO_CHAR(INTERVAL '15h 2m 12s', 'HH24:MI:SS');
  ```

  </td>
  <td>

  ```less
  to_char  
  -------- 
  15:02:12 
  ```

  </td>
</tr>
<tr>
  <td>
  
  `INTEGER`
  </td>
  <td>
  
  ```sql
  SELECT
    TO_CHAR(125, '999'),
    TO_CHAR(125, '00999'),
    TO_CHAR(2015, '9G999D99');
  ```

  </td>
  <td>

  ```less
  to_char to_char to_char   
  ------- ------- --------- 
  125     00125   2'015.00 
  ```

  </td>
</tr>
</table>

### `TO_NUMBER()`

La fonction `TO_NUMBER()` converti un nombre en chaîne de caractères selon le format désiré.

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    TO_NUMBER('12454.8-', '99G999D9S');
  ```

  </td>
  <td>

  ```less
  to_number 
  --------- 
  -12454.8  
  ```

  </td>
</tr>
</table>

### `TO_DATE()` et `TO_TIMESTAMP()`

Les fonctions `TO_DATE()` et `TO_TIMESTAMP()` convertissent une chaîne de caractères en `DATE` ou `TIMESTAMP`

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    TO_DATE('23 Nov 2021', 'DD Mon YYYY'),
    TO_DATE('23.11.2021', 'DD.MM.YYYY');
  ```

  </td>
  <td>

  ```less
  to_date    to_date    
  ---------- ---------- 
  2021-11-23 2021-11-23 
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    TO_TIMESTAMP('23 Nov 2021', 'DD Mon YYYY');
  ```

  </td>
  <td>

  ```less
  to_timestamp        
  ------------------- 
  2021-11-23 00:00:00 
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    id,
    nom,
    TO_TIMESTAMP(TO_CHAR(date_naissance, 'YYYY-MM-DD'), 'YYYY-MM-DD')
  FROM
    partenaire_physique
  ```

  </td>
  <td />
</tr>
</table>

# Dates

## `CURRENT_DATE`, `CURRENT_TIME`, `CURRENT_TIMESTAMP`

Les variables `CURRENT_DATE`, `CURRENT_TIME` et `CURRENT_TIMESTAMP` retournent respectivement la date, l'heure et la date et l'heure actuelle.

### Exemples <!-- omit in toc -->

```sql
SELECT 
  CURRENT_DATE, 
  CURRENT_TIME, 
  CURRENT_TIMESTAMP;
```

## `NOW()`

Cette fonction retourne la date et l'heure actuelle

### Exemples <!-- omit in toc -->

```sql
-- Retourne la date et l'heure actuelle
SELECT NOW();

-- Retourne uniquement la date actuelle
SELECT 
  NOW()::DATE;
```

## Différence entre 2 dates

Pour calculer la différence entre 2 dates, vous pouvez utiliser l'opérateur '-' (moins)

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    NOW() - rental_date AS diff
  FROM
    rental;
  ```
  
  </td>
  <td>

  ```less
  diff                      
  ------------------------- 
  5980 days 12:24:45.240629 
  5980 days 12:23:42.240629 
  5980 days 12:14:36.240629 
  ...
  ```

  </td>
</tr>
</table>

## `AGE()`

Le fonction `AGE()` calcule l'âge à la date actuelle en années, mois et jours.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    AGE(rental_date)
  FROM
    rental;
  ```
  
  </td>
  <td>

  ```less
  age                              
  -------------------------------- 
  16 years 4 mons 14 days 01:06:30 
  16 years 4 mons 14 days 01:05:27 
  16 years 4 mons 14 days 00:56:21 
  ...
  ```

  </td>
</tr>
</table>

## `EXTRACT()`, `DATE_PART()`

Les fonctions `EXTRACT()` et `DATE_PART()` permettent d'obtenir une information précise à partir d'une `DATE` ou d'un `TIMESTAMP` ; par exemple l'année, le trimestre, le mois, etc.

> :pencil2: **_INFO_**
>
> La liste complète des `patterns` utilisable pour ces fonctions est disponible dans la documentation officielle [9.9. Date/Time Functions and Operators](https://www.postgresql.org/docs/14/functions-datetime.html)

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    EXTRACT(YEAR FROM rental_date)    AS annee,
    EXTRACT(MONTH FROM rental_date)   AS mois,
    EXTRACT(DAY FROM rental_date)     AS jour,
    EXTRACT(QUARTER FROM rental_date) AS trimestre,
    EXTRACT(WEEK FROM rental_date)    AS numSemaine
  FROM
    rental;
  ```
  
  </td>
  <td>

  ```less
  annee  mois jour trimestre numsemaine 
  ------ ---- ---- --------- ---------- 
  2005.0 5.0  24.0 2.0       21.0      
  2005.0 5.0  24.0 2.0       21.0      
  2005.0 5.0  24.0 2.0       21.0      
  ...
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    CAST(EXTRACT(YEAR FROM rental_date) AS INTEGER) AS annee,
    EXTRACT(YEAR FROM rental_date)::INTEGER AS annee
  FROM
    rental;
  ```

  </td>
  <td>

  ```less
  annee annee 
  ----- ----- 
  2005  2005  
  2005  2005  
  2005  2005  
  ...   ...
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    DATE_PART('YEAR', rental_date)    AS annee,
    DATE_PART('MONTH', rental_date)   AS mois,
    DATE_PART('DAY', rental_date)     AS jour,
    DATE_PART('QUARTER', rental_date) AS trimestre,
    DATE_PART('WEEK', rental_date)    AS numSemaine
  FROM
    rental;
  ```
  
  </td>
  <td>

  ```less
  annee  mois jour trimestre numsemaine 
  ------ ---- ---- --------- ---------- 
  2005.0 5.0  24.0 2.0       21.0      
  2005.0 5.0  24.0 2.0       21.0      
  2005.0 5.0  24.0 2.0       21.0      
  ...
  ```

  </td>
</tr>
</table>

## `INTERVAL`

Le type de données `INTERVAL` vous permet de stocker et de manipuler une période de temps en années, mois, jours, heures, minutes, etc.

En plus de la syntaxe détaillée, `PostgreSQL` vous permet d'écrire les valeurs d'intervalle en utilisant le format `ISO 8601` dont la syntaxe est la suivante :

```less
P quantité [ T quantité ]
```

Par exemple, l'intervalle de 6 ans 5 mois 4 jours 3 heures 2 minutes 1 seconde peut être écrit comme suit :

```less
P6Y5M4DT3H2M1S
```

> :pencil2: **_INFO_**
>
> La liste complète des `patterns` utilisable pour la fonction `INTERVAL` est disponible dans la documentation officielle [8.5. Date/Time Types](https://www.postgresql.org/docs/14/datatype-datetime.html)

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    NOW();
  ```

  ```sql
  SELECT
    NOW() + INTERVAL '2 months ago' AS "-2 mois ago";
  ```

  ```sql
  SELECT
    NOW() - INTERVAL '2 months'     AS "-2 mois";
  ```

  ```sql
  SELECT
    NOW() - INTERVAL 'P2M'          AS "ISO 8601";
  ```
  
  </td>
  <td>

  ```less
  now                 
  ------------------- 
  2021-12-06 13:56:23 
  ```

  ```less
  -2 mois ago         
  ------------------- 
  2021-10-06 13:56:23 
  ```

  ```less
  -2 mois             
  ------------------- 
  2021-10-06 13:56:23 
  ```

  ```less
  ISO 8601            
  ------------------- 
  2021-10-06 13:56:24 
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    NOW();
  ```

  ```sql
  SELECT
    NOW() - INTERVAL '1 year 3 hour 20 minutes' AS "-1 an, 3 heures, 20 minutes";
  ```

  ```sql
  SELECT
    NOW() - INTERVAL 'P1YT3H20M'                AS " ISO 8651";
  ```
  
  </td>
  <td>

  ```less
  now                 
  ------------------- 
  2021-12-06 13:57:38 
  ```

  ```less
  -1 an, 3 heures, 20 minutes 
  --------------------------- 
  2020-12-06 10:37:38         
  ```

  ```less
  ISO 8651           
  ------------------- 
  2020-12-06 10:37:39 
  ```

  </td>
</tr>
</table>

## `JUSTIFY_DAYS()`, `JUSTIFY_HOURS()`, `JUSTIFY_INTERVAL()`

- `JUSTIFY_DAYS()` ajuste l'intervalle sur une période de 30 jours pour représenter un mois

  <table>
  <tr>
    <th>Requête</th>
    <th>Résultat</th>
  </tr>
  <tr>
    <td>

  ```sql
  SELECT
    JUSTIFY_DAYS(INTERVAL '35 days');
  ```

  </td>
  <td>

  ```less
  justify_days 
  ------------ 
  1 mon 5 days 
  ```

  </td>
  </tr>
  </table>

- `JUSTIFY_HOURS()` ajuste l'intervalle pour que les périodes de 24 heures soient représentées sous forme de jours

  <table>
  <tr>
    <th>Requête</th>
    <th>Résultat</th>
  </tr>
  <tr>
    <td>

  ```sql
  SELECT
    JUSTIFY_HOURS(INTERVAL '27 hours');
  ```

  </td>
  <td>

  ```less
  justify_hours  
  -------------- 
  1 day 03:00:00 
  ```

  </td>
  </tr>
  </table>

- `JUSTIFY_INTERVAL()` ajuste l'intervalle en utilisant `JUSTIFY_DAYS()` et `JUSTIFY_HOURS()` tout en tenant compte des signes

  <table>
  <tr>
    <th>Requête</th>
    <th>Résultat</th>
  </tr>
  <tr>
    <td>

  ```sql
  SELECT
    JUSTIFY_INTERVAL(INTERVAL '1 mon -4 hour');
  ```

  </td>
  <td>

  ```less
  justify_interval 
  ---------------- 
  29 days 20:00:00 
  ```

  </td>
  </tr>
  </table>

## `DATE_TRUNC()`

La fonction `DATE_TRUNC()` tronque une valeur `TIMESTAMP` ou `INTERVAL` en fonction d'une partie de date spécifiée ; par exemple une heure, une semaine , un mois, etc.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    DATE_TRUNC('YEAR', rental_date)  AS annee,
    DATE_TRUNC('MONTH', rental_date) AS mois,
    DATE_TRUNC('MIN', rental_date)   AS minutes
  FROM
    rental;
  ```
  
  </td>
  <td>

  ```less
  annee               mois                minutes             
  ------------------- ------------------- ------------------- 
  2005-01-01 00:00:00 2005-05-01 00:00:00 2005-05-24 22:53:00 
  2005-01-01 00:00:00 2005-05-01 00:00:00 2005-05-24 22:54:00 
  2005-01-01 00:00:00 2005-05-01 00:00:00 2005-05-24 23:03:00 
  ...
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    DATE_TRUNC('YEAR', NOW()) - INTERVAL '1 day';
  ```

  ```sql
  SELECT
    DATE_TRUNC('YEAR', NOW()) + INTERVAL '-1 day';
  ```

  ```sql  
  SELECT
    DATE_TRUNC('YEAR', NOW()) + INTERVAL '1 year -1 day';
  ```

  ```sql  
  SELECT
    DATE_TRUNC('YEAR', NOW()) - RANDOM()*100 * INTERVAL '1 day';
  ```

  </td>
  <td>

  ```less
  ?column?            
  ------------------- 
  2020-12-31 00:00:00 
  ```

  ```less
  ?column?            
  ------------------- 
  2020-12-31 00:00:00 
  ```

  ```less
  ?column?            
  ------------------- 
  2021-12-31 00:00:00 
  ```

  ```less
  ?column?            
  ------------------- 
  2020-11-23 02:28:27 
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    staff_id,
    date_trunc('year', rental_date) annee,
    COUNT (rental_id)               rental
  FROM
    rental
  GROUP BY
    staff_id, 
    annee
  ORDER BY
    annee;
  ```
  
  </td>
  <td>

  ```less
  staff_id annee               rental 
  -------- ------------------- ------ 
  2        2005-01-01 00:00:00 7907   
  1        2005-01-01 00:00:00 7955   
  1        2006-01-01 00:00:00 85     
  2        2006-01-01 00:00:00 97     
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    TO_DATE('15.8.2022', 'DD.MM.YYYY') AS dte_orig,
    CAST(DATE_TRUNC('MON', TO_DATE('15.8.2022', 'DD.MM.YYYY')) + INTERVAL 'P1M-1D' AS DATE) AS interval,
    CAST(DATE_TRUNC('MON', TO_DATE('15.8.2022', 'DD.MM.YYYY')) + JUSTIFY_INTERVAL(INTERVAL 'P1M-1D') AS DATE) AS justify_interval;
  ```
  
  </td>
  <td>

  ```less
  dte_orig   interval   justify_interval 
  ---------- ---------- ---------------- 
  2022-08-15 2022-08-31 2022-08-30                
  ```

  </td>
</tr>
</table>

## `TIMESTAMP`, `TIMESTAMPTZ`

`PostgreSQL` fournit deux types de données pour gérer l'horodatage, l'un sans fuseau horaire (`timestamp`) et l'autre avec fuseau horaire (`timestamptz`).

Le type de données `timestamp` permet de stocker la date et l'heure. Cependant, il ne contient aucune donnée concernant le fuseau horaire. Cela signifie que lorsque vous modifiez le fuseau horaire de votre serveur de base de données, la valeur d'horodatage stockée dans la base de données ne change pas.

Le type de données `timestamptz` stocke le fuseau horaire en plus de la date et heure. Dans ce cas, `PostgreSQL` enregistre le fuseau horaire au format UTC (_Universal Time Coordinated_) dans la base de données. Lorsque vous insérez une valeur dans une colonne `timestamptz`, `PostgreSQL` convertit la date et l'heure du serveur en valeur UTC avant de l'enregistrer. Lorsque vous interrogez un `timestamptz` à partir de la base de données, `PostgreSQL` reconvertit la valeur UTC avec le fuseau horaire du serveur.

> :pencil2: **_INFO_**
>
> L'instruction `SHOW TIMEZONE` affiche le fuseau horaire utilisé par votre base de données (normalement `Europe/Zurich`)

### Format

Le format des données `timestamp` :

```sql
yyyy-MM-dd HH:mm:ss.SSSSSS
```

Le format des données `timestamptz` :

```sql
yyyy-MM-dd HH:mm:ss.SSSSSS+01
```

> `+01` indique le fuseau horaire

# Window Functions

## Clause `Over`

La clause `OVER` détermine une fenêtre (ou un sous-ensemble de ligne) dans un ensemble de résultats de requête. Une fenêtre (ou window) peut être appliquée pour définir et calculer une valeur pour chaque ligne de l'ensemble.

De manière très abstraite, nous pouvons dire que `OVER` se comporte comme `GROUP BY`. Les fonctions fenêtrées effectuent des calculs d'agrégations sur des paquets de lignes (qu'on appelle partitions). En revanche, une fois le calcul terminé, les lignes ne sont pas agrégées et le résultat du calcul est ajouté à toutes les lignes de la partition. Le nombre de lignes n'est donc pas modifié et le résultat de la requête contient autant de lignes que la table initiale.

### Arguments

La clause `OVER()` fournit deux arguments optionnels :

| Argument       | Description                                                                                                                          |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| `PARTITION BY` | Définit la méthode de partitionnement. Si cette clause n'est pas mentionnée, la table entière est considérée comme unique partition. |
| `ORDER BY`     | Définit l'ordre à l'intérieur d'une partition. Si cette clause n'est pas mentionnée, aucun tri n'est effectué dans la fenêtre.       |

### Exemples <!-- omit in toc -->

Ci-après un exemple illustrant la différence entre un `GROUP BY` et une instruction `OVER` :

<table>
<tr>
  <th>Type</th>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  `SELECT`
  </td>
  <td>
  
  ```sql
  SELECT 
    customer.last_name, 
    payment.amount
  FROM 
    payment
  JOIN 
    customer
  ON 
    payment.customer_id = customer.customer_id
  WHERE 
    last_name LIKE 'AL%'
  ORDER BY
    customer.last_name;
  ```

  </td>
  <td>

  ```less
  last_name amount 
  --------- ------ 
  ...
  ALEXANDER 3.99   
  ALEXANDER 4.99   
  ALEXANDER 3.99   
  ALLARD    0.99   
  ALLARD    0.99   
  ALLARD    4.99   
  ...
  ```

  </td>
</tr>
<tr>
  <td>
  
  `GROUP BY`
  </td>
  <td>
  
  ```sql
  SELECT
    customer.last_name,
    SUM(payment.amount),
    ROUND(AVG(payment.amount), 2) AS average
  FROM
    payment
  JOIN
    customer
  ON
    payment.customer_id = customer.customer_id
  WHERE
    last_name LIKE 'AL%'
  GROUP BY
    customer.last_name
  ORDER BY
    customer.last_name;
  ```

  </td>
  <td>

  ```less
  last_name sum    average 
  --------- ------ ------- 
  ALEXANDER 105.73 3.92    
  ALLARD    160.68 5.02    
  ALLEN     126.69 4.09    
  ALVAREZ   114.73 4.25    
  ```

  </td>
</tr>
<tr>
  <td>
  
  `OVER`
  </td>
  <td>
  
  ```sql
  SELECT
    customer.last_name,
    payment.amount,
    SUM(payment.amount) OVER(
                         PARTITION BY
                             customer.last_name),
    ROUND(AVG(payment.amount) OVER(
                               PARTITION BY
                                   customer.last_name), 2) AS average
  FROM
    payment
  JOIN
    customer
  ON
    payment.customer_id = customer.customer_id
  WHERE
    last_name LIKE 'AL%'
  ORDER BY
    customer.last_name;
  ```
  
  </td>
  <td>

  ```less
  last_name amount sum    average 
  --------- ------ ------ ------- 
  ...
  ALEXANDER 3.99   105.73 3.92    
  ALEXANDER 4.99   105.73 3.92    
  ALEXANDER 3.99   105.73 3.92    
  ALLARD    0.99   160.68 5.02    
  ALLARD    0.99   160.68 5.02    
  ALLARD    4.99   160.68 5.02    
  ...
  ```

  </td>
</tr>
<tr>
  <td>
  
  Idem sans utiliser `OVER`
  </td>
  <td>

  ```sql
  SELECT
    cust.last_name,
    pay.amount,
    lft.sum,
    lft.average
  FROM
    payment AS pay
  JOIN
    customer AS cust
  ON
    pay.customer_id = cust.customer_id
  LEFT JOIN
    ( SELECT
        customer.last_name,
        SUM(payment.amount)           AS SUM,
        ROUND(AVG(payment.amount), 2) AS average
      FROM
        payment
      JOIN
        customer
      ON
        payment.customer_id = customer.customer_id
      WHERE
        last_name LIKE 'AL%'
      GROUP BY
        customer.last_name ) AS lft
  ON
    cust.last_name = lft.last_name
  WHERE
    cust.last_name LIKE 'AL%'
  ORDER BY
    cust.last_name;
  ```

  </td>
  <td>

  ```less
  last_name amount sum    average 
  --------- ------ ------ ------- 
  ...
  ALEXANDER 3.99   105.73 3.92    
  ALEXANDER 4.99   105.73 3.92    
  ALEXANDER 3.99   105.73 3.92    
  ALLARD    0.99   160.68 5.02    
  ALLARD    0.99   160.68 5.02    
  ALLARD    4.99   160.68 5.02    
  ...
  ```

  </td>
</tr>
</table>

## `LEAD()`, `LAG()`

Les fonctions `LEAD()` et `LAG()` renvoient les valeurs pour une ligne avec un décalage donné après (`lead`) ou avant (`lag`) de la ligne actuelle.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    city,
    country_id,
    LEAD(country_id, 1) OVER (ORDER BY city) AS next_country_id
  FROM
    city;
  ```

  </td>
  <td>

  ```less
  city               country_id next_country_id 
  ------------------ ---------- ---------------
  ...
  A Corua (La Corua) 87         82              
  Abha               82         101             
  Abu Dhabi          101        60              
  Acua               60         97              
  Adana              97         (null)              
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    city,
    country_id,
    LAG(country_id, 1) OVER (ORDER BY city) AS prev_country_id
  FROM
    city;
  ```

  </td>
  <td>

  ```less
  city               country_id prev_country_id 
  ------------------ ---------- --------------- 
  A Corua (La Corua) 87         (null)          
  Abha               82         87              
  Abu Dhabi          101        82              
  Acua               60         101             
  Adana              97         60              
  ...  
  ```

  </td>
</tr>
</table>

## `ROW_NUMBER()`, `RANK()`, `DENSE_RANK()`

Ces 3 fonctions permettent de numéroter les lignes dans une partition :

| Fonction       | Description                                                                                                                                                                     |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `ROW_NUMBER()` | Attribue un nombre unique pour chaque ligne                                                                                                                                     |
| `RANK()`       | Attribue un nombre unique pour chaque groupe contenant la même valeur. Cette numérotation contient des trous qui suivent la numérotation obtenue par la fonction `ROW_NUMBER()` |
| `DENSE_RANK()` | Attribue un nombre unique pour chaque groupe contenant la même valeur. Cette numérotation ne contient pas de trous                                                              |

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT 
    ROW_NUMBER() OVER(ORDER BY "length"),
    RANK() OVER(ORDER BY "length"),
    DENSE_RANK() OVER(ORDER BY "length"),
    title,
    "length"
  FROM 
    film;
  ```
  
  </td>
  <td>

  ```less
  row_number rank dense_rank title               length 
  ---------- ---- ---------- ------------------- ------ 
  1          1    1          RIDGEMONT SUBMARINE 46     
  2          1    1          KWAI HOMEWARD       46     
  3          1    1          LABYRINTH LEAGUE    46     
  4          1    1          ALIEN CENTER        46     
  5          1    1          IRON MOON           46     
  6          6    2          DOWNHILL ENOUGH     47     
  7          6    2          SHANGHAI TYCOON     47     
  8          6    2          SUSPECTS QUILLS     47     
  9          6    2          HANOVER GALAXY      47     
  10         6    2          HAWK CHILL          47     
  11         6    2          HALLOWEEN NUTS      47     
  12         6    2          DIVORCE SHINING     47     
  13         13   3          SUNSET RACER        48     
  14         13   3          ODDS BOOGIE         48     
  15         13   3          PARADISE SABRINA    48     
  ...
  ```

  </td>
</tr>
</table>

# Expressions conditionnelles et opérateurs

## `CASE`

L'instruction `CASE` est indentique à la structure condidionnelle `IF/ELSE` que l'on trouve dans d'autres langages de programmation.

Elle peut être utilisée dans les clauses : `SELECT`, `WHERE`, `GROUP BY` et `HAVING`.

### Syntaxe

```sql
CASE 
  WHEN cond1 THEN res1
  WHEN cond2 THEN res2
  [WHEN ...]
  [ELSE res]
END
```

Chaque condition (`cond1`, `cond2`, etc.) est une expression booléenne qui renvoie soit `True` soit `False`.

Lorsqu'une condition est évaluée comme `False`, l'expression `CASE` évalue la condition suivante de haut en bas jusqu'à ce qu'elle trouve une condition `True`.

Si une condition est `True`, l'expression `CASE` renvoie le résultat correspondant qui suit la condition. Par exemple, si la `cond2` est `True`, l'expression `CASE` renvoie `res2` et arrête d'évaluer les expressions suivantes.

Si toutes les conditions sont `False`, l'expression `CASE` renvoie le résultat qui suit le mot clé `ELSE`. Si vous omettez la clause `ELSE`, l'expression `CASE` renvoie `NULL`.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT 
    title,
    length,
      CASE
        WHEN length <= 50 
          THEN 'Short'
        WHEN length > 50 AND length <= 120 
          THEN 'Medium'
        WHEN length > 120 
          THEN 'Long'
      END duration
  FROM 
    film
  ORDER BY 
    title;
  ```

  </td>
  <td>

  ```less
  title            length duration 
  ---------------- ------ -------- 
  ACADEMY DINOSAUR 86     Medium   
  ACE GOLDFINGER   48     Short    
  ADAPTATION HOLES 50     Short    
  AFFAIR PREJUDICE 117    Medium   
  AFRICAN EGG      130    Long     
  ...
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT 
    title,
    length,
      CASE length
        WHEN 48
          THEN 'Short'
        WHEN 117 
          THEN 'Medium'
        WHEN 169 
          THEN 'Long'
        ELSE
          'NOT DEFINED'    
      END duration
  FROM 
    film
  ORDER BY 
    title;
  ```

  </td>
  <td>

  ```less
  title            length duration    
  ---------------- ------ ----------- 
  ACADEMY DINOSAUR 86     NOT DEFINED 
  ACE GOLDFINGER   48     Short       
  ADAPTATION HOLES 50     NOT DEFINED 
  AFFAIR PREJUDICE 117    Medium      
  AFRICAN EGG      130    NOT DEFINED 
  AGENT TRUMAN     169    Long        
  ...
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    SUM(CASE rating WHEN 'G' 
          THEN 1 
          ELSE 0 
        END) "General Audiences",
    SUM(CASE rating WHEN 'PG'
          THEN 1 
          ELSE 0 
        END) "Parental Guidance Suggested"
  FROM film;
  ```

  </td>
  <td>

  ```less
  General Audiences Parental Guidance Suggested 
  ----------------- --------------------------- 
  178               194                         
  ```

  </td>
</tr>
</table>

## `COALESCE`

La fonction `COALESCE` retourne le premier argument non `NULL`

### Syntaxe

```sql
COALESCE(arg1, arg2, etc.);
```

La fonction `COALESCE` accepte un nombre illimité d'arguments. Elle retourne le premier argument qui n'est pas `NULL`. Si tous les arguments sont `NULL`, la fonction `COALESCE` renverra `NULL`.

`COALESCE` évalue les arguments de gauche à droite jusqu'à ce qu'elle trouve le premier argument non `NULL`.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    COALESCE(1, 2),
    COALESCE(NULL, 2 , 1),
    COALESCE(NULL, NULL);
  ```

  </td>
  <td>

  ```less
  coalesce coalesce coalesce 
  -------- -------- -------- 
  1        2        (null)   
  ```

  </td>
</tr>
<tr>
  <td>

  Remplace les champs `NULL` par `'0'`

  ```sql
  SELECT
    postal_code,
    COALESCE (postal_code, '0')
  FROM
    address;
  ```

  </td>
  <td>

  ```less
  postal_code coalesce 
  ----------- -------- 
  (null)      0        
  (null)      0        
  35200       35200    
  17886       17886    
  ```

  </td>
</tr>
</table>

## `NULLIF`

La fonction `NULLIF` return `NULL` si les 2 arguments passés en paramètre sont identiques. Si ce n'est pas le cas (`arg1` != `arg2`), le 1<sup>er</sup> argument est restourné.

### Syntaxe

```sql
NULLIF(arg1, arg2);
```

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    NULLIF(1, 1),
    NULLIF(1, 0),
    NULLIF('A', 'B');
  ```

  </td>
  <td>

  ```less
  nullif nullif nullif 
  ------ ------ ------ 
  (null) 1      A      
  ```

  </td>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    amount,
    NULLIF(amount, '0'),
    COALESCE(NULLIF(amount, '0'), '0.50')
  FROM
    payment
  ORDER BY
    amount;
  ```

  </td>
  <td>

  ```less
  amount nullif coalesce 
  ------ ------ -------- 
  0.00   (null) 0.50     
  0.00   (null) 0.50     
  0.99   0.99   0.99     
  0.99   0.99   0.99     
  ...
  ```

  </td>
</tr>
<tr>
  <td>

  Éviter les divisions par `0`

  ```sql
  SELECT 
    -- 1.0 / amount ==> ERROR: division by zero
    1.0 / NULLIF(amount, '0')
  FROM
    payment
  ORDER BY
    amount;
  ```

  </td>
  <td>

  ```less
  ?column?           
  ------------------ 
  (null)             
  (null)             
  1.0101010101010101 
  1.0101010101010101 
  ```

  </td>
</tr>
</table>

## `MOD()`

La fonction `MOD()` (ou le caractère `%`) calcul le reste d'une division euclidienne ; par exemple `11 modulo 4 = 3`.

### Exemples <!-- omit in toc -->

<table>
<tr>
  <th>Requête</th>
  <th>Résultat</th>
</tr>
<tr>
  <td>

  ```sql
  SELECT
    11 % 4 AS remainder,
    MOD(11, 4);
  ```

  </td>
  <td>
  
  ```less
  remainder mod 
  --------- --- 
  3         3   
  ```

  </td>
</tr>
</table>

# Gestion des index

## `CREATE INDEX`

### Syntaxe

```sql
CREATE INDEX index_name ON table_name [USING method]
(
    column_name [ASC | DESC] [NULLS {FIRST | LAST }],
    ...
);
```

#### Méthode d'indexation

`PostgreSQL` prend en charge plusieurs types d'index (`method`) dont voici les plus courants :

| method  | Description                                                                                                                                                                                                                                                                                                                                                     |
| ------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `btree` | `B-Tree` est la valeur par défaut lorsque vous créez un index. Les `B-tree` sont stocké sous forme d'arbres balancés et ne permettent de répondre qu'à des questions très simples. Ils fonctionnent uniquement avec les opérateurs : `<`, `<=`, `=`, `>=`, `BETWEEN`, `IN`, `IS NULL` et `IN NOT NULL`. Toutefois, cela couvre la majorité des cas.             |
| `hash`  | Les index de type `Hash indexes` ne gère que la comparaison d'égalité `=`. Cela signifie que chaque fois qu'une colonne indexée est impliquée dans une comparaison à l'aide de l'opérateur égal (`=`), l'optimiseur de requêtes envisagera d'utiliser un index de hachage.                                                                                      |
| `gin`   | Les index `GIN (Generalized Inverted Indexes)` sont particulièrement utiles lorsque plusieurs valeurs sont stockées dans une seule colonne, par exemple pour les types `hstore`, `array`, `jsonb` et `range`.                                                                                                                                                   |
| `brin`  | Les index `BRIN (Block Range Indexes)` sont est plus petit et moins coûteux à maintenir que les index `B-tree`. `BRIN` permet l'utilisation d'un index sur une très grande table qui serait impossible avec `B-tree`. Il est souvent utilisé sur une colonne qui a un ordre de tri linéaire comme la colonne contenant la date d'insertion d'un enregistrement. |
| `gist`  | Les index `GiST (Generalized Search Tree)` sont plus rapide à maintenir qu'un index `GIN`, mais moins performant à l'interrogation. Il est intéressant si on a besoin d'indexer plusieurs colonnes sans trop savoir dans quel ordre on va y accéder ou pour le recherche de type "full text".                                                                   |

### `UNIQUE INDEX`

L'index unique est un index `B-Tree` qui garantit que la table n'aura pas plus d'une ligne avec la même valeur.

Il y a peu de différences entre un index unique et une [contrainte `UNIQUE`](#contraintes). Dans les 2 cas, leur utilisation est intéressante car cela garanti l'intégrité des données et offre d'excellentes performances.

> :pencil2: **_NOTES_**
>
> Les index uniques peuvent être utilisés lors de la création d'[index d'expressions](#index-dexpressions) ou d'[index partiel](#index-partiel)

#### Exemples <!-- omit in toc -->

```sql
CREATE UNIQUE INDEX
  idx_unique_firstname_lastname
ON
  staff
  (
    first_name,
    last_name
  );
```

```sql
CREATE UNIQUE INDEX
  idx_unique_username
ON
  staff
  (
    username
  );
```

### Index d'expressions

L'index d'expressions est un index `B-Tree` qui utilise des fonctions pour définir le périmètre des données indexées. Les recherches sur ces données sont alors aussi efficaces que si elles avaient été réalisées sur une valeur brute.

#### Exemples <!-- omit in toc -->

```sql
CREATE INDEX
  idx_staff_mail
ON
  staff
  (
    lower(email)
  );
```

### Index partiel

L'index partiel est un index `B-Tree` qui couvre un sous-ensemble de données obtenues grâce à une clause `WHERE`. L'idée est d'augmenter l'efficacité de l'index en réduisant la taille des données indexées.

#### Exemples <!-- omit in toc -->

```sql
CREATE INDEX
  idx_staff_is_active
ON
  staff
  (
    last_name
  )
WHERE
  active IS true;
```

### Index multicolonnes

La création d'index multicolonnes est possible. Toutefois, le moteur `PostgreSQL` a la capacité de combiner plusieurs indexes sur des colonnes uniques pour obtenir un réslutat. De ce fait, il n'y a que de très rares cas où un index multi-colonnes s'avère nécessaire.

> :pencil2: **_INFO_**
>
> Pour plus d'information, voir les articles [Multi-column Indexes](https://devcenter.heroku.com/articles/postgresql-indexes#multi-column-indexes) et [Techniques d'indexation](https://public.dalibo.com/exports/formation/manuels/formations/perf2/perf2.handout.html)

### `Hash index`

_(cf. [Types d'index](#types-dindex))_

#### Exemples <!-- omit in toc -->

```sql
CREATE INDEX
  idx_hash_release_year
ON
  film
USING HASH
  (
    release_year
  );
```

## `REINDEX`

Dans de rares cas, il peut arriver qu'un index soit corrompu ou qu'il contienne des données invalides. Dans ce cas, l'instruction `REINDEX` permettra de le regénérer.

> :pencil2: **_INFO_**
>
> L'opération `REINDEX` sera similaire à un `DROP INDEX` puis `CREATE INDEX`, mais les éléments et actions vérouillées le temps de l'indexation seront différents.

### Syntaxe

```sql
REINDEX [ ( VERBOSE ) ] { action } name;
```

<table>
<tr>
  <th>
  
  `action`</th>
  <th>Description</th>
  <th>Requête</th>
</tr>
<tr>
  <td>
  
  `INDEX`</td>
  <td>Recrée un index particulier</td>
  <td>

  ```sql
  REINDEX INDEX index_name;
  ```

  </td>
</tr>
<tr>
  <td>
  
  `TABLE`</td>
  <td>Recrée tous les index d'une table</td>
  <td>

  ```sql
  REINDEX TABLE table_name;
  ```

  </td>
</tr>
<tr>
  <td>
  
  `SCHEMA`</td>
  <td>Recrée tous les index d'un schema</td>
  <td>

  ```sql
  REINDEX SCHEMA schema_name;
  ```

  </td>
</tr>
<tr>
  <td>
  
  `DATABASE`</td>
  <td>Recrée tous les index d'une base de données</td>
  <td>

  ```sql
  REINDEX DATABASE database_name;
  ```

  </td>
</tr>
<tr>
  <td>
  
  `SYSTEM`</td>
  <td>Recrée tous les index du catalogue système d'une base de données</td>
  <td>

  ```sql
  REINDEX SYSTEM database_name;
  ```

  </td>
</tr>
</table>

## `DROP INDEX`

### Syntaxe

| Instruction                   | Description                                                                                                                                                                                                                                                                  |
| ----------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `DROP INDEX [ CONCURRENTLY ]` | Lors de la supression d'un index, `PostgreSQL` vérouille les accès sur la table tant que la suppression n'est pas terminée. Pour forcer la commande à attendre la fin de la transaction en conflit avant de supprimer l'index, vous pouvez utiliser l'option `CONCURRENTLY`. |
| `[ IF EXISTS ] index_name`    |                                                                                                                                                                                                                                                                              |
| `[ CASCADE \| RESTRICT ];`    | Si l'index a des objets dépendants, l'option `CASCADE` supprimera automatiquement ces objets et tous les objets qui dépendent de ces objets (Defaut: `RESTRICT`)                                                                                                             |

#### Exemples <!-- omit in toc -->

```sql
DROP INDEX
  idx_staff_is_active;
```

# Cas pratiques

## Exemple 1 <!-- omit in toc -->

1. Somme des valeurs de production (`mnt_vp_totale`) dans la table `mk_dwh_etat_portefeuille`, par date d'extraction et `id_produit`(sans les `id_produit` < 0)
2. L'évolution de la valeur de production en montant et pourcentage de mois en mois par produits (champs : `dl_datextract`)
3. Part de marché d'un produit dans le mois par rapport aux autres produits

```sql
WITH
  TEMP AS
  ( SELECT
      dl_datextract,
      id_produit,
      SUM(mnt_vp_totale) AS vp
    FROM
      bidwh.mk_dwh_etat_portefeuille
    WHERE
      id_produit>0
    AND mnt_vp_totale<>0
    GROUP BY
      dl_datextract,
      id_produit
    ORDER BY
      dl_datextract
  )
SELECT
  dl_datextract,
  id_produit,
  vp,
  lag(vp,1) over (
                PARTITION BY
                  id_produit
                ORDER BY
                  dl_datextract) AS prec,
  TO_CHAR(COALESCE(lag(vp,1) over (
                                 PARTITION BY
                                   id_produit
                                 ORDER BY
                                   dl_datextract),0),'999 999 999 999') AS vpmoiprec,
  TO_CHAR(vp-COALESCE(lag(vp,1) over (
                                    PARTITION BY
                                      id_produit
                                    ORDER BY
                                      dl_datextract),0),'999 999 999 999') AS diffva,
  TO_CHAR((vp-(lag(vp,1) over (
                             PARTITION BY
                               id_produit
                             ORDER BY
                               dl_datextract)))/(lag(vp,1) over
                                                                 (
                                                               PARTITION BY
                                                                 id_produit
                                                               ORDER BY
                                                                 dl_datextract))*100 ,'90d00%')AS
  diffvr,
  TO_CHAR(SUM(vp) over (
                      PARTITION BY
                        dl_datextract),'999 999 999 999') AS marchetotal,
  TO_CHAR(100*vp/SUM(vp) over (
                             PARTITION BY
                               dl_datextract),'90d00%') AS partdemarche,
  SUM(vp) over (
              PARTITION BY
                id_produit
              ORDER BY
                dl_datextract) AS sumcumul
FROM
  TEMP
```

# DBVisualizer

## Raccourcis

| Raccourci        | Action                                                                   |
| ---------------- | ------------------------------------------------------------------------ |
| ctrl + espace    | Complète automatiquement la sentence (autocompletion)                    |
| ctrl + return    | Exécute la requête SQL                                                   |
| ctrl + shift + f | Formate la requête SQL                                                   |
| ctrl + t         | Ouvre un nouvel onglet pour éditer une requête SQL                       |
| crtl + y         | Supprime le ligne courante                                               |
| crtl + shift + r | Commenter une ligne                                                      |
| crtl + shift + b | Commentet un bloc                                                        |
| ctrl + shift + p | Dans la matrice de l'onglet `Data`, affiche le SQL Preview de la requête |

# Google Sheet

## `QUERY()`

### Syntaxe

| Instruction | Description                                                                |
| ----------- | -------------------------------------------------------------------------- |
| `=QUERY(`   |                                                                            |
| `data`      | la plage de données que vous souhaitez analyser                            |
| `query`     | la requête que vous souhaitez exécuter, entre guillemets                   |
| `[header]`  | nombre facultatif qui indique le nombre de lignes réservées pour l'en-tête |
| `)`         |                                                                            |

#### Exemples <!-- omit in toc -->

```less
=QUERY(Sheet!$A$2:$H$7, "select A where (B<>'Eng' and G=true) or (D > " & A2 & ")")
```

```less
=QUERY(A1:D234, "SELECT C, count(B), min(D), max(D), avg(D) GROUP BY C ORDER BY avg(D) DESC LIMIT 3", 1)
```

# Références

- [PostgreSQL Documentation](https://www.postgresql.org/docs/current/index.html)
- [Cours et tutoriels sur le langage SQL](https://sql.sh/)
- [PostgreSQL Tutorial](https://www.postgresqltutorial.com/)
- [PostgreSQL Wiki](https://wiki.postgresql.org/)
- [Date/Time Functions and Operators](https://www.postgresql.org/docs/current/functions-datetime.html)
- [Les fonctions de fenêtrage en SQL](https://datajourney.io/les-fonctions-de-fenetrage-en-sql.html)
- [Efficient Use of PostgreSQL Indexes](https://devcenter.heroku.com/articles/postgresql-indexes)
- [Indexation & SQL Avancé](https://public.dalibo.com/exports/formation/manuels/formations/perf2/perf2.handout.html)

<p style='text-align: right;'><sub>&copy; 2020-2022 - dServ Solutions SàRL - [Pascal Rapaz](mailto:pascal.rapaz@dserv.ch)</sub></p>
