<!--
Copyright (C) 2021 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.com)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Cheatsheet Markdown <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [Formatage](#formatage)
  - [Titres](#titres)
  - [Styles](#styles)
    - [Mise en évidence](#mise-en-évidence)
  - [Liens hypertexte](#liens-hypertexte)
  - [Listes](#listes)
    - [Listes à puces](#listes-à-puces)
    - [Listes numérotées](#listes-numérotées)
    - [Todo list](#todo-list)
  - [Caractères spéciaux](#caractères-spéciaux)
  - [Insertion d'images](#insertion-dimages)
  - [Ignorer le formatage](#ignorer-le-formatage)
  - [Commentaires](#commentaires)
- [Tableaux](#tableaux)
  - [Avec entête](#avec-entête)
  - [Sans entête](#sans-entête)
  - [Alignement](#alignement)
  - [Spécifique](#spécifique)
- [Code source](#code-source)
- [Blocs](#blocs)
- [Extensions](#extensions)
- [Sources](#sources)

## Formatage

### Titres

Pour créer un titre, ajoutez un  &laquo; `#` &raquo; avant le texte de votre titre. Le nombre de &laquo; `#` &raquo; que vous utilisez déterminera la taille du texte.

```md
# Titre 1
## Titre 2
### Titre 3
#### Titre 4
```

*Résultat*

***

> # Titre 1 <!-- omit in toc -->
>
> ## Titre 2 <!-- omit in toc -->
>
> ### Titre 3 <!-- omit in toc -->
>
> #### Titre 4 <!-- omit in toc -->

***

### Styles

| Style                            | Syntaxe            | Exemple                             | Résultat                          |
| -------------------------------- | ------------------ | ----------------------------------- | --------------------------------- |
| Gras                             | `** **`            | `**Texte en gras**`                 | **Texte en gras**                 |
| Italique                         | `* *`              | `*Texte en italique*`               | *Texte en italique*               |
| Barré                            | `~~  ~~`           | `~~Texte barré~~`                   | ~~Texte barré~~                   |
| Gras avec une partie en italique | `** **` et `* *`   | `**Ce texte est *important***`      | **Ce texte est *important***      |
| Gras et italique                 | `*** ***`          | `***Ce texte est très important***` | ***Ce texte est très important*** |

#### Mise en évidence

Pour mettre en évidence un mot dans une phrase, encadrez-le du caractère  &laquo; `` ` `` &raquo; (`backticks`)

```md
Ce document est écrit en `markdown`.
```

*Résultat*

Ce document est écrit en `markdown`.

### Liens hypertexte

Vous pouvez créer un lien hypertexte mettant le texte entre crochets &laquo; `[ ]` &raquo;, suivi de l'URL entre parenthèses &laquo; `( )` &raquo;.

```md
[GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)
```

Dans le cas où vous mettez uniquement le lien sans texte, ce dernier doit être les symboles &laquo; `< >` &raquo;

```md
<https://docs.gitlab.com/ee/user/markdown.html>
```

*Résultat*

[GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)

<https://docs.gitlab.com/ee/user/markdown.html>

### Listes

#### Listes à puces

Vous pouvez créer une liste à puces en utilisant le caractère &laquo; `-` &raquo; ou  &laquo; `*` &raquo; en début de ligne.

```md
- John Doe
  - Doris Smith
- Jane Alleman
```

*Résultat*

- John Doe
  - Doris Smith
- Jane Madisson

#### Listes numérotées

Pour numéroter une liste, vous devez la précéder d'un nombre.

```md
1. John Doe
   - Doris Smith
2. Jane Madisson
   1. Thom Jones
```

*Résultat*

1. John Doe
   - Doris Smith
2. Jane Madisson
   1. Thom Jones

#### Todo list

Vous pouvez très simplement créer une todo list en utilisant des crochets &laquo; `[ ]` &raquo;

```md
- [ ] Tâche à résaliser
- [x] Tâche terminée
```

*Résultat*

- [ ] Tâche à résaliser
- [x] Tâche terminée

### Caractères spéciaux

Vous pouvez utiliser les symbols `html` pour insérer des caractères spéciaux :

| Caractère | Code      |
| :-------: | --------- |
|  &laquo;  | `&laquo;` |
|  &raquo;  | `&raquo;` |
|  &rarr;   | `&rarr;`  |
|  &#8680;  | `&#8680;` |
|  &#8658;  | `&#8658;` |
|  &#9744;  | `&#9744;` |
|  &#9746;  | `&#9746;` |
|  &copy;   | `&copy;`  |
|  &euro;   | `&euro;`  |

> :bulb: ***TIP***
>
> Une liste complète des symboles `html` est disponible sur le site &laquo; [HTML Symbols](https://www.w3schools.com/html/html_symbols.asp) &raquo;.

### Insertion d'images

Plusieurs méthodes permettent d'insérer une image dans un document `markdown`

1. Insertion `Inline`

    ```md
    ![alt text for screen readers](path/to/image.png or url "Text to show on mouseover")
    ```

    *Exemple*

    ```md
    ![logo markdown](assets/images/markdown_128.png "Markdown")
    ```

    ![logo markdown](assets/images/markdown_128.png "Markdown")

2. Insertion `Référencée`

    ```md
    ![alt text for screen readers][logo]

    [logo]: path/to/image.png or url "Text to show on mouseover"
    ```

    *Exemple*

    ```md
    ![logo markdown][logo]

    [logo]: assets/images/markdown_128.png "Markdown"
    ```

   ![logo markdown][logo]

   [logo]: assets/images/markdown_128.png "Markdown"

3. Insertion `HTML`

    ```html
    <img alt="alt text for screen readers" src="path/to/image.png or url" title="Text to show on mouseover" style="height: 100px; width: 100px;"/>
    ```

    *Exemple*
  
    ```html
    <img alt="logo markdown" src="assets/images/markdown_128.png" title="Markdown" style="height: 50px; width: 50px;"/>
    ```

    <img alt="logo markdown" src="assets/images/markdown_128.png" title="Markdown" style="height: 50px; width: 50px;"/>

### Ignorer le formatage

Vous pouvez ignorer le formatage `markdown` avec le caractère d'échappement &laquo; `\` &raquo;

```less
Renommez le fichier \*ancien_nom.txt\* &#8680; \*nouveau_nom.txt\*
```

*Résultat*

Renommez le fichier \*ancien_nom.txt\* &#8680; \*nouveau_nom.txt\*

### Commentaires

Pour insérer un commentaire, il suffit d'utiliser les balises `hmtl` &laquo; `<!-- -->` &raquo;.

## Tableaux

> :information_source: ***INFO***
>
> De base, le texte est "wrappé" dans la cellule.
>
> Pour forcer un retour de chariot, vous devez utiliser la balise html `<br />`

### Avec entête

```md
| Type   | Syntaxe               |
| ------ | --------------------- |
| String | `var s = 'John Doe';` |
```

*Résultat*

| Type   | Syntaxe               |
| ------ | --------------------- |
| String | `var s = 'John Doe';` |

### Sans entête

```md
|        |                       |
| ------ | --------------------- |
| String | `var s = 'John Doe';` |
```

*Résultat*

|        |                       |
| ------ | --------------------- |
| String | `var s = 'John Doe';` |

> :information_source: ***INFO***
>
> Le résultat peut être différent en fonction de l'interpréteur `markdown` utilisé, car certains ne prennent pas en charge les tableaux sans entête. (<https://stackoverflow.com/a/17543474>)
>
> Pour obtenir un rendu identique sur tous les interpréteur, vous pouvez utiliser les balises `html` classiques pour la gestion des tables. Un exemple est disponible dans la section &laquo; [Spécifique](#spécifique) &raquo;.

### Alignement

```md
| Par défaut | Gauche   |   Droite |  Centré  |
| ---------- | :------- | -------: | :------: |
| markdown   | markdown | markdown | markdown |
```

*Résultat*

| Par défaut | Gauche   |   Droite |  Centré  |
| ---------- | :------- | -------: | :------: |
| markdown   | markdown | markdown | markdown |

### Spécifique

Il n'est pas possible d'insérer un bloc contenant du code sur plusieurs ligne dans un tableau. En revanche, il est possible de créer un tableau en `html` et d'y inclure des balises `markdown`.

````html
<table>
<tr>
  <th>Type</th>
  <th>Syntaxe</th>
</tr>
<tr>
  <td>String</td>
  <td>

  ```js
  var s = 'John Doe';
  ```
  
  </td>
</tr>
</table>
````

*Résultat*

<table>
<tr>
  <th>Type</th>
  <th>Syntaxe</th>
</tr>
<tr>
  <td>String</td>
  <td>

  ```js
  var s = 'John Doe';
  ```

  </td>
</tr>
</table>

## Code source

Il est possible d'insérer des blocs qui conservent l'indentation. Le bloc ainsi créé doit débuter et finir par 3 backticks &laquo; ` ``` ` &raquo;

````md
```
{ 
  "firstname" : "John",
  "lastname" : "Doe",
  "age" : 25,
}
```
````

*Résultat*

```less
{ 
  "firstname" : "John",
  "lastname" : "Doe",
  "age" : 25,
}
```

Il est également possible d'utiliser la "Syntax Highlighting" en spécifiant le langage utilisé directement après les backticks &laquo; ` ``` ` &raquo;

````md
```json
{ 
  "firstname" : "John",
  "lastname" : "Doe",
  "age" : 25,
}
```
````

*Résultat*

```json
{ 
  "firstname" : "John",
  "lastname" : "Doe",
  "age" : 25,
}
```

## Blocs

**Variante 1**

Une barre verticale met en évidence le contenu du bloc.

```md
> :information_source: ***INFO***
>
> :pencil2: ***NOTES***
>
> :bulb: ***TIP***
>
> :warning: ***WARNING*** 
>
> :skull: ***WARNING*** 
```

*Résultat*

> :information_source: ***INFO***
>
> :pencil2: ***NOTES***
>
> :bulb: ***TIP***
>
> :warning: ***WARNING***
>
> :skull: ***WARNING***

**Variante 2**

Le bloc est séparé du reste du document par 2 lignes horizontales.

```md
***
:information_source: ***INFO***

:pencil2: ***NOTES***

:bulb: ***TIP***

:warning: ***WARNING*** 

:skull: ***WARNING*** 
***
```

*Résultat*

***
:information_source: ***INFO***

:pencil2: ***NOTES***

:bulb: ***TIP***

:warning: ***WARNING***

:skull: ***WARNING***
***

> :bulb: ***TIP***
>
> [Liste complète des emojis](https://gist.github.com/rxaviers/7360908#file-gistfile1-md) disponibles avec le format `markdown`

## Extensions

| Logiciel | Extension                                                                                                     |
| -------- | ------------------------------------------------------------------------------------------------------------- |
| Chrome   | [Markdown Viewer](https://chrome.google.com/webstore/detail/markdown-viewer/ckkdlimhmcjmikdlpkmbgfkaikojcbjk) |
| Visual Studio Code | [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)<br />[markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)<br />[Markdown Emoji](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-emoji) |

## Sources

- [The Markdown Guide](https://www.markdownguide.org/)
- [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)
- [Getting started with writing and formatting on GitHub](https://docs.github.com/en/github/writing-on-github/getting-started-with-writing-and-formatting-on-github)

<br /><p style='text-align: right;'><sub>&copy; 2021 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.ch)</sub></p>
