<!--
Copyright (C) 2010-2024 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.com)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Cheatsheet Docker CLI <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [Introduction](#introduction)
- [Images](#images)
- [Run](#run)
- [Containers](#containers)
- [Réseau](#réseau)
- [Dépôts](#dépôts)
- [Volumes](#volumes)
- [Références](#références)

## Introduction
Le CLI Docker (Command Line Interface Docker) est l'interface en ligne de commande pour gérer les conteneurs. C’est un outil puissant pour les développeurs et les administrateurs système qui travaillent avec des conteneurs.

Grâce au CLI Docker, les utilisateurs peuvent exécuter des commandes pour démarrer, arrêter, supprimer et gérer des conteneurs. Ils peuvent également utiliser des commandes pour créer, supprimer et gérer des images, ainsi que des réseaux Docker.

Le CLI Docker est disponible pour les systèmes d'exploitation Windows, GNU/Linux et MacOS. Les utilisateurs peuvent installer Docker sur leur machine locale et accéder au CLI Docker en ouvrant une fenêtre de terminal ou de ligne de commande.

Pour une liste complète des commandes disponibles en CLI, référez-vous à la documentation officielle : [Docker Docs](https://docs.docker.com/)

**L’exécution des commandes CLI de docker nécessitent des droits particuliers. Pour éviter de lancer toutes les commandes en mode root ou sudo, vous pouvez ajouter votre utilisateur dans le groupe docker :**
```bash
sudo usermod -aG docker $USER
```

## Images

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `docker build <folder>`
  </td>
  <td>
  
  Compile une image depuis un fichier Dockerfile  

  ```bash
  # Compile une image depuis le fichier Dockerfile du dossier courant
  docker build . 
  ```
  </td>
</tr>
<tr>
  <td>

  `docker tag <image> <newimage>`
  </td>
  <td>

  Permet de donner un nom (un tag) à une image

  ```bash
  # Ajoute le tag eptmweb:23.04 à l'image eptmweb (eptmweb:latest)
  docker tag eptmweb eptmweb:23.04
  ```
  </td>
</tr>
<tr>
  <td>

  `docker build -t <image> <folder>`
  </td>
  <td>

  Compile et tag une image depuis un fichier Dockerfile

  ```bash
  # Compile une image depuis le fichier Dockerfile du dossier courant et ajoute le tag eptmweb:23.04
  docker build -t eptmweb:23.04 .
  ```
  </td>
</tr>
<tr>
  <td>

  `docker images`

  `docker image ls`
  </td>
  <td>
  
  Liste les images locales
  </td>
</tr>
<tr>
  <td>
  
  `docker rmi <image>`
  
  `docker image rm <image>`
  </td>
  <td>
  
  Supprime une image  
  </td>
</tr>
<tr>
  <td>

  `docker image prune`
  </td>
  <td>
  
  Supprime les images inactives

  ```bash
  # Supprimer toutes les images inutilisées
  docker image prune -a
  ```
  </td>
</tr>
<tr>
  <td>

  `docker system prune`
  </td>
  <td>
  
  Permet de supprimer toutes les ressources inutilisées ainsi que les éléments temporaires
  </td>
</tr>
</table>

## Run

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `docker run <image>`
  </td>
  <td>
  
  Démarre un nouveau container depuis une image
  </td>
</tr>
<tr>
  <td>

  `docker run --rm <image>`
  </td>
  <td>
  
  Nettoie automatiquement le container et supprime le système de fichiers lors de l’arrêt du container
  </td>
</tr>
<tr>
  <td>

  `docker run -d <image>`
  </td>
  <td>
  
  Démarre un nouveau container depuis une image en Permet de démarrer un container en arrière plan (background)
  </td>
</tr>
<tr>
  <td>

  `docker run --name <name> <image>`
  </td>
  <td>
  
  Démarre un nouveau container depuis une image et lui assigne un nom
  </td>
</tr>
<tr>
  <td>
  
  `docker run -P <image>`
  </td>
  <td>
  
  Démarre un nouveau container et “map” les ports, entre la machine hôte et le container, en utilisant les ports définis via la commande `EXPOSE` du fichier Dockerfile 
  </td>
</tr>
<tr>
  <td>

  `docker run -p <hostport>:<containerport> <image>`
  </td>
  <td>
  
  Démarre un nouveau container et “map” un port entre la machine hôte et le container

  ```bash
  # Supprimer toutes les images inutilisées
  docker image prune -a
  ```
  </td>
</tr>
<tr>
  <td>

  `docker run -e [name]=[value] <image>`

  `docker run --env [name]=[value] <image>`
  </td>
  <td>
  
  L'option`-e` (ou `--env`) permet de définir des variables d'environnement qui seront disponibles dans le container
  </td>
</tr>
<tr>
  <td>

  `docker run --hostname <hostname> <image>`
  </td>
  <td>
  
  Attribute un hostname lors du démarrage du container  
  </td>
</tr>
<tr>
  <td>

  `docker run --add-host <hostname>:<ip> <image>`
  </td>
  <td>
  
  Ajoute une entrée DNS lors du démarrage du container  
  </td>
</tr>
<tr>
  <td>

  `docker run --mount type=volume,src=<volumename>,dst=<containerdir> <image>`
  </td>
  <td>

  Monte le volume `<volumename>` dans le container `<containerdir>`.
  </td>
</tr>
<tr>
  <td>

  `docker run --mount type=bind,src=<hostdir>,dst=<containerdir> <image>`
  </td>
  <td>
  
 Bind-mount le dossier `<hostdir>` dans le container `<containerdir>`.
  
  Si le dossier `<hostdir>` n’existe pas au démarrage du container, il sera créé par Docker avant le démarrage du container. 
  </td>
</tr>
<tr>
  <td>

  `docker run --mount type=tmpfs,dst=<containerdir>,size=<size> <image>`
  </td>
  <td>

  Permet de créer un montage en mémoire (tmpfs) sur le dossier `<containerdir>`.
  
  ```bash
  docker run --mount type=tmpfs,dst=/tmp,size=1g eptmweb:23.04
  ```
  </td>
</tr>
<tr>
  <td>

  `docker start|restart|stop <containername> (or <containerid>)`
  </td>
  <td>
  
  Permet de démarrer|redémarrer|arrêter un container existant
  </td>
</tr>
<tr>
  <td>

  `docker exec -it <containername> <shell>`
  </td>
  <td>
  
  Ouvre un `<shell>` dans un container en cours d’exécution
  
  ```bash
  docker exec -it eptmweb:23.04 sh 
      $ apk update
      $ apk add <software>
  ```
  </td>
</tr>
</table>


# Containers

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>
  
  `docker ps`
  </td>
  <td>

  Affiche la liste de tous les containers en cours d’exécution
  </td>
</tr>
<tr>
  <td>

  `docker ps -a`
  </td>
  <td>

  Affiche la liste de tous les containers
  </td>
</tr>
<tr>
  <td>

  `docker rm <containername>`
  </td>
  <td>

  Supprime un container
  </td>
</tr>
<tr>
  <td>

`docker container prune`

`docker rm -f `

`docker ps -aq`

`docker rm -f $(docker ps -aq)`

  </td>
  <td>

  Supprime tous les containers arrêtés
  </td>
</tr>
<tr>
  <td>
  
  `docker rename <oldname> <newname>`
  </td>
  <td>
  
  Permet de renommer un container
  </td>
</tr>
<tr>
  <td>

  `docker cp <containername>:<src> <dest>`

  `docker cp <src> <containername>:<dst> `
  </td>
  <td>
  
  Permet de copier un fichier du container sur votre disque en local ou de votre disque local vers le container
  </td>
</tr>
</table>

# Réseau


<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `docker network create <networkname>`
  </td>
  <td>

  Permet de créer un réseau Docker en mode bridge
  
  ```bash
  docker network create mybridge
  ```
  </td>
</tr>
<tr>
  <td>

  `docker network create -d <driver> <networkname>`
  </td>
  <td>
  
  Permet de créer un réseau Docker en spécifiant le mode souhaité : bridge, overlay ou macvlan

  ```bash
  docker network create --driver overlay mybridge
  ```

  **Il n’est pas nécessaire de créer un réseau pour le mode `host`.**

  **Pour utiliser le mode `host`, vous devez spécifier le paramètre `--network host` au démarrage du container (option de la commande `docker run`)**
  </td>
</tr>
<tr>
  <td>

`docker network ls`
  </td>
  <td>

  Affiche la liste des réseaux Docker
  </td>
</tr>
<tr>
  <td>

  `docker network inspect <networkname>`
  </td>
  <td>

  Récupère les informations sur le réseau

  ```bash
  docker network inspect bridge

  [
    {
      "Name": "bridge",
      "Id": "2cefa504b5f7b3...",
      "Created": "2023-03-31",
      "Scope": "local",
      "Driver": "bridge",
      "EnableIPv6": false,
      "IPAM": {
          "Driver": "default",
          "Options": null,
          "Config": [
            {
              "Subnet": "172.17.0.0/16",
              "Gateway": "172.17.0.1"
            }
          ]
      },
  ...
  ```


  </td>
</tr>
<tr>
  <td>

  `docker network rm <networkname>`
  </td>
  <td>

  Permet de supprimer un réseau docker
  ```bash
  docker network rm mybridge
  ```
  </td>
</tr>
</table>

# Dépôts

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `docker login -u <user> -p <password>`
  </td>
  <td>

  Permet de se logger à un dépôt
  </td>
</tr>
<tr>
  <td>

  `docker logout`
  </td>
  <td>
  
  Permet de se délogger d’un dépôt
  </td>
</tr>
<tr>
  <td>

  `docker search`
  </td>
  <td>
  
  Permet de rechercher une image dans le dépôt
  </td>
</tr>
<tr>
  <td>

  `docker pull <name>:<tag>`
  </td>
  <td>

  Permet de downloader une image présente sur un dépôt
  </td>
</tr>
<tr>
  <td>

  `docker push <name>:<tag>`
  </td>
  <td>

  Permet d’uploader une image dans un dépôt
  </td>
</tr>
</table>

# Volumes

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `docker volume create  <volumename>`
  </td>
  <td>

  Permet de créer un volume
  </td>
</tr>
<tr>
  <td>
  
  `docker volume ls`
  </td>
  <td>

  Permet d’afficher la liste des volumes
  </td>
</tr>
<tr>
  <td>

  `docker volume inspect  <volumename>`
  </td>
  <td>
  
  Fournit des informations détaillées sous forme de tableau JSON sur un volume
  </td>
</tr>
<tr>
  <td>

  `docker volume rm <volumename>`
  </td>
  <td>

  Permet de supprimer un volume
  </td>
</tr>
<tr>
  <td>

  `docker volume prune`
  </td>
  <td>
  
  Supprime tous les volumes inutilisés
  </td>
</tr>
</table>

# Infos, Statistiques et Monitoring

<table>
<tr>
  <th>Commande</th>
  <th>Description</th>
</tr>
<tr>
  <td>

  `docker logs -f <containername> (or <containerid>)`
  </td>
  <td>
  
  Récupère et suit les logs d’un container en live
  </td>
</tr>
<tr>
  <td>

  `docker stats`
  </td>
  <td>

  Affiche les statistiques des containers en cours d’exécution
  </td>
</tr>
<tr>
  <td>

  `docker inspect <containername> (or <containerid>)`
  </td>
  <td>
  
  Fournit des informations détaillées sous forme de tableau JSON sur les objets Docker (image Docker, container, volume, etc.)
  </td>
</tr>
<tr>
  <td>
  
  `docker diff <containername> (or <containerid>)`
  </td>
  <td>

  Affiche tous les fichiers modifiés dans un container
  </td>
</tr>
<tr>
  <td>

  `docker port <containername> (or <containerid>)`
  </td>
  <td>
  
  Affiche tous les ports mappés d’un container
  </td>
</tr>
<tr>
  <td>

  `docker top <containername> (or <containerid>)`
  </td>
  <td>
  
  Affiche les process en cours d’exécution dans un container
  </td>
</tr>
<tr>
  <td>

  `docker --version`
  </td>
  <td>
  
  Affiche le numéro de version de docker
  </td>
</tr>
<tr>
  <td>

  `docker version`
  </td>
  <td>

  Affiche les informations détaillées sur la version de docker
  </td>
</tr>
<tr>
  <td>

  `docker system info`
  </td>
  <td>

  Affiche les détails sur la configuration du moteur Docker, les ressources système disponibles et d'autres informations pertinentes
  </td>
</tr>
<tr>
  <td>

  `docker system df`
  </td>
  <td>
  
  Affiche l’espace utilisé par les images, les containers ainsi que les volumes
  </td>
</tr>
</table>

## Références

-  [Docker Docs](https://docs.docker.com/)

<br /><p style='text-align: right;'><sub>&copy; 2010-2024 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.ch)</sub></p>