<!--
Copyright (C) 2021 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.com)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Cheatsheet Next.js <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [VS Code](#vs-code)
  - [Configuration du debugger](#configuration-du-debugger)
  - [Exécution en mode Debug](#exécution-en-mode-debug)
- [Créer une application](#créer-une-application)
- [Démarrer le serveur de développement](#démarrer-le-serveur-de-développement)
- [Structure du projet](#structure-du-projet)
  - [Absolut Import](#absolut-import)
- [Routes et liens](#routes-et-liens)
  - [Contenu statique (`assets`)](#contenu-statique-assets)
  - [Routes dynamiques](#routes-dynamiques)
- [Metadata](#metadata)
- [Contexte](#contexte)
- [Style](#style)
  - [Niveau composant](#niveau-composant)
  - [Global](#global)
- [Rendu](#rendu)
  - [Rendu statique avec données](#rendu-statique-avec-données)
  - [Rendu sur le serveur avec données](#rendu-sur-le-serveur-avec-données)
  - [Rendu sur le client](#rendu-sur-le-client)
- [API Routes](#api-routes)
  - [Dynamique API routes](#dynamique-api-routes)
- [Déploiement](#déploiement)
- [Authentication](#authentication)
  - [Firebase](#firebase)
- [Sources](#sources)

## VS Code

### Configuration du debugger

1. Éditez le fichier `package.json` du projet et ajoutez l'instruction:

   ```less
   "debug": "NODE_OPTIONS='--inspect' next dev"
   ```

2. Éditez le fichier de configuration du Workspace ou créez un fichier de configuration `.vscode/launch.json` dans votre projet

   ```less
   {
     "version": "0.2.0",
     "configurations": [
       {
         "type": "node",
         "request": "attach",
         "name": "Launch Program",
         "skipFiles": [
           "<node_internals>/**"
         ],
       }
     ]
   }
   ```

### Exécution en mode Debug

1. Démarrez le projet avec la commande `npm start debug`
2. Exécutez le launcher dans l'onglet `Debug` de VS Code

## Créer une application

- Ouvrir un terminal
- Dans le dossier qui doit contenir le projet, exécutez la commande
  
  ```bash
  npx create-next-app --use-npm [project_name]
  ```

> :information_source: **_INFO_**
>
> `npm` est un gestionnaire de package qui va vous aider à installer les dépendances contenues dans le fichier `package.json`
>
> `npx` est un outil pour exécuter des packages `node.js`

## Démarrer le serveur de développement

- Ouvrir un terminal
- Dans le dossier contenant le serveur, lancez la commande

  ```bash
  npm run dev
  ```

## Structure du projet

Il n'y a pas vraiment de règles définies par `Next.js` pour l'arborescence des dossiers.

Un exemple de structure cohérent est disponible sur le site [A 2021 guide about structuring your Next.js project](https://dev.to/vadorequest/a-2021-guide-about-structuring-your-next-js-project-in-a-flexible-and-efficient-way-472)

### Absolut Import

@TODO : <https://nextjs.org/docs/advanced-features/module-path-aliases>

## Routes et liens

Le dossier [pages](https://nextjs.org/docs/basic-features/pages) contient toutes les routes du projet. Pour y accéder, il faut utiliser le composant [Link](https://nextjs.org/docs/api-reference/next/link) :

```js
import Link from 'next/link'

<Link href="/about">
  <a>About</a>
</Link>
```

Documentation:

- [Pages](https://nextjs.org/docs/basic-features/pages)
- [Routing](https://nextjs.org/docs/routing/introduction)
- [next/link](https://nextjs.org/docs/api-reference/next/link)

> :pencil2: **_NOTES_**
>
> Pour insérer un lien situé à l'extérieur de l'application `Next.js`, il faut utiliser le tag `<a>` sans la balise `Link`.
>
> De même, si vous devez ajouter un attribut (p.ex `className`), vous devez l'ajouter à la balise `<a>` comme dans l'exemple ci-après :
>
> ```js
> <Link href="/">
>   <a className="foo" target="_blank" rel="noopener noreferrer">Hello World</a>
> </Link>
> ```

### Contenu statique (`assets`)

`Next.js` peut mettre à disposition des éléments statiques, comme des images, pour autant qu'ils soient situés dans le dossier `public`.

Ce répertoire sera également utile pour d'autres éléments statiques tel que le fichier `robots.txt` fournit par `Google Site Verification`.

```js
import Image from 'next/image'

<Image
  src="/images/profile.jpg"
  height={144} 
  width={144} 
  alt="Alt text for screen readers"
/>
```

Documentation:

- [Static File Serving](https://nextjs.org/docs/basic-features/static-file-serving)
- [next/image](https://nextjs.org/docs/api-reference/next/image)

### Routes dynamiques

Les pages qui sont entre crochets `[]` sont considérées comme des routes dynamiques.

Le code de la page dynamiques est identique à celui d'une page classique à la différence que la méthode `getStaticPaths` doit être implémentée.

Fichier : `pages/posts/[id].js`

```js
export default function Post() {
  return <Layout>...</Layout>
}

export async function getStaticPaths() {
  // Return a list of possible value for id
}
```

Documentation:

- [Dynamic Routes](https://nextjs.org/docs/routing/dynamic-routes)
- [Dynamic Routes Details](https://nextjs.org/learn/basics/dynamic-routes/dynamic-routes-details))
- [The path key](https://nextjs.org/docs/basic-features/data-fetching#the-paths-key-required)
- [The fallback jey](https://nextjs.org/docs/basic-features/data-fetching#the-fallback-key-required)
- [Implement getStaticPaths](https://nextjs.org/learn/basics/dynamic-routes/implement-getstaticpaths)

## Metadata

Pour modifier les `metadata` contenus dans la balise `<head>`, il faut utiliser l'objet `Head`.

```js
import Head from 'next/head'

<Head>
  <title>Next.js Application</title>
</Head>
```

> :pencil2: **_NOTES_**
>
> Pour personnaliser le contenu de la balise `<html>` et y ajouter la langue par exemple, vous devez adapter l'objet `Document`.
>
> Pour plus d'information, référez-vous à la documentation - [custom `Document`](https://nextjs.org/docs/advanced-features/custom-document)

Documentation:

- [next/head](https://nextjs.org/docs/api-reference/next/head)
- [custom `Document`](https://nextjs.org/docs/advanced-features/custom-document)

## Contexte

@TODO: <https://reactjs.org/docs/context.html>

## Style

### Niveau composant

`Next.js` supporte les [Modules CSS](https://github.com/css-modules/css-modules) pou gérer les différents styles des composants.

> :warning: **_WARNING_**
>
> La convention de nommage `[name].module.css` doit être respectée

Fichier : `components/Button.module.css`

```js
.error {
  color: white;
  background-color: red;
}
```

Fichier : `components/Button.js`

```js
import styles from './Button.module.css'

export function Button() {
  return (
    <button
      type="button"
      className={styles.error}
    >
      Destroy
    </button>
  )
}
```

Documentation:

- [Modules CSS](https://github.com/css-modules/css-modules)
- [Built-In CSS Support](https://nextjs.org/docs/basic-features/built-in-css-support)

@TODO: voir si intérêt de compléter avec les infos du site <https://nextjs.org/learn/basics/assets-metadata-css/styling-tips>

### Global

Pour définir un style global à toute l'application, vous devez créer le fichier `pages/_app.js` qui importe votre page `css` personnalisés

```js
import '../styles/global.css'

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}
```

Documentation:

- [Built-In CSS Support](https://nextjs.org/docs/basic-features/built-in-css-support)

## Rendu

@TODO: compléter ce chapitre

`Next.js` offre 2 modes pour le rendu des pages web : `Static Generation` et `Server-side Rendering`.

- `Static Generation` crée le rendu lors de la compilation. Les pages `html` générées sont réutilisée à chaque requêtes.
- `Server-side Rendering` les pages `html` sont générées à chaque requête.

> :information_source: **_INFO_**
>
> En mode développement, lorsque le serveur est démarré avec la commande `npm run dev`, les pages sont toujours générées à chaque appel même si vous utilisez le mode `Static Generation`.

### Rendu statique avec données

Un composant statique peut avoir une méthode asynchrone `getStaticProps` qui indique à `Next.js` une dépendance sur des données.

```js
export default function Home(props) { ... }

export async function getStaticProps() {
  // Get external data from the file system, API, DB, etc.
  const data = ...

  // The value of the `props` key will be passed to the `Home` component
  return {
    props: ...
  }
}
```

@TODO: voir si intérêt de compléter avec les infos du site <https://nextjs.org/learn/basics/data-fetching/getstaticprops-details>

### Rendu sur le serveur avec données

Pour utiliser le `Server-side Rendering` avec des données, vous devez implémenter la méthode `getServerSideProps`.

```js
export async function getServerSideProps(context) {
  return {
    props: {
      // props for your component
    }
  }
}
```

### Rendu sur le client

Avec cette technique, une page statique est générée sur le serveur (`Static Generation`) puis envoyée au client. Elle sera ensuite complétée par des données chargées en `JavaScript` directement sur le client.

`Next.js` met à disposition la librairie `SWR` qui prends, entre autre, en charge la gestion du cache et l'invalidation des données.  

```js
import useSWR from 'swr'

function Profile() {
  const { data, error } = useSWR('/api/user', fetcher)

  if (error) return <div>failed to load</div>
  if (!data) return <div>loading...</div>
  return <div>hello {data.name}!</div>
}
```

> :information_source: **_INFO_**
>
> Cette approche est intéressante pour la mise en place d'un dashboard par exemple

Documentation:

- [Pre-rendering](https://nextjs.org/docs/basic-features/pages#pre-rendering)
- [Fetching data on the client side](https://nextjs.org/docs/basic-features/data-fetching#fetching-data-on-the-client-side)
- [SWR](https://swr.vercel.app/)

## API Routes

@TODO: voir la documentation suivante

Documentation:

- [Creating API Routes](https://nextjs.org/learn/basics/api-routes/creating-api-routes)
- [API Routes](https://nextjs.org/docs/api-routes/introduction)

### Dynamique API routes

Documentation:

- [Dynamic API Routes](https://nextjs.org/docs/api-routes/dynamic-api-routes)

## Déploiement

@TODO: <https://nextjs.org/docs/deployment>

## Authentication

### Firebase

@TODO: <https://javascript.plainenglish.io/next-js-firebase-authentication-including-ssr-1045b097ee18>
       <https://nextjs.org/docs/authentication>
       <https://github.com/gladly-team/next-firebase-auth>
       <https://dev.to/dingran/next-js-firebase-authentication-and-middleware-for-api-routes-29m1>

## Sources

- [The React Framework for Production](https://nextjs.org/)
- [Next Right Now](https://unlyed.github.io/next-right-now/)
- [React.js cheatsheet](https://devhints.io/react)

<br /><p style='text-align: right;'><sub>&copy; 2021 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.ch)</sub></p>
