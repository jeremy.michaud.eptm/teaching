<!--
Copyright (C) 2010-2024 by [Jérémy Michaud](mailto:jeremy.michaud@edu.vs.ch)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Cheatsheet Docker CLI <!-- omit in toc -->

## Table des matières <!-- omit in toc -->

- [Introduction](#introduction)
- [Commandes de base](#commandes-de-base)
- [Références](#références)

## Introduction
Git est un système de contrôle de version distribué open-source conçu pour suivre les modifications du code source pendant le développement logiciel. C'est un outil essentiel pour les développeurs, leur permettant de gérer l'historique des projets, collaborer efficacement et suivre l'évolution de leur code.

Grâce à Git, les développeurs peuvent enregistrer des modifications, revenir à des versions précédentes, créer des branches pour travailler sur différentes fonctionnalités en parallèle, et fusionner ces branches de manière contrôlée. Il facilite la collaboration entre développeurs en permettant à plusieurs personnes de travailler sur le même projet simultanément.

Git a été créé initialement par Linus Torvalds en 2005 pour le développement du noyau Linux. Il est aujourd'hui utilisé par des millions de développeurs dans le monde entier, que ce soit pour des projets open-source ou des développements privés. Des plateformes comme GitHub, GitLab et Bitbucket sont construites autour de Git, offrant des services d'hébergement de code et de collaboration.

Le système est multiplateforme et fonctionne sur Windows, macOS et Linux. Il est gratuit, open-source et dispose d'une communauté active qui contribue continuellement à son développement.

Pour une documentation complète, consultez la documentation officielle : [Git Documentation](https://git-scm.com/doc)


**Les commandes suivantes permettent de configurer votre identité personnelle qui sera utilisée pour identifier les auteurs de chaque modification dans un projet Git. Ces informations sont essentielles pour la traçabilité des contributions, permettant à chaque développeur d'être clairement reconnu lors de ses commits dans des projets individuels ou collaboratifs :**
```bash
# A faire à chaque connexion sur les ordinateurs de l'école
git config --global user.name "<Prénom> <Nom>"
git config --global user.email "<prenom>.<nom>@edu.vs.ch"
```

## Commandes de base

| Commande | Description | Exemple |
|----------|-------------|---------|
| `git init` | Initialise un nouveau dépôt Git local | ```git init ``` |
| `git status` | Vérifie l'état des fichiers dans le dépôt | ```git status ``` |
| `git add <fichier>` | Ajoute un fichier à la zone de préparation | ```bash git add monfichier.py ``` |
| `git log` | Affiche l'historique des commits | ```git log ``` |
| `git commit -m "<message>"` | Crée un nouveau commit avec un message descriptif | ```git commit -m "Ajout du fichier principal" ``` |
| `git branch` | Liste toutes les branches locales | ```bash git branch ``` |
| `git branch <nom_branche>` | Crée une nouvelle branche | ```bash git branch ma_nouvelle_branche ``` |
| `git checkout <nom_branche>` | Bascule vers une branche existante | ```bash git checkout main ``` |
| `git checkout -b <nom_branche>` | Crée et bascule vers une nouvelle branche | ```git checkout -b nouvelle_fonctionnalite ``` |
| `git diff` | Montre les modifications non indexées | ```git diff ``` |

## Références

- [Documentation Git officielle](https://git-scm.com/doc)

## Références

- [Documentation Git officielle](https://git-scm.com/doc)

<br /><p style='text-align: right;'><sub>&copy; 2010-2024 by [jérémy Michaud](mailto:jeremy.michaud@edu.vs.ch)</sub></p>