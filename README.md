<!--
Copyright (C) 2021 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.com)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
Free Documentation License".

A copy of the license is agreement is available at [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Teaching <!-- omit in toc -->

This repository contains various documents useful for IT courses

## Table of content <!-- omit in toc -->

- [Cheatsheets](#cheatsheets)
  - [Development](#development)
  - [GNU/Linux](#gnulinux)
  - [Hacking](#hacking)
  - [Database](#database)
  - [Misc](#misc)
- [Development](#development-1)
  - [Python](#python)
  - [Java](#java)

## Cheatsheets

### Development

- [Markdown](https://gitlab.com/pascal.rapaz/teaching/-/blob/master/cheatsheet/markdown.md)
- [JavaScript](https://gitlab.com/pascal.rapaz/teaching/-/blob/master/cheatsheet/javascript.md)
- [Python](https://gitlab.com/pascal.rapaz/teaching/-/blob/master/cheatsheet/python.md)

### GNU/Linux

- [bash](https://gitlab.com/pascal.rapaz/teaching/-/blob/master/cheatsheet/bash.md)
- [btrfs](https://gitlab.com/pascal.rapaz/teaching/-/blob/master/cheatsheet/btrfs.md)

### Hacking

- [kali](https://gitlab.com/pascal.rapaz/teaching/-/blob/master/cheatsheet/kali.md)

### Database

- [PostgreSQL](https://gitlab.com/pascal.rapaz/teaching/-/blob/master/cheatsheet/postgresql.md)

### Misc

- [VMWare Workstation](https://gitlab.com/pascal.rapaz/teaching/-/blob/master/cheatsheet/vmware.md)

## Development

### Python

- [Exercices and samples](https://gitlab.com/pascal.rapaz/teaching/-/tree/master/python)

### Java

- [Exercices and samples](https://gitlab.com/pascal.rapaz/teaching/-/tree/master/java)

<br /><p style='text-align: right;'><sub>&copy; 2021-2022 by [Pascal Rapaz](mailto:pascal.rapaz@gmail.ch)</sub></p>
