package ch.rapazp.java.formation.practice.sommePrenom;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * SommePrenom.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.11.2002     1.0       Creation
 * 29.11.2002     1.1       Modification selon remarques de: JMM, PR et LIG.
 * 16.01.2014     1.2       Remplacement de println par printf
 * 01.02.2014     1.3       Ajout variante 3
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 2
 * 
 * Compte la somme des lettres constituant un prenom (a=1, b=2, etc.)
 * 
 * @author Pascal Rapaz
 * @version 1.3
 */
public class SommePrenom {

  public static void main(String[] args) {
    if (args.length != 1) {
      System.err.println("Execution: java SommePrenom <prenom>");
      System.exit(0);
    }// endIf

    calcSommePrenom(args[0]);
  }// endMain

  /**
   * Calcul la somme des lettres d'unprenom.
   * 
   * @param prenom
   *          Le prenom a utiliser pour le calcul.
   */
  private static void calcSommePrenom(String prenom) {

    int somme;

    // creation d'une string temporaire afin de pouvoir reprendre la variable
    // prenom dans chaque
    // variante
    String tmp;

    /* Variante 1 - tout en minuscule */
    somme = 0;
    tmp = prenom.toLowerCase();

    for (int i = 0; i < tmp.length(); i++) {
      somme += tmp.charAt(i) - 'a' + 1;
    }// endFor

    System.out.printf("Variante 1: La somme des lettres de votre prenom vaut: %d\n", somme);

    /* Variante 2 - tout en majuscule */
    somme = 0;
    tmp = prenom.toUpperCase();

    for (int i = 0; i < tmp.length(); i++) {
      somme += tmp.charAt(i) - 'A' + 1;
    }// endFor

    System.out.printf("Variante 2: La somme des lettres de votre prenom vaut: %d\n", somme);

    // Variante 3 - mixte majuscules / minuscules
    somme = 0;
    tmp = prenom;

    for (int i = 0; i < tmp.length(); i++) {

      somme += tmp.charAt(i) + 1;

      if (Character.isLowerCase(tmp.charAt(i))) {
        somme -= 'a';
      } else {
        somme -= 'A';
      }// endIf
    }// endFor

    System.out.printf("Variante 3: La somme des lettres de votre prenom vaut: %d\n", somme);

  }// endFct
}// /:~
