package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Formatage.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 10.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Formatage de l'affichage.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Formatage {

  public static void main(String[] args) {
    double x = 10000.0 / 3.0;

    System.out.print("Quel est l'age du capitaine ?\n");

    System.out.println("Juste une nouvelle ligne");
    System.out.println(23);
    System.out.println(x); // affiche 3333.3333333333335

    System.out.printf("La valeur du champs x vaut %d en int, "
        + "%f en float et %s en string", (int) x, x, x);

    NumberFormat nf = NumberFormat.getNumberInstance();
    String fx = nf.format(x); // pour la suisse la chaine vaut: "3'333.333"
    System.out.println(fx);

    System.out.println(NumberFormat.getCurrencyInstance().format(x)); // affiche
                                                                      // "SFr. 3'333.33"
    System.out.println(NumberFormat.getPercentInstance().format(x)); // affiche
                                                                     // "333'333%"
    System.out.println(NumberFormat.getCurrencyInstance(Locale.CANADA_FRENCH)
        .format(x)); // affiche "3 333,33 $"

    DecimalFormat df = new DecimalFormat("0.#####");
    System.out.println(df.format(x)); // affiche:"3333.33333"
  }// endMain
}// /:~
