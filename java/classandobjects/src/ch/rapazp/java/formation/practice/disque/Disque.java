package ch.rapazp.java.formation.practice.disque;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Disque.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 17.12.2002     1.0       Creation
 * 24.01.2003     1.1       Modification du constructeur avec parametres
 *                                ainsi que de formatage de l'heure
 * -----------------------------------------------------------------------------
 */

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Exercice 2
 * 
 * Representation d'un disque sous forme de classe
 * 
 * @author Pascal Rapaz
 * @version 1.1
 */
public class Disque {

  private String artist = null;
  private String title = null;
  private String style = null;
  private int nbrTrack = 0;
  private long time = 0;

  public static void main(String[] args) {

    // Creation des disques
    Disque d1 = new Disque("Midnight Oil", "Capricornia", "rock alternatif", 12, calculTime(0, 56,
        56));

    Disque d2 = new Disque("Tom McRae", "Tom McRae", "rock", 13, calculTime(0, 45, 17));

    Disque d3 = new Disque("Alanis Morissette", "Under Rug Swept", "rock", 13,
        calculTime(0, 50, 52));

    // Affichage des differents disques
    System.out.println("Disque 1");
    System.out.println("~~~~~~~~");
    System.out.println(d1.getArtist());
    System.out.println(d1.getTitle());
    System.out.println(d1.getStyle());
    System.out.println(d1.getNbrTrack());
    System.out.println(d1.getFormatedTime());

    System.out.println("\nDisque 2");
    System.out.println("~~~~~~~~");
    System.out.println(d2.getArtist());
    System.out.println(d2.getTitle());
    System.out.println(d2.getStyle());
    System.out.println(d2.getNbrTrack());
    System.out.println(d2.getFormatedTime());

    System.out.println("\nDisque 3");
    System.out.println("~~~~~~~~");
    System.out.println(d3.getArtist());
    System.out.println(d3.getTitle());
    System.out.println(d3.getStyle());
    System.out.println(d3.getNbrTrack());
    System.out.println(d3.getFormatedTime());

    // Calcul du nombre total de morceaux
    System.out.println("\nLe nombre total de morceaux est: "
        + (d1.getNbrTrack() + d2.getNbrTrack() + d3.getNbrTrack()));

    // Calcul de la duree totale
    System.out.println("La duree total est: "
        + formatTime(d1.getTime() + d2.getTime() + d3.getTime()));
  } // endMain

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Helpers
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Retourne une heure en format milliseconde
   * 
   * @param hour
   *          Heure
   * @param min
   *          Minutes
   * @param sec
   *          Secondes
   * @return long Represente l'heure en milliseconde
   */
  public static long calculTime(int hour, int min, int sec) {

    /*
     * Rappel: 1 heure = 1 * 60 * 60 * 1000 millisecondes
     */
    return (hour * 3600 + min * 60 + sec) * 1000;
  } // endFct

  /**
   * Retourne l'heure formatee.
   * 
   * @param time
   *          L'heure a formater
   * @return String L'heure formatee
   */
  public static String formatTime(long time) {

    // Definition du timezone afin de ne pas avoir de decalage horaire dans
    // le formatage de l'heure

    /*
     * Methode 1:
     * 
     * TimeZone utc = TimeZone.getTimeZone("GMT");
     * 
     * DateFormat df = DateFormat.getTimeInstance(DateFormat.MEDIUM);
     * 
     * df.setTimeZone(utc);
     * 
     * return df.format(new Date(time));
     */

    /*
     * Methode 2
     */
    TimeZone utc = TimeZone.getTimeZone("GMT");
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    sdf.setTimeZone(utc);

    return sdf.format(new Date(time));

  } // endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Construit un nouvel objet de type Disque.
   * 
   * @param artist
   *          Nom de l'artiste
   * @param title
   *          Titre du disque
   * @param style
   *          Style musical
   * @param nbrTracks
   *          Nombre de morceaux (pistes)
   * @param time
   *          Duree du disque
   */
  public Disque(String artist, String title, String style, int nbrTracks, long time) {

    this.setArtist(artist);
    this.setTitle(title);
    this.setStyle(style);
    this.setNbrTrack(nbrTracks);
    this.setTime(time);
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Retourne la duree du disque formatee comme suit: <code>hh:mm:ss</code>
   * 
   * @return String La duree formatee
   */
  public String getFormatedTime() {

    return formatTime(time);
  } // endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Getters and Setters
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Retourne le nom de l'artiste
   * 
   * @return String
   */
  public String getArtist() {
    return artist;
  } // endFct

  /**
   * Retourne le style de musique du disque
   * 
   * @return String
   */
  public String getStyle() {
    return style;
  } // endFct

  /**
   * Retourne le nombre de morceaux
   * 
   * @return int
   */
  public int getNbrTrack() {
    return nbrTrack;
  } // endFct

  /**
   * Retourne le titre de l'album
   * 
   * @return String
   */
  public String getTitle() {
    return title;
  } // endFct

  /**
   * Retourne la duree du disque en milliseconde
   * 
   * @return long
   */
  public long getTime() {
    return time;
  } // endFct

  /**
   * Valorise le nom de l'artiste
   * 
   * @param artist
   *          Le nom de l'artiste
   */
  public void setArtist(String artist) {
    this.artist = artist;
  } // endFct

  /**
   * Valorise le style de musique du disque
   * 
   * @param edition
   *          Le style de musique
   */
  public void setStyle(String style) {
    this.style = style;
  } // endFct

  /**
   * Valorise le nombre de morceaux
   * 
   * @param nbrTrack
   *          Le nombre de morceaux
   */
  public void setNbrTrack(int nbrTrack) {
    this.nbrTrack = nbrTrack;
  } // endFct

  /**
   * Valorise le titre du disque
   * 
   * @param title
   *          Le titre du disque
   */
  public void setTitle(String title) {
    this.title = title;
  } // endFct

  /**
   * Valorise la duree du disque en milliseconde
   * 
   * @param totalTime
   *          La duree du disque
   */
  public void setTime(long time) {
    this.time = time;
  } // endFct
} // /:~
