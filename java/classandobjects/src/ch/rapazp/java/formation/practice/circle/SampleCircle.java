package ch.rapazp.java.formation.practice.circle;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * SampleCircle.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 17.12.2002     1.0       Creation
 * 24.01.2003     1.1       Modification du nom de variable de la classe 
 *                          CircleClass
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 1
 * 
 * Exemple de creation d'une classe cercle
 * 
 * @author Pascal Rapaz
 * @version 1.1
 */
public class SampleCircle {

  public static void main(String[] args) {
    Circle c = new Circle(25);

    System.out.println("surface   = " + c.area());
    System.out.println("perimetre = " + c.circumference());

    System.out.println("2.68 rad = " + Circle.radiusToDegrees(2.68)
        + "Deg");
  }// endMain
}// /:~

/**
 * Classe cercle
 */
class Circle {

  private static final double PI = 3.14159;
  private double rayon = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Construit un objet cercle avec un rayon defini par l'utilisateur
   * 
   * @param r
   *          Rayon
   */
  public Circle(double rayon) {
    this.rayon = rayon;
  }// endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Converti un angle en radian en degre
   * 
   * @param rads
   *          Valeur du rayon a convertir en degre
   * 
   * @return double Representation en degre du rayon
   */
  public static double radiusToDegrees(double rads) {
    return rads * 180 / PI;
  }// endFct

  /**
   * Retourne la surface d'un cercle
   */
  public double area() {
    return PI * rayon * rayon;
  }// endFct

  /**
   * Retourne le perimetre d'un cercle
   */
  public double circumference() {
    return 2 * PI * rayon;
  }// endFct
}// /:~
