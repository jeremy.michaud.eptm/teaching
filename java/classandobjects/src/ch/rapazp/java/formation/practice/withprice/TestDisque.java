package ch.rapazp.java.formation.practice.withprice;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * TestDisque.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 18.12.2002     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 6
 * 
 * Classe de test pour les objets de type Disque.
 * 
 * Modifications effectuees dans le main() par rapport a l'exercice 5:
 * 
 * - Differents test sur le prix
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class TestDisque {

  public static void main(String[] args) {

    // Creation des disques
    Disque d1 = new Disque();
    d1.setArtist("Midnight Oil");
    d1.setTitle("Capricornia");
    d1.setStyle("rock alternatif");
    d1.setNbrTrack(12);
    d1.setTime(Disque.calculTime(0, 56, 56));

    Disque d2 = new Disque("David Gray", "White ladder");
    d2.setStyle("folk");
    d2.setNbrTrack(11);
    d2.setTime(Disque.calculTime(0, 69, 30));

    Disque d3 = new Disque("Alanis Morissette", "Under Rug Swept", "rock", 13, Disque.calculTime(0,
        50, 52), 35.1);

    // Affichage des differents disques
    d1.afficheToi();
    System.out.println("Le prix a-t-il ete fixe: " + d1.isPrixFixe());
    d1.setPrix(-1);
    d1.afficheToi();
    d1.setPrix(29.95);
    d1.afficheToi();
    System.out.println("Le prix a-t-il ete fixe: " + d1.isPrixFixe());
    d1.setPrix(19.95);

    System.out.println();

    d2.afficheToi();
    System.out.println(d3);

    // Calcul du nombre total de morceaux
    System.out.println("\nLe nombre total de morceaux est: "
        + (d1.getNbrTrack() + d2.getNbrTrack() + d3.getNbrTrack()));

    // Calcul de la duree totale
    System.out.println("La duree total est: "
        + Disque.formatTime(d1.getTime() + d2.getTime() + d3.getTime()));
  } // endMain
} // /:~
