package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * TriangleRectangle.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 09.10.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * TriangleRectangle heritant de la classe abstraite 'Shape'
 *
 * @author Pascal Rapaz
 * @version 1.0
 */
public class TriangleRectangle extends Shape {

  private double coteA = 0;
  private double coteB = 0;
  private double coteC = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public TriangleRectangle(double base, double hauteur, double diag) {

    coteA = base;
    coteB = hauteur;
    coteC = diag;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Implementation des methodes abstraites
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * (non-Javadoc)
   * 
   * @see ch.rapazp.java.formation.sample.Shape#area()
   */
  public double area() {

    return coteA * coteB / 2;
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see ch.rapazp.java.formation.sample.Shape#perimeters()
   */
  public double perimeters() {

    return coteA + coteB + coteC;
  } // endFct
} // /:~
