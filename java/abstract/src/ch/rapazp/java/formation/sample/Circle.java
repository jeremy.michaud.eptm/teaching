package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Circle.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 09.10.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Cercle heritant de la classe abstraite 'Shape'
 *
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Circle extends Shape {

  private double rayon = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public Circle(double rayon) {

    this.rayon = rayon;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Implementation des methodes abstraites
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * (non-Javadoc)
   * 
   * @see ch.rapazp.java.formation.sample.Shape#area()
   */
  public double area() {

    return Math.PI * rayon * rayon;
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see ch.rapazp.java.formation.sample.Shape#perimeters()
   */
  public double perimeters() {

    return 2 * Math.PI * rayon;
  } // endFct
} // /:~
