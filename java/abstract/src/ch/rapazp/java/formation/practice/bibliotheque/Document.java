package ch.rapazp.java.formation.practice.bibliotheque;

/*
 * ----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * ----------------------------------------------------------------------------
 *
 * Document.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 01.09.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Representation abstraite d'un document
 *
 * @author Pascal Rapaz
 * @version 1.0
 */
public abstract class Document {

  private String titre = null;

  // membre static : nombre de documents
  private static int nbrDocuments = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeur
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  protected Document(String titre) {
    this.titre = titre;
    nbrDocuments++;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes abstraites
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public abstract void displayFormated();

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public static int getNbDocuments() {
    return nbrDocuments;
  } // endFct

  public String toString() {
    return "\nTitre: " + titre;
  } // endFct
} // /:~
