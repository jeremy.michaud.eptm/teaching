package ch.rapazp.java.formation.practice.bibliotheque;

/*
 * ----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * ----------------------------------------------------------------------------
 *
 * Internet.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 01.09.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Representation d'un document internet.
 *
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Internet extends Document {

  private String url;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public Internet(String titre, String url) {

    super(titre);
    this.url = url;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public String toString() {

    return super.toString() + " - URL: " + url;
  }// endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Implementation des methodes abstraites
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * (non-Javadoc)
   * 
   * @see
   * ch.rapazp.java.formation.practice.bibliotheque.Document#displayFormated()
   */
  public void displayFormated() {

    System.out.println("Document internet: " + this.toString());
  } // endFct
} // /:~
