package ch.rapazp.java.formation.sample.statique;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * SampleStatique.java
 *
 * -----------------------------------------------------------------------------
 * WHEN            WHO   VERSION   DESCRIPTION
 * 14 fevr. 2002   RaP       1.0   Creation
 * -----------------------------------------------------------------------------
 */

/**
 * <p>
 * Exemple presentant la difference entre une variable <code>static</code> et
 * <code>final</code>.
 * </p>
 *
 * @author Pascal Rapaz
 * @version 1.0
 */
public class SampleStatique {

  public static void main(String[] args) {

    Statique s1 = new Statique();
    Statique s2 = new Statique();

    System.out.println("Valeurs avant modifications");
    System.out.println("Objet 1:");
    System.out.println(s1);
    System.out.println("Objet 2:");
    System.out.println(s2);

    s2.setA(999);

    System.out.println("Valeurs APRES modifications");
    System.out.println("Objet 1:");
    System.out.println(s1);
    System.out.println("Objet 2:");
    System.out.println(s2);
  } // end main
} // /:~

class Statique {

  /* La variable A peut etre modifiee. */
  // Toutes les instances 'pointeront' sur la meme variable.
  // Si l'une ou l'autre la modifie, toutes auront la meme valeur
  private static int A = 10;

  /* Les variables B et C ne peuvent etre modifiees. */
  // Si vous essayer de les modifier, java interdira la compilation.
  private static final int B = 20;
  // Toutes les instance 'pointeront' sur la meme variable.
  // Une variable sera cree par instance
  private final int C = 30;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public Statique() {
  } // end Statique

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * <p>
   * Methode permettant de modifier la valeur de la variable A.
   * </p>
   * 
   * <p>
   * <b> REM: IL N'EST PAS POSSIBLE DE FAIRE DES METHODES SIMILAIRES POUR B ET C
   * CAR ELLE SONT DECLAREES COMME ETANT FINAL </b>
   * </p>
   * 
   * @param value
   *          La nouvelle valeure pour la variable
   */
  public void setA(int value) {
    A = value;
  } // end setA

  /**
   * <p>
   * Retourne une chaine de caractere contenant les variables A, B et C.
   * </p>
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return "A: " + A + "\nB: " + B + "\nC: " + C;
  } // end toString
} // /:~
