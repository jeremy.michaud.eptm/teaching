package ch.rapazp.java.formation.practice.vehicule;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * SampleVehicule.java
 *
 * Vehicule.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 15.02.2003     1.0       Creation
 * 02.06.2003     1.1       Misc
 * -----------------------------------------------------------------------------
 */

import java.util.Random;

/**
 * Exercice 1
 * 
 * Classe de test
 * 
 * @author Pascal Rapaz
 * @version 1.1
 */
public class SampleVehicule {

  static final int NBRVHC = 10;

  public static void main(String[] args) {

    Vehicule[] vehicules = new Vehicule[NBRVHC];

    int tmp;
    Automobile a;
    Camion c;

    // affectation alleatoire des differents vehicules
    Random r = new Random();

    for (int i = 0; i < NBRVHC; i++) {
      tmp = r.nextInt(3);

      if ((r.nextInt(2)) == 0) {
        a = null;

        switch (tmp) {
        case 0:
          a = new Automobile("coupe sport", "VS 233424", "Billy LePetit", r.nextInt(4) + 1);
          break;
        case 1:
          a = new Automobile("break", "VD 85435", "Bernard Lenoir", r.nextInt(6) + 1);
          break;
        default:
          a = new Automobile("cabriolet", "NE 976445", "Yetti Desforets", r.nextInt(4) + 1);
        } // endSwitch

        vehicules[i] = a;
      } else {
        c = null;

        switch (tmp) {
        case 0:
          c = new Camion("camionnette", "VS 84524", "La Tribune", r.nextInt(10) + 10);
          c.setTypeChargement("Journaux du matin");
          break;
        case 1:
          c = new Camion("bus", "VD 685224", "Le Chalet", r.nextInt(20) + 10);
          c.setTypeChargement("bois");
          break;
        default:
          c = new Camion("camion", "GE 5422114", "Tout Transport", r.nextInt(20) + 20);
          c.setTypeChargement("Meubles");
        } // endSwitch

        vehicules[i] = c;
      } // endIf

      // demarrer le vehicule
      if (r.nextInt(2) == 0) {
        vehicules[i].demarrer();
      } // endIf
    } // endFor

    // affiche le contenu
    for (int i = 0; i < NBRVHC; i++) {

      System.out.print(vehicules[i]);

      if (vehicules[i] instanceof Camion) {
        System.out.println("\nChargement: " + ((Camion) vehicules[i]).getTypeChargement());
      } // endIf

      if (vehicules[i].isStarted()) {
        System.out.println("Le vehicule est en marche!");
      } // endIf

      System.out.print("\n----\n");
    } // endFor
  } // endMain
} // /:~
