package ch.rapazp.java.formation.practice.disque.compactDisc;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * DiscHelper.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 07.02.2003     1.0       Creation
 * 26.09.2003     1.1       Modification methode calculTime(String) selon
 *                          proposition JMM
 * -----------------------------------------------------------------------------
 */

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.TimeZone;

/**
 * Exercice 2
 * 
 * Cette classe contient toutes les methodes de service utilisee par les classes
 * de gestion de disques
 *
 * @author Pascal Rapaz
 * @version 1.1
 */
public abstract class DiscHelper {

  /**
   * Retourne une heure en format milliseconde
   *
   * @param time
   *          L'heure au format <code>hh:mm:ss</code>, <code>mm:ss</code> ou
   *          <code>ss</code>
   * @return long Represente l'heure en milliseconde
   */
  public static long calculTime(String time) {

    int hh = 0;
    int mm = 0;
    int ss = 0;
    StringTokenizer st = new StringTokenizer(time, ":");

    switch (st.countTokens()) {
    case 3:
      hh = Integer.parseInt(st.nextToken());

    case 2:
      mm = Integer.parseInt(st.nextToken());

    case 1:
      ss = Integer.parseInt(st.nextToken());
    } // endSwitch

    return calculTime(hh, mm, ss);
  } // endFct

  /**
   * Retourne une heure en format milliseconde
   *
   * @param min
   *          Minutes
   * @param sec
   *          Secondes
   * @return long Represente l'heure en milliseconde
   */
  public static long calculTime(int min, int sec) {

    return calculTime(0, min, sec);
  } // endFct

  /**
   * Retourne une heure en format milliseconde
   *
   * @param hour
   *          Heure
   * @param min
   *          Minutes
   * @param sec
   *          Secondes
   * @return long Represente l'heure en milliseconde
   */
  public static long calculTime(int hour, int min, int sec) {

    /*
     * Rappel: 1 heure = 1 * 60 * 60 * 1000 millisecondes
     */
    return (hour * 3600 + min * 60 + sec) * 1000;
  } // endFct

  /**
   * Retourne l'heure formatee
   *
   * @param time
   *          L'heure a formater
   * @return String L'heure formatee
   */
  public static String formatTime(long time) {

    // Definition du timezone afin de ne pas avoir de decalage horaire dans
    // le formatage de l'heure
    TimeZone utc = TimeZone.getTimeZone("GMT");
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    sdf.setTimeZone(utc);

    return sdf.format(new Date(time));
  } // endFct
} // /:~
