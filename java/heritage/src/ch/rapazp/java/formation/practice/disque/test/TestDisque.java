package ch.rapazp.java.formation.practice.disque.test;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * TestDisque.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 18.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import ch.rapazp.java.formation.practice.disque.compactDisc.DiscHelper;
import ch.rapazp.java.formation.practice.disque.compactDisc.audio.AudioCD;
import ch.rapazp.java.formation.practice.disque.compactDisc.audio.EnumGenre;
import ch.rapazp.java.formation.practice.disque.compactDisc.audio.Track;
import ch.rapazp.java.formation.practice.disque.compactDisc.dvd.DVD;
import ch.rapazp.java.formation.practice.disque.compactDisc.dvd.EnumLanguage;
import ch.rapazp.java.formation.practice.disque.compactDisc.dvd.EnumZone;

/**
 * Exercice 2
 * 
 * Classe de test pour les objets de type Disque.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class TestDisque {

  public static void main(String[] args) {

    /*
     * Tests disques audio
     */
    // Disque 1
    AudioCD d1 = new AudioCD();
    Track[] t1 = new Track[10];

    t1[0] = new Track(1, "21 Things I Want in a Lover", DiscHelper.calculTime(3, 27));
    t1[1] = new Track(2, "Narcissus", DiscHelper.calculTime(3, 38));
    t1[2] = new Track(3, "Hands Clean", DiscHelper.calculTime(4, 30));
    t1[3] = new Track(4, "Flinch", DiscHelper.calculTime(6, 3));
    t1[4] = new Track(5, "So Unsexy", DiscHelper.calculTime(5, 8));
    t1[5] = new Track(6, "Precious Illusions", DiscHelper.calculTime(4, 10));
    t1[6] = new Track(7, "That Particular Time", DiscHelper.calculTime(4, 22));
    t1[7] = new Track(8, "A Man", DiscHelper.calculTime(4, 33));
    t1[7] = new Track(9, "You Owe Me Nothing in Return", DiscHelper.calculTime(4, 58));
    t1[8] = new Track(10, "Surrendering", DiscHelper.calculTime(4, 36));
    t1[9] = new Track(11, "Utopia", DiscHelper.calculTime(4, 58));

    d1.setArtist("Alanis Morissette");
    d1.setTitle("Under Rug Swept");
    d1.setGenre(EnumGenre.ROCK);
    d1.setTrackList(t1);
    d1.setTotalTime(DiscHelper.calculTime(0, 56, 56));

    // Disque 2
    Track[] t2 = new Track[10];

    t2[0] = new Track(1, "Please Forgive Me", DiscHelper.calculTime(5, 36));
    t2[1] = new Track(2, "Babylon", DiscHelper.calculTime(4, 26));
    t2[2] = new Track(3, "My Oh My", DiscHelper.calculTime(4, 37));
    t2[3] = new Track(4, "We're Not Right", DiscHelper.calculTime(3, 4));
    t2[4] = new Track(5, "Nightblindness", DiscHelper.calculTime(4, 23));
    t2[5] = new Track(6, "Silver Lining", DiscHelper.calculTime(6, 0));
    t2[6] = new Track(7, "White Ladder", DiscHelper.calculTime(4, 15));
    t2[7] = new Track(8, "This Years Love", DiscHelper.calculTime(4, 5));
    t2[7] = new Track(9, "Sail Away", DiscHelper.calculTime(5, 15));
    t2[8] = new Track(10, "Say Hello Wave Goodbye", DiscHelper.calculTime(9, 4));
    t2[9] = new Track(11, "Babylon II", DiscHelper.calculTime(6, 11));

    AudioCD d2 = new AudioCD("David Gray", "White ladder", t2);
    d2.setGenre(EnumGenre.FOLK);
    d2.setTrackList(t2);
    d2.setTotalTime(DiscHelper.calculTime(0, 69, 30));

    // Disque 3
    Track[] t3 = new Track[11];

    t3[0] = new Track(1, "golden age", DiscHelper.calculTime("3:41"));
    t3[1] = new Track(2, "too much sunshine", DiscHelper.calculTime("3:47"));
    t3[2] = new Track(3, "capricornia", DiscHelper.calculTime("3:18"));
    t3[3] = new Track(4, "luritja way", DiscHelper.calculTime("4:00"));
    t3[4] = new Track(5, "tone poem", DiscHelper.calculTime("4:53"));
    t3[5] = new Track(6, "a crocodile cries", DiscHelper.calculTime("1:08"));
    t3[6] = new Track(7, "mosquito march", DiscHelper.calculTime("3:09"));
    t3[7] = new Track(8, "been away too long", DiscHelper.calculTime("3:17"));
    t3[7] = new Track(9, "say your prayers", DiscHelper.calculTime("4:26"));
    t3[8] = new Track(10, "under the overpass", DiscHelper.calculTime("4:02"));
    t3[9] = new Track(11, "world that i see", DiscHelper.calculTime("4:09"));
    t3[10] = new Track(12, "poets & slaves", DiscHelper.calculTime("5:56"));

    AudioCD d3 = new AudioCD("Midnight Oil", "Capricornia", EnumGenre.ROCK, t3,
        DiscHelper.calculTime(0, 50, 52), 35.10);

    // Affichage des differents disques
    System.out.println(d1);
    System.out.println(d2);
    System.out.println(d3);

    // calcul du nombre total de morceaux
    System.out.println("\nLe nombre total de morceaux est: "
        + (d1.getNbrTrack() + d2.getNbrTrack() + d3.getNbrTrack()));

    // calcul de la duree totale
    System.out.println("La duree total est: "
        + DiscHelper.formatTime(d1.getTotalTime() + d2.getTotalTime() + d3.getTotalTime()));
    /*
     * Fin tests disques audio
     */

    System.out.println("\n**** **** ****\n");

    /*
     * Tests disques dvd
     */
    EnumLanguage langues[] = new EnumLanguage[2];
    langues[0] = EnumLanguage.FRANCAIS;
    langues[1] = EnumLanguage.ANGLAIS;

    DVD seigneur = new DVD("Le seigneur des anneaux", "Peter Jackson",
        DiscHelper.calculTime("03:30:00"), 59.90, langues, EnumZone.EUROPE);

    System.out.println(seigneur);
    /*
     * Fin tests disques audio
     */
  } // endMain
} // /:~
