package ch.rapazp.java.formation.practice.disque.compactDisc.audio;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * EnumGenre.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 17.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 2
 * 
 * Enumeration pour la gestion des styles musicaux
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public enum EnumGenre {

    BLUES("blues"),
    CLASSICAL("classical"),
    COUNTRY("country"),
    FOLK("folk"),
    JAZZ("jazz"),
    NEWAGE("newage"),
    REGGEA("reggea"),
    ROCK("rock"),
    MISC("misc"),
    POP("pop"),
    ELECTRO("electro"),
    AUTRE("autre");

  private final String style;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeur
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  EnumGenre(String style) {
    this.style = style;
  }// endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * @return String Represente le style musical
   */
  public String style() {
    return style;
  }// endFct
  
  public String toString() {
    return this.style();
  }// endFct
}// /:~
