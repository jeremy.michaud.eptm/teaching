package ch.rapazp.java.formation.practice.disque.compactDisc.audio;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * AudioCD.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 18.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.text.DecimalFormat;

import ch.rapazp.java.formation.practice.disque.compactDisc.CompactDisc;

/**
 * Exercice 2
 * 
 * Representation d'un disque audio sous forme de classe
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class AudioCD extends CompactDisc {

  private String artist = null;
  private int nbrTrack = 0;
  private boolean prixFixe = false;
  private EnumGenre genre = null;
  private Track[] trackList = null;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public AudioCD() {
  } // endConst

  /**
   * Construit un nouvel objet de type disque audio avec les champs specifies
   * 
   * @param artist
   *          Nom de l'artiste
   * @param title
   *          Titre du disque
   * @param trackList
   *          Liste des morceaux
   */
  public AudioCD(String artist, String title, Track[] trackList) {

    this.setArtist(artist);
    this.setTitle(title);
    this.setTrackList(trackList);
  } // end AudioCD

  /**
   * Construit un nouvel objet de type disque audio avec les champs specifies
   * 
   * @param artist
   *          Nom de l'artiste
   * @param title
   *          Titre du disque
   * @param genre
   *          Style musical
   * @param trackList
   *          Liste des morceaux
   * @param prix
   *          Le prix du disque
   * @param time
   *          Duree du disque
   */
  public AudioCD(String artist, String title, EnumGenre genre, Track[] trackList, long time,
      double prix) {

    this(artist, title, trackList);

    this.setGenre(genre);

    this.setTotalTime(time);
    this.setPrix(prix);
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Compare le nombre de morceaux de 2 disques
   * 
   * @param disque
   *          Le disque a comparer
   * @return int 0 le nombre de morceaux est identique, 1 l'instance courante a
   *         plus de morceaux et -1 le parametre a plus de morceaux.
   */
  public int compare(AudioCD disque) {

    // On a acces aux variables des autres disques
    // (protection d'acces par classe et pas par objet en Java)
    if (nbrTrack == disque.nbrTrack) {

      return 0;
    } else if (nbrTrack > disque.nbrTrack) {

      return 1;
    } else {

      return -1;
    } // endIf
  } // endFct

  /**
   * Retourne une <code>String</code> representant le disque.<br>
   * Les differents champs du disque sont separes par des ';' et respectent
   * l'ordre suivant:<br>
   * <i>artiste - titre - genre - nombre de morceaux - duree du disque -
   * prix</i>
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {

    StringBuffer ret = new StringBuffer();

    ret.append(artist + ";" + super.getTitle() + ";" + genre + ";" + nbrTrack + ";"
        + super.getFormatedTime() + ";");

    if (super.getPrix() == 0)
      ret.append("Prix pas encore fixe");
    else
      ret.append(new DecimalFormat("0.00").format(super.getPrix()));

    for (int i = 0; i < nbrTrack; i++) {
      ret.append("\n\t" + trackList[i]);
    } // endFor

    return ret.toString();
  } // endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Getters and Setters
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Retourne le nom de l'artiste.
   * 
   * @return String
   */
  public String getArtist() {
    return artist;
  } // endFct

  /**
   * Retourne la liste des morceaux.
   * 
   * @return Track[] La liste des Morceaux
   */
  public Track[] getTrackList() {
    return trackList;
  } // endFct

  /**
   * Retourne le nombre de morceaux.
   * 
   * @return int
   */
  public int getNbrTrack() {
    return nbrTrack;
  } // endFct

  /**
   * Retourne le genre de musique du disque.
   * 
   * @return EnumGenre
   */
  public EnumGenre getGenre() {
    return genre;
  } // endFct

  /**
   * Retourne <code>true</code> si le prix a deja ete fixe et <code>false</code>
   * si ce n'est pas le cas.
   * 
   * @return boolean L'etat du code prix
   */
  public boolean isPrixFixe() {
    return prixFixe;
  } // endFct

  /**
   * Valorise le nom de l'artiste.
   * 
   * @param artist
   *          Le nom de l'artiste
   */
  public void setArtist(String artist) {
    this.artist = artist;
  } // endFct

  /**
   * Valorise le genre de musique du disque.
   * 
   * @param genre
   *          Le genre de musique
   */
  public void setGenre(EnumGenre genre) {
    this.genre = genre;
  } // endFct

  /**
   * Valorise la liste des morceaux ainsi que le nombre de pistes du cd.
   * 
   * @param trackList
   *          Liste des morceaux
   */
  public void setTrackList(Track[] trackList) {
    this.trackList = trackList;
    this.nbrTrack = trackList.length;
  } // endFct

  /**
   * Valorise le prix du disque.
   * 
   * @param prix
   *          Le prix du disque
   */
  public void setPrix(double prix) {

    if (prixFixe) {

      System.err.println("Erreur: le prix est deja fixe!");
    } else if (prix > 0) {

      super.setPrix(prix);
      prixFixe = true;
    } else {

      System.err.println("Erreur: le prix est negatif!");
    } // endIf
  } // endFct
} // /:~
