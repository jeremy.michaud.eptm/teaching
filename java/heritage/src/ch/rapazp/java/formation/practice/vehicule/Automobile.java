package ch.rapazp.java.formation.practice.vehicule;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Automobile.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 1
 * 
 * Classe automobile heritant de la classe de base Vehicule
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Automobile extends Vehicule {

  private int nbrPlaces = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Construit une nouvelle auto
   * 
   * @param genre
   *          Le genre de vehicule
   * @param immatriculation
   *          Le numero d'immatriculation du vehicule
   * @param proprietaire
   *          Le nom du proprio
   * @param nbrPlaces
   *          Le nombre de places
   */
  public Automobile(String genre, String immatriculation, String proprietaire, int nbrPlaces) {

    super(genre, immatriculation, proprietaire);
    this.nbrPlaces = nbrPlaces;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Retourne les donnees du vehicule.
   * 
   * @return Les donnees du vehicule (genre, immatriculation, proprietaire et
   *         nombre de places)
   */
  public String toString() {
    return super.toString() + "\nNombre places: " + nbrPlaces + "\n";
  } // endFct
} // /:~
