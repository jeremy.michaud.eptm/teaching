package ch.rapazp.java.formation.practice.disque.compactDisc.audio;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Track.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 19.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import ch.rapazp.java.formation.practice.disque.compactDisc.DiscHelper;

/**
 * Exercice 2
 * 
 * Decrit un morceau de l'album
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Track {

  private int number = 0;
  private String title = null;
  private long time = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Construit un objet qui decrit un morceau
   * 
   * @param number
   *          Le numero du morceau
   * @param title
   *          Le titre du morceau
   * @param time
   *          La duree du morceau
   */
  public Track(int number, String title, long time) {

    this.setNumber(number);
    this.setTitle(title);
    this.setTime(time);
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public String toString() {

    return number + ";" + title + ";" + DiscHelper.formatTime(time);
  } // endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Getters and Setters
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public int getNumber() {
    return number;
  }

  public long getTime() {
    return time;
  }

  public String getTitle() {
    return title;
  }

  public void setNumber(int number) {
    this.number = number;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public void setTitle(String title) {
    this.title = title;
  }

} // /:~
