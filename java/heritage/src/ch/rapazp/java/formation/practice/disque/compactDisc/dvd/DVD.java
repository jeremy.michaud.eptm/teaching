package ch.rapazp.java.formation.practice.disque.compactDisc.dvd;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * DVD.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 18.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.text.DecimalFormat;

import ch.rapazp.java.formation.practice.disque.compactDisc.CompactDisc;

/**
 * Exercice 2
 * 
 * Representation d'un digital versatil disc sous forme de classe
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class DVD extends CompactDisc {

  private String realizer = null;
  private EnumZone zone = null;
  private EnumLanguage[] languageList = null;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public DVD() {
  } // endConst

  /**
   * Construit un nouveau DVD
   * 
   * @param title
   *          Titre du disque
   * @param realizer
   *          Le nom du realisateur
   * @param totalTime
   *          Duree totale du disque
   * @param prix
   *          Prix du disque
   * @param languageList
   *          Liste des langues a disposition
   * @param zone
   *          La zone du DVD
   */
  public DVD(String title, String realizer, long totalTime, double prix,
      EnumLanguage[] languageList, EnumZone zone) {

    super(title, totalTime, prix);
    this.setRealizer(realizer);
    this.setLanguageList(languageList);
    this.setZone(zone);
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public String toString() {

    StringBuilder ret = new StringBuilder();

    ret.append(super.getTitle() + ";" + realizer + ";" + super.getFormatedTime() + ";" + zone + ";");

    ret.append(new DecimalFormat("0.00").format(super.getPrix()));

    for (int i = 0; i < languageList.length; i++) {
      ret.append("\n\t" + languageList[i]);
    } // endFor

    return ret.toString();

  } // endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Getters and Setters
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public String getRealizer() {
    return realizer;
  }

  public EnumLanguage[] getLanguageList() {
    return languageList;
  }

  public EnumZone getZone() {
    return zone;
  }

  public void setRealizer(String realizer) {
    this.realizer = realizer;
  }

  public void setLanguageList(EnumLanguage[] languageList) {
    this.languageList = languageList;
  }

  public void setZone(EnumZone zone) {
    this.zone = zone;
  }

} // /:~
