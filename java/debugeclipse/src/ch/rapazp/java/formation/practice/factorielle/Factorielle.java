package ch.rapazp.java.formation.practice.factorielle;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Factorielle.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.02.2003     1.0       Creation
 * 03.02.2014     1.1       Remplacement des Buffer par un Scanner
 * -----------------------------------------------------------------------------
 */

import java.util.Scanner;

/**
 * Exemple d'utilisation du debugger d'Eclipse.
 *
 * @author Pascal Rapaz
 * @version 1.1
 */

public class Factorielle {

  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);

    System.out.println("Entrer la valeur de n :");

    // Recuperation de la saisie de l'utilisateur
    int inputInt = scanner.nextInt();
    int resFactorielle = factorielle(inputInt);

    // affichage du resultat
    System.out.println(inputInt + "! = " + resFactorielle);

    scanner.close();
  }// endMain

  /**
   * Calcule la factorielle d'un nombre (n!)
   * 
   * @param n
   *          nombre > 0
   * @return n!
   * @version 1.0
   */
  public static int factorielle(int n) {

    if (n <= 0)
      return 1;
    else
      return n * factorielle(n - 1);

  }// endFct
}// /:~
