package ch.rapazp.java.formation.practice.interfaces.tests;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * AllTests.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.03.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BaseTest.class, CompareTest.class, TriTest.class })
public class AllTests {
}// /:~
