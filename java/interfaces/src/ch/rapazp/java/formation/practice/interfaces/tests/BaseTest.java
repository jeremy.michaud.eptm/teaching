package ch.rapazp.java.formation.practice.interfaces.tests;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * BaseTest.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.03.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import ch.rapazp.java.formation.practice.interfaces.base.Priorite;
import ch.rapazp.java.formation.practice.interfaces.base.Tache;

@RunWith(value = Parameterized.class)
public class BaseTest {

  private Tache task;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public BaseTest(String description, int priorite) {

    task = new Tache(description);
    task.setPriorite(priorite);
  }// endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Test case
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  @Parameters
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {

        { "Préparer formation Java", Priorite.MAX_PRIORITE },
        { "Mettre en place tests unitaires du module 'interfaces'", Priorite.MED_PRIORITE },
        { "Mettre à jour le site web", 2 },

    });
  }

  @Test
  public void test() {

    assertNotNull("La tâche ne doit pas être null", task);
    System.out.println(task);
  }// endTest
} // /:~