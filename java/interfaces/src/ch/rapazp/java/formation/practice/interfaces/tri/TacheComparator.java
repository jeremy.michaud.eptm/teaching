package ch.rapazp.java.formation.practice.interfaces.tri;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * TacheComparator.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.10.2003     1.0       Creation
 * 06.11.2003     1.1       Adaptation methode compare(o1, o2) selon solution 
 *                          proposee par WH
 * 13.03.2014     1.2       Ajout des generics pour l'interface Comparable
 * -----------------------------------------------------------------------------
 */

import java.util.Comparator;

/**
 * Exercice 3
 * 
 * Comparateur de tache
 * 
 * @author Rodrigue Vaudan
 * @author Pascal Rapaz
 * @version 1.2
 */
public class TacheComparator implements Comparator<Tache> {

  private boolean triAsc = false;

  /**
   * Constructeur
   * 
   * @param triAsc
   *          Defini si le tri sera ascendant sur la priorite (true) ou
   *          descendant (false)
   */
  public TacheComparator(boolean triAsc) {
    this.triAsc = triAsc;
  } // endConst

  /*
   * (non-Javadoc)
   * 
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  public int compare(Tache t1, Tache t2) {

    if (triAsc) {
      return t1.compareTo(t2);
    } // end if

    return t2.compareTo(t1);
  } // end compare
} // /:~
