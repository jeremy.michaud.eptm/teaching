package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2003-2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * CenteredRectangle.java.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 23.10.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Permet la creation d'un rectangle avec les coordonnes de son centre
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class CenteredRectangle extends Rectangle implements Centered {

  private double centerX = 0;
  private double centerY = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Construit un rectangle avec ses coordonnées de centre
   * 
   * @param centerX
   *          coordonnee X du centre
   * @param centerY
   *          coordonnee Y du centre
   * @param largeur
   *          longueur de la largeur du rectangle
   * @param hauteur
   *          hauteur du rectangle
   */
  public CenteredRectangle(double centerX, double centerY, double largeur, double hauteur) {

    super(largeur, hauteur);
    setCenter(centerX, centerY);
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Implementation des methodes de l'interface
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * (non-Javadoc)
   * 
   * @see ch.rapazp.java.formation.sample.Centered#setCenter(double, double)
   */
  public void setCenter(double x, double y) {

    centerX = x;
    centerY = y;
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see ch.rapazp.java.formation.sample.Centered#getCenterX()
   */
  public double getCenterX() {

    return centerX;
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see ch.rapazp.java.formation.sample.Centered#getCenterY()
   */
  public double getCenterY() {

    return centerY;
  } // endFct
} // /:~
