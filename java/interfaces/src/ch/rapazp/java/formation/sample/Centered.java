package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2003-2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Centered.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 23.10.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Interface pour la definition du centre d'une forme geometrique
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public interface Centered {

  /**
   * Defini les coordonnee du centre
   * 
   * @param x
   *          coordonnee X du centre
   * @param y
   *          coordonnee Y du centre
   */
  public void setCenter(double x, double y);

  public double getCenterX();

  public double getCenterY();
} // /:~
