package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2003-2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Shape.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 09.10.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Classe abstraite de base pour la representation de formes geometriques
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public abstract class Shape {

  public abstract double area();

  public abstract double perimeters();
} // /:~
