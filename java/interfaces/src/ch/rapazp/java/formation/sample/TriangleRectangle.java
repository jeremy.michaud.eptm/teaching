package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2003-2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * TriangleRectangle.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 23.10.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * TriangleRectangle heritant de la classe abstraite 'Shape'
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class TriangleRectangle extends Shape {

  private double coteA = 0;
  private double coteB = 0;
  private double coteC = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Construit un nouveau triangle rectangle
   * 
   * @param base
   *          longueur de la base du triangle
   * @param hauteur
   *          hauteur du triangle
   * @param diag
   *          longueur d'une diagonale du triangle
   */
  public TriangleRectangle(double base, double hauteur, double diag) {

    coteA = base;
    coteB = hauteur;
    coteC = diag;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Implementation des methodes abstraites
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * (non-Javadoc)
   * 
   * @see ch.rapazp.java.formation.sample.Shape#area()
   */
  public double area() {

    return coteA * coteB / 2;
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see ch.rapazp.java.formation.sample.Shape#perimeters()
   */
  public double perimeters() {

    return coteA + coteB + coteC;
  } // endFct
} // /:~
