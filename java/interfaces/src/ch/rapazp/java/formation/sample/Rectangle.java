package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2003-2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Rectangle.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 09.10.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Rectangle heritant de la classe abstraite 'Shape'
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Rectangle extends Shape {

  private double hauteur = 0;
  private double base = 0;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Construit un nouveau rectangle
   * 
   * @param base
   *          longueur de la base du rectangle
   * @param hauteur
   *          hauteur du rectangle
   */
  public Rectangle(double base, double hauteur) {

    this.base = base;
    this.hauteur = hauteur;
  } // endConst

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Implementation des methodes abstraites
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * (non-Javadoc)
   * 
   * @see Shape#area()
   */
  public double area() {

    return base * hauteur;
  } // endFct

  /*
   * (non-Javadoc)
   * 
   * @see Shape#circumference()
   */
  public double perimeters() {

    return (2 * base) + (2 * hauteur);
  } // endFct
} // /:~
