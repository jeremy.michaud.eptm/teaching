package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Vectors.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 14.11.2002     1.0       Creation
 * 10.01.2014	  1.1       Migration Java 1.7
 * -----------------------------------------------------------------------------
 */

/**
 * Exemples d'utilisation des tableaux.
 * 
 * @author Pascal Rapaz
 * @version 1.1
 */
public class Vectors {
  public static void main(String[] args) {
    // creation et copie d'un tableau dans une vraiable tableau
    int[] petitPremiers = { 2, 3, 12, 7, 11, 13 };
    int[] grandDerniers = petitPremiers;

    System.out.println("Avant modif petitPremiers: " + toString(petitPremiers));
    System.out.println("Avant modif grandDerniers: " + toString(grandDerniers));

    petitPremiers[2] = 5; // grandDerniers[2] vaut egalement 5

    System.out.println("APRES modif petitPremiers: " + toString(petitPremiers));
    System.out.println("APRES modif grandDerniers: " + toString(grandDerniers));

    // Reaffectation de la variable grandDerniers
    grandDerniers = new int[] { 1001, 1002, 1003, 1004, 1005, 1006, 1007 };

    System.out.println();
    System.out.println("petitPremiers vaut: " + toString(petitPremiers));
    System.out.println("grandDerniers vaut: " + toString(grandDerniers));

    // Copie du contenu d'un tableau dans un autre
    System.arraycopy(petitPremiers, 2, grandDerniers, 3, 4);

    System.out.println("Apres copie, grandDerniers vaut: "
        + toString(grandDerniers));
  }// endMain

  /**
   * Convertit un tableau en chaine de caracteres
   * 
   * @param tableau
   *          Le tableau a convertir
   * @return String Une chaine de caractere contenant les elements d'un tableau
   */
  public static String toString(int[] tableau) {
    StringBuilder sb = new StringBuilder();
    for (int entree : tableau) {
      sb.append(entree + " ");
    }// endFor

    return sb.toString();
  }// endFct
}// /:~
