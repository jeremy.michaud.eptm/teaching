package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * ShellSort.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 14.11.2002     1.0       Creation
 * 10.01.2014	    1.1       Migration Java 1.7
 * -----------------------------------------------------------------------------
 */

/**
 * Exemple de passage d'un tableau en tant qu'argument a une methode.
 * 
 * Remarque : la methode sort utilise l'algorithme de tri ShellSort
 * 
 * @source Au coeur de Java 2 - chap3 pg107
 * @author Pascal Rapaz
 * @version 1.1
 */
public class ShellSort {
  public static void main(String[] args) {
    // cree un tableau de 10 entiers
    int[] numbers = new int[10];

    // rempli le tableau de maniere aleatoire
    for (int i = 0; i < numbers.length; i++)
      numbers[i] = (int) (Math.random() * 100);

    System.out.println("Non trie: " + toString(numbers));
    sort(numbers);
    System.out.println("Trie    : " + toString(numbers));
  }// endMain

  /**
   * Tri le tableau selon l'algorithmne de tri ShellSort.
   * 
   * @param tab
   *          Tableau contenant les valeurs a trier
   */
  public static void sort(int[] tab) {
    int n = tab.length;
    int incr = n / 2;

    while (incr > 0) {
      for (int i = incr; i < n; i++) {
        int tmp = tab[i];
        int j = i;

        while (j >= incr && tmp < tab[j - incr]) {
          tab[j] = tab[j - incr];
          j -= incr;
        }// endWhile

        tab[j] = tmp;
      }// endFor

      incr /= 2;
    }// endWhile
  }// endFct

  /**
   * Convertit un tableau en chaine de caracteres
   * 
   * @param tableau
   *          Le tableau a convertir
   * @return String Une chaine de caractere contenant les elements d'un tableau
   */
  public static String toString(int[] tableau) {
    StringBuilder sb = new StringBuilder();
    for (int entree : tableau) {
      sb.append(entree + " ");
    }// endFor

    return sb.toString();
  }// endFct
}// /:~
