package ch.rapazp.java.formation.pratice.truefalse;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * TrueFalse.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 14.11.2002     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 1
 * 
 * Creation statique d'un tableau de boolean.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class TrueFalse {

  public static void main(String[] args) {

    boolean[] tab = { true, false, true };

    System.out.println("Le dernier element du tableau est: "
        + tab[tab.length - 1]);
  } // endMain
} // /:~
