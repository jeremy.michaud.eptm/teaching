package ch.rapazp.java.formation.pratice.splitstring;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * SplitString.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.util.Arrays;

/**
 * Exercice 3
 *
 * Exemple de manipulation de String.
 *
 * @author Pascal Rapaz
 * @version 1.0
 */
public class SplitString {

  public static void main(String args[]) {
    String[] values = "12;ab;cd;AB;fx;ef;QA".split(";");
    Arrays.sort(values);

    for (String s : values) {
      System.out.println(s);
    }// endFor
  }// endMain
}// /:~
