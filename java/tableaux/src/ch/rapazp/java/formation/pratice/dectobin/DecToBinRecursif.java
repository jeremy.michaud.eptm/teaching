package ch.rapazp.java.formation.pratice.dectobin;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * DecToBinRecursif.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 17.12.2002     1.0       Creation
 * 16.01.2003     1.1       Support des ArrayList et des LinkedList
 * 10.01.2014	    1.2       Migration Java 1.7
 * -----------------------------------------------------------------------------
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Exercice 7
 * 
 * Converti un nombre decimal en binaire de maniere recursive.
 * 
 * @author Pascal Rapaz
 * @version 1.2
 */
public class DecToBinRecursif {

  public static void main(String[] args) {

    // if (args.length == 0) {
    // System.out.println("Utilisation: java DecToBinRecursif nbr");
    // System.exit(1);
    // }// endif
    //
    // new DecToBinRecursif(Integer.parseInt(args[0]));

    new DecToBinRecursif(25);
  }// endMain

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Effectue la conversion d'un nombre decimal en binaire de maniere recursive.
   * 
   * @param val
   *          Le nombre a convertir
   */
  public DecToBinRecursif(int val) {

    System.out.println("ArrayList : " + this.toString(this.convertArray(val)));
    System.out.println("LinkedList: " + this.toString(this.convertLinked(val)));

    /*
     * Utilise la methode toString() par defaut de la classe AbstractCollection
     * dont herite la classe ArrayList et LinkedList
     */
    System.out.println("ArrayList  (default toString): " + this.convertArray(val));
    System.out.println("LinkedList (default toString): " + this.convertLinked(val));
  }// endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * Retourne une <code>List</code> contenant la valeur binaire d'un nombre.
   * 
   * @param nbr
   *          Le nombre a convertir
   * @return List Representation binaire du nombre
   */
  public ArrayList<Integer> convertArray(int nbr) {
    ArrayList<Integer> list = new ArrayList<Integer>();

    _convert(nbr, (Collection<Integer>) list);

    return list;
  }// endFct

  /**
   * Retourne une <code>List</code> contenant la valeur binaire d'un nombre.
   * 
   * @param nbr
   *          Le nombre a convertir
   * @return List Representation binaire du nombre
   */
  public LinkedList<Integer> convertLinked(int nbr) {
    LinkedList<Integer> list = new LinkedList<Integer>();

    _convert(nbr, (Collection<Integer>) list);

    return list;
  }// endFct

  /**
   * Methode de service effectuant la conversion binaire d'un nombre de maniere
   * recursive.
   * 
   * On divise par 2 jusqu'a ce que l'on ai un dividende de 0. Ensuite on
   * remonte et on ajoute le reste de la division dans un nouvel objet de type
   * <code>Integer</code>. Au final, il n'y a pas besoin de renverser le contenu
   * de la collection car elle est triee lors de la creation.
   * 
   * @param nbr
   *          Le nombre a convertir
   * @param list
   *          Liste a affectee lors du calcul. Cette liste est de type
   *          Collection afin de garantir la reutilisabilite de la methode.
   */
  private void _convert(int nbr, Collection<Integer> list) {
    // clause de finitude
    if (nbr == 0)
      return;

    // pas recursif
    _convert(nbr / 2, list);
    list.add(new Integer(nbr % 2));
  }// endFct

  /**
   * Retourne une <code>Collection</code> comme chaine de caracteres.
   * 
   * @param list
   *          La liste a convertir
   * @return String Une chaine representant le contenu de l'LinkedList
   */
  public String toString(Collection<Integer> list) {

    StringBuilder ret = new StringBuilder();

    for (int val : list)
      ret.append(val);

    return ret.toString();
  }// endFct
}// /:~
