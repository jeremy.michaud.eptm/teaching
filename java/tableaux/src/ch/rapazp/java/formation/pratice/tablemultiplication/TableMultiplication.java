package ch.rapazp.java.formation.pratice.tablemultiplication;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * TableMultiplication.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 14.11.2002     1.0       Creation
 * 10.01.2014	    1.1       Migration Java 1.7
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 4
 * 
 * Affiche une table de multiplication 20*20.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class TableMultiplication {
  static final int TAILLE = 20;
  int[][] table = null;

  /**
   * Cree une nouvelle table de multiplication.
   */
  public TableMultiplication() {
    table = new int[TAILLE][TAILLE];

    for (int i = 0; i < TAILLE; i++)
      for (int j = 0; j < TAILLE; j++)
        table[i][j] = (i + 1) * (j + 1);
  }// endFct

  /**
   * Affiche la table de multiplication.
   */
  public void afficheTable() {

    for (int[] ligne : table) {
      System.out.println();

      for (int element : ligne)
        System.out.print(element + " ");
    }// endFor
  }// endFct

  public static void main(String[] args) {
    TableMultiplication t = new TableMultiplication();
    t.afficheTable();
  }// endMain
}// /:~
