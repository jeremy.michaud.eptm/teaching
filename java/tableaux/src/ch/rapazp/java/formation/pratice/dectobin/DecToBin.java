package ch.rapazp.java.formation.pratice.dectobin;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * DecToBin.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 13.12.2002     1.0       Creation
 * 10.01.2014	    1.1       Migration Java 1.7
 * -----------------------------------------------------------------------------
 */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

/**
 * Exercice 7
 * 
 * Converti un nombre numerique en un nombre binaire.
 * 
 * 
 * Choisir son objet List:
 * 
 * - Les tableaux sont plus rapides que n'importe quel conteneur pour les acces
 * aleatoires et les iterations.
 * 
 * - Les acces aleatoires (via <i>get()</i>) sont bon marche pour les
 * <b>ArrayLists</b> et couteux pour les <b>LinkedLists</b>.
 * 
 * - L'iteration est plus rapide pour une <b>LinkedList</b> que pour une
 * <b>ArrayList</b>.
 * 
 * - Les insertions et les suppressions au milieu d'une liste sont
 * spectaculairement meilleur marche pour une <b>LinkedList</b> que pour une
 * <b>ArrayList</b> (particulierement les suppressions).
 * 
 * - Les <b>Vector</b> doivent etre evites! Ils ne sont presents dans la
 * bibliotheque que pour fournir une compatibilite ascendante avec le code
 * existant.
 * 
 * - La meilleure approche est de choisir une <b>ArrayList</b> par defaut, et de
 * changer pour une LinkedList si on decouvre des problemes de performance dus a
 * de nombreuses insertions et suppressions au milieu de la liste.
 * 
 * source http://penserenjava.free.fr/pens/indexMain_10&10.htm
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class DecToBin {

  private ArrayList<Integer> arrayList = null;
  private LinkedList<Integer> linkedList = null;
  private Vector<Integer> vector = null;

  public static void main(String[] args) {

    DecToBin dtb = new DecToBin(177);
    System.out.println(dtb);
  } // endMain

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Constructeurs
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public DecToBin(int nbr) {

    int val;

    arrayList = new ArrayList<Integer>();
    linkedList = new LinkedList<Integer>();
    vector = new Vector<Integer>();

    while (nbr > 0) {
      val = nbr % 2;

      arrayList.add(0, val);
      linkedList.addFirst(val);
      vector.add(0, val);

      nbr /= 2;
    }// endWhile
  }// endFct

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Methodes
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  public String toString() {

    StringBuilder res = new StringBuilder();

    /* Traitement de l'objet a base d'une ArrayList */
    res.append("Utilisation d'une ArrayList: ");

    for (int val : arrayList) {
      res.append(String.valueOf(val));
    }// endFor

    /* Traitement de l'objet a base d'une LinkedList */
    res.append("\n\nUtilisation d'une LinkedList: ");

    for (int val : linkedList) {
      res.append(String.valueOf(val));
    }// endFor

    /* Traitement de l'objet a base d'un Vector */
    res.append("\n\nUtilisation d'un Vecteur: ");

    for (int val : vector) {
      res.append(String.valueOf(val));
    }// endFor

    return res.toString();
  }// endFct
}// /:~
