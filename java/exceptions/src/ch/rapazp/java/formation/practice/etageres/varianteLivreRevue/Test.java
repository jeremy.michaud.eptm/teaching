package ch.rapazp.java.formation.practice.etageres.varianteLivreRevue;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * Test.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 25.04.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 4
 * 
 * Classe de test pour les etagere contenant des livres et des revues.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Test {

  public static void main(String[] args) {

    Etagere monEtagere = new Etagere(8);

    try {
      monEtagere.poser(new Revue("Linux Planète", "KDE 3.1 vs Gnome 2.2", 10.30));
      monEtagere.poser(new Livre("Cadavre X", "Patricia Cornwell", "Livre de Poche"));
      monEtagere.poser(new Livre("EJB fondamental", "Sun", "Sun Edition"));
      monEtagere.poser(new Livre("Au coeur de Java", "Java", "Java Edition"));
      monEtagere.poser(new Revue("Login", "Strategie de Sun envers Linux", 12.30));
      monEtagere.poser(new Revue("Voici", "Caroline va-t-elle enfin commencer à chanter?", 9.90));
      monEtagere.poser(new Livre("C++", "Strustrup", "Strustrup Edition"));
      monEtagere.poser(new Livre("Le CID", "Corneille", "France Loisirs"));
      monEtagere.poser(new Livre("La cuisine pour tous", "Marie-Thé", "FR3"));
    } catch (EtagereException ex) {
      System.out.println(ex.getMessage());
    } // endTry

    /*
     * Affichage de l'etagere variante 1:
     * 
     * Utilisation implicite de la methode toString de la classe Etagere.
     */
    System.out.println(monEtagere);

    /*
     * Affichage de l'etagere variante 2:
     * 
     * J'affiche les infos en fonction de l'objet recupere. Pour ce faire, on
     * utilise le casting por appeler les methodes desirees.
     */
    try {
      Object tmp;

      for (int i = 0; i < 10; i++) {
        tmp = monEtagere.getElement(i);

        if (tmp instanceof Livre) {
          System.out.println("Livre: " + ((Livre) tmp).getAuteur() + "\n");
        } else {
          System.out.println("Revue: " + ((Revue) tmp).getTheme() + "\n");
        } // endIf

      } // endFor
    } catch (EtagereException ex) {
      System.err.println(ex.getMessage());
    } // endTry
  } // endMain
} // /:~