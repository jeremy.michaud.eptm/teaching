package ch.rapazp.java.formation.practice.etageres.varianteLivre;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * Test.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 3
 * 
 * Classe de test pour les etagere contenant uniquement des livres.
 * 
 * @author Rodrigue Vaudan
 * @version 1.0
 */
public class Test {

  public static void main(String[] args) {
    Etagere monEtagere = new Etagere();

    try {
      monEtagere.poserLivre(new Livre("Cadavre X", "Patricia Cornwell", "Livre de Poche"));
      monEtagere.poserLivre(new Livre("EJB fondamental", "Sun", "Sun Edition"));
      monEtagere.poserLivre(new Livre("Au coeur de Java", "Java", "Java Edition"));
      monEtagere.poserLivre(new Livre("C++", "Strustrup", "Strustrup Edition"));
      monEtagere.poserLivre(new Livre("Le CID", "Corneille", "France Loisirs"));
      monEtagere.poserLivre(new Livre("La cuisine pour tous", "Marie-Thé", "FR3"));
    } catch (EtagerePleineException ex) {
      System.err.println(ex.getMessage());
    } // endTry

    System.out.println(monEtagere);
  } // endMain
} // /:~