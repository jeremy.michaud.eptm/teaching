package ch.rapazp.java.formation.practice.etageres.varianteLivreRevue;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * EtagereException.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 4
 * 
 * Erreur survenant sur une etagere
 * 
 * @author Rodrigue Vaudan
 * @version 1.0
 */
public class EtagereException extends Exception {

  public EtagereException(String str) {
    super(str);
  } // endConst
} // /:~

