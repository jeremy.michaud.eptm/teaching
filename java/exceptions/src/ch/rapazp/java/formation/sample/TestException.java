package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * TestException.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */
public class TestException {

  public static void uneMethode(int a, int b) throws Exception {
    if (a == b) {
      throw new Exception("A égal B");
    } else {
      System.out.println("OK!");
    }// endIf
  }// endFct

  public static void main(String[] args) {
    uneMethode(2, 2);
  }// endMain
}// /:~