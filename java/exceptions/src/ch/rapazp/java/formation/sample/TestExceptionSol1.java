package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * TestExceptionSol1.java
 *
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */
public class TestExceptionSol1 {

  public static void uneMethode(int a, int b) {
    try {
      if (a == b) {
        throw new Exception("A �gal B");
      } else {
        System.out.println("OK!");
      }// endIf
    } catch (Exception ex) {
      System.out.println("Exception : " + ex);
    }// endTry
  }// endFct

  public static void main(String[] args) {
    uneMethode(2, 2);
  }// endMain
}// /:~
