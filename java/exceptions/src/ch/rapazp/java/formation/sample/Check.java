package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * Check.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */
public class Check {

  public static void checkString(String str) {
    if (str == null) {
      throw new NullPointerException("string vaut 'null'");
    }// endIf
  }// endFct

  public static void main(String[] args) {
    String myString = null;
    checkString(myString);
  }// endMain
}// /:~