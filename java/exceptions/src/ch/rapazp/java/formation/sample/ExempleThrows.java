package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * ExempleThrows.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */
public class ExempleThrows {

  public static void methodeAppelante() throws Exception {
    String str = null;

    methode(str);
  }// endFct

  public static void methode(String str) throws Exception {
    if (str == null) {
      throw new Exception("la string vaut 'null' !");
    }// endIf

    System.out.println("La string vaut : '" + str + "'");
  }// endFct

  public static void main(String[] args) {
    try {
      methodeAppelante();
    } catch (Exception ex) {
      System.out.println("Exception : " + ex);
    }// endTry
  }// endMain
}// /:~
