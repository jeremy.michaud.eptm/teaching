package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * ExempleThrow.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */
public class ExempleThrow {

  public static void methode(String str) {
    try {
      if (str == null) {
        NullPointerException exception = new NullPointerException("la string vaut 'null' !");
        throw exception;
        // ou en une seule instruction :
        // throw new NullPointerException("la string vaut 'null' !");
      }// endIf
    } catch (NullPointerException ex) {
      System.out.println("Exception : " + ex);
    }// endTry
  }// endFct

  public static void main(String[] args) {
    String str = null;

    methode(str);
  }// endMain
}// /:~