package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2002, 2003 by Rodrigue Vaudan
 * -----------------------------------------------------------------------------
 *
 * TestSaisie.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 20.02.2003     1.0       Creation
 * -----------------------------------------------------------------------------
 */
public class TestSaisie {
  public static void controle(String chaine) throws SaisieErroneeException {
    if (chaine.equals("") == true)
      throw new SaisieErroneeException("Saisie erronee : chaine vide");
  }// endFct

  public static void main(String[] args) {
    String chaine1 = "bonjour";
    String chaine2 = "";

    try {
      controle(chaine1);
    } catch (SaisieErroneeException e) {
      System.out.println("Chaine1 saisie erronee : " + e);
    }// endTry

    try {
      controle(chaine2);
    } catch (SaisieErroneeException e) {

      System.out.println("Chaine2 saisie erronee\n" + e);
      e.printStackTrace();
    }// endTry
  }// endMain
}// /:~