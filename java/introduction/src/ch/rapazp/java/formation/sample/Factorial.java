package ch.rapazp.java.formation.sample;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Factorial.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 08.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Calcul la valeur factorielle d'un nombre (ex: 4! = 4*3*2*1 = 24)
 */
public class Factorial {

  // Ici debute le programme
  public static void main(String[] args) {
    int input = Integer.parseInt(args[0]);
    double result = factorial(input);

    System.out.println(result);
  }// endMain

  // Cette methode calcul x!
  public static double factorial(int x) {
    if (x < 0)
      return 0.0; // retour egal 0.0 si l'entree n'est pas valide

    double fact = 1.0;

    while (x > 1) {
      fact *= x;
      x -= 1;
    }// endWhile

    return fact;
  }// endFct
}// /:~
