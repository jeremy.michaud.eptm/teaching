package ch.rapazp.java.formation.practice.nombrepremiers;

/* -------------------------------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -------------------------------------------------------------------------------------------------
 *
 * NombrePremiers.java
 *
 * -------------------------------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 18.01.2002     1.0       Creation
 * -------------------------------------------------------------------------------------------------
 */

/**
 * Exercice 8
 * 
 * Affiche les 20 premiers nombres premiers :
 * 
 * 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class NombrePremiers {

  public static void main(String[] args) {

    int racine;
    boolean isPremier;

    int cpt = 0;

    // boucle infine car je ne connais pas la borne max qui permet d'afficher les 20 premiers
    // nombre premiers
    for (int i = 3;; i++) {

      // Ne test pas les nombres pairs
      if (i % 2 == 0) {
        continue;
      }// endIf

      // on limite le test sur les nombres impairs et jusqu'au plus grand diviseur du nombre a
      // tester
      isPremier = true;
      racine = (int) Math.sqrt(i);

      for (int j = 3; j <= racine; j += 2) {

        if (i % j == 0) {
          // le nombre n'est pas premier... j'arrete la boucle
          isPremier = false;
          break;
        }// endIf
      }// endFor

      if (isPremier) {

        System.out.print(i + " ");
        cpt++;

        // quota atteint, je quitte
        if (cpt == 20) {
          break;
        }// endIf
      }// endIf
    }// endFor
  }// endMain
} // /:~
