package ch.rapazp.java.formation.practice.pi;

/* -------------------------------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -------------------------------------------------------------------------------------------------
 *
 * Pi.java
 *
 * -------------------------------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 19.01.2014     1.0       Creation
 * 21.01.2014     1.1       Fix: nombre d'iterations dans l'implementation de la méthode leibnizFormula
 * -------------------------------------------------------------------------------------------------
 */

/**
 * Exercice 9
 * 
 * Implementation de la formule de Leibniz permettant de calculer une approximation du nombre PI
 * (http://fr.wikipedia.org/wiki/Pi)
 * 
 * PI = 4/1 - 4/3 + 4/5 - 4/7 + 4/9 - 4/11 + ...
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class Pi {

  public static void main(String[] args) {

    System.out.println("Formule de Leibniz:");

    System.out.println(leibnizFormula(10));
    System.out.println(leibnizFormula(100));
    System.out.println(leibnizFormula(1000000));
    System.out.println(leibnizFormula(100000000));

    System.out.println("Representation Java de PI:\n" + Math.PI);
  }// endMain

  /**
   * Implementation de la formule de Leibniz
   * 
   * @param nbrIterations
   *          Nombre d'itération avant de retourner le resultat. Un nombre eleve d'iteration donnera
   *          un resultat plus precis
   * @return double approximation du nombre PI
   */
  public static double leibnizFormula(int nbrIterations) {

    nbrIterations *= 2;

    double pi = 0.0;
    boolean mustAdd = true;

    for (int i = 1; i < nbrIterations; i += 2) {

        if (mustAdd) {

          pi += 4.0 / i;
        } else {

          pi -= 4.0 / i;
        }// endIf

        mustAdd = !mustAdd;
    }// endFor

    return pi;
  }// endFct
}// /:~
