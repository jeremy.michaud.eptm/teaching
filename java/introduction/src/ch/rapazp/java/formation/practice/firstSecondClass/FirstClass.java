package ch.rapazp.java.formation.practice.firstSecondClass;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * FirstClass.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 09.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.util.Locale;

/**
 * Exercice 2 (fourni via google drive)
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class FirstClass {

  /**
   * Methode getString.
   * 
   * @return String
   */
  public String getString() {
    return Locale.getDefault().getDisplayName();
  }// endFct
}// /:~
