package ch.rapazp.java.formation.practice.firstSecondClass;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * SecondClass.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 09.01.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 2 (fourni via google drive)
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class SecondClass {

  public static void main(String[] args) {

    FirstClass stringGetter = new FirstClass();
    System.out.println(stringGetter.getString());
  }// endMain
}// /:~
