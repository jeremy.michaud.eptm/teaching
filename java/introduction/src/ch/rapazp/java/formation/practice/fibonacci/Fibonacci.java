package ch.rapazp.java.formation.practice.fibonacci;

/* -----------------------------------------------------------------------------
 * (c) 2002 - 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * Fibonacci.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 17.11.2002     1.0       Creation
 * 15.11.2002     1.1       Modifie selon solution LB
 * -----------------------------------------------------------------------------
 */

/**
 * Exercice 4
 * 
 * Calcul les 20 premiers nombres de Fibonacci.
 * 
 * ex: 1, 1, 2, 3, 5, 8, 13, etc
 * 
 * @author Pascal Rapaz
 * @version 1.1
 */
public class Fibonacci {

  /** debut du pragramme */
  public static void main(String[] args) {

    // initialise les variables
    int nbr1 = 0;
    int nbr2 = 1;
    int res;

    // boucle sur les 20 prochains termes
    for (int i = 0; i < 20; i++) {
      res = nbr1 + nbr2; // Calcul les terme suivant

      // decale les nombre pour l'iteration suivante
      nbr2 = nbr1;
      nbr1 = res;

      System.out.print(res + " "); // affiche le nouveau terme
    } // endFor

    System.out.println(); // termine la ligne
  } // endMain
} // /:~
