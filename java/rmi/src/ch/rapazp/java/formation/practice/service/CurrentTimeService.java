package ch.rapazp.java.formation.practice.service;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * CurrentTimeService.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 24.06.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Service pour l'appel distant des methodes permettant de recuperer l'heure du
 * serveur.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public interface CurrentTimeService extends Remote {

  /**
   * Retourne la date et l'heure actuelle du serveur
   * 
   * @return Un <code>long</code> representant la date et l'heure du serveur en
   *         millisecondes
   * @throws RemoteException
   *           Une erreur de communication s'est produite
   */
  public long getServerTimeMillis() throws RemoteException;
} // /:~
