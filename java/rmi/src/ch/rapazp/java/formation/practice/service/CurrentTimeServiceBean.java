package ch.rapazp.java.formation.practice.service;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * CurrentTimeServiceBean.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 24.06.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.rmi.RemoteException;

/**
 * Implementation du service pour la recuperation de l'heure du serveur.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class CurrentTimeServiceBean implements CurrentTimeService {

  /*
   * (non-Javadoc)
   * 
   * @see ch.rp.rmi.service.CurrentTimeService#getServerTimeMillis()
   */
  public long getServerTimeMillis() throws RemoteException {

    return System.currentTimeMillis();
  }
} // /:~
