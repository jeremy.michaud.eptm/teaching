package ch.rapazp.java.formation.practice.client;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * CurrentTimeClient.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 24.06.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Date;

import ch.rapazp.java.formation.practice.service.CurrentTimeService;

/**
 * Client charge d'appeler le methodes distantes
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class CurrentTimeClient {

  public static void main(String args[]) {

    new CurrentTimeClient();
  } // end main

  public CurrentTimeClient() {

    CurrentTimeService currentServerTime;
    Date serverTime = null;

    try {
      currentServerTime = (CurrentTimeService) Naming.lookup("rmi:///currentTimeService");

      serverTime = new Date(currentServerTime.getServerTimeMillis());

      System.out.println(serverTime);
    } catch (MalformedURLException murle) {

      System.out.println();
      System.out.println("MalformedURLException");
      System.out.println(murle);
    } catch (RemoteException re) {

      System.out.println();
      System.out.println("RemoteException");
      System.out.println(re);
    } catch (NotBoundException nbe) {

      System.out.println();
      System.out.println("NotBoundException");
      System.out.println(nbe);
    } catch (java.lang.ArithmeticException ae) {

      System.out.println();
      System.out.println("ArithmeticException");
      System.out.println(ae);
    } // end try
  } // end CurrentTimeClient
} // /:~
