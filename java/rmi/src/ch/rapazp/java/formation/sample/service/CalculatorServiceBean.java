package ch.rapazp.java.formation.sample.service;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * CalculatorServiceBean.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 19.06.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.rmi.RemoteException;

/**
 * Implementation du service de calcul.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class CalculatorServiceBean implements CalculatorService {

  /*
   * (non-Javadoc)
   * 
   * @see ch.rp.rmi.service.CalculatorService#add(double, double)
   */
  public double add(double a, double b) throws RemoteException {

    return a + b;
  }

  /*
   * (non-Javadoc)
   * 
   * @see ch.rp.rmi.service.CalculatorService#sub(double, double)
   */
  public double sub(double a, double b) throws RemoteException {

    return a - b;
  }

  /*
   * (non-Javadoc)
   * 
   * @see ch.rp.rmi.service.CalculatorService#mul(double, double)
   */
  public double mul(double a, double b) throws RemoteException {

    return a * b;
  }

  /*
   * (non-Javadoc)
   * 
   * @see ch.rp.rmi.service.CalculatorService#div(double, double)
   */
  public double div(double a, double b) throws RemoteException {

    return a / b;
  }
} // /:~
