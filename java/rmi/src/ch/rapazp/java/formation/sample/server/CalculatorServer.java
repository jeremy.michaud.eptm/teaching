package ch.rapazp.java.formation.sample.server;

/* -----------------------------------------------------------------------------
 * (c) 2014 by Pascal Rapaz
 * -----------------------------------------------------------------------------
 *
 * CalculatorServer.java
 *
 * -----------------------------------------------------------------------------
 * WHEN           VERSION   DESCRIPTION
 * 19.06.2014     1.0       Creation
 * -----------------------------------------------------------------------------
 */

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import ch.rapazp.java.formation.sample.service.CalculatorService;
import ch.rapazp.java.formation.sample.service.CalculatorServiceBean;

/**
 * Implementation du serveur RMI.
 * 
 * @author Pascal Rapaz
 * @version 1.0
 */
public class CalculatorServer {

  public static void main(String args[]) {

    new CalculatorServer();
  } // endMain

  public CalculatorServer() {

    try {

      CalculatorService calculatorServiceBean = new CalculatorServiceBean();
      CalculatorService calculatorService = (CalculatorService) UnicastRemoteObject.exportObject(
          calculatorServiceBean, 0);

      Registry registry = LocateRegistry.getRegistry();
      registry.bind("calculatorService", calculatorService);
    } catch (Exception e) {

      e.printStackTrace();
      System.exit(1);
    } // endTry
  } // endConst
} // /:~