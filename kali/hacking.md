# Ethical Hacking <!-- omit in toc -->

## Table des matières <!-- omit in toc -->
- [Vocabulaire](#vocabulaire)
- [Empreintes](#empreintes)
- [Enumération](#enum%c3%a9ration)
- [Network scanning](#network-scanning)
- [ARP attack](#arp-attack)
- [Crack](#crack)
- [Session Hijacking](#session-hijacking)
- [Wifi](#wifi)
- [Mobile](#mobile)

## Vocabulaire
* Hack Value : statisfaction d'entrer dans un réseau
* Exploit : utilisationd es vulnérabilité
* Payload : partie du code qui va réaliser l'action
* Zero-Day : attaque de failles non patchées
* Doxing : utilisation des données personnelles
* Spoofing : prendre la place de, usurper l'identité
* Chaînage : réplication des soucis, des accès, pour aller plus loin

## Empreintes
Collecter les infos sur le réseau : sublist3r
* passif -> pas d'action sur la cible
* actif -> scannage

## Enumération
Attaque sur les intranets : hyena

## Network scanning
1. scannage réseau : nmap
2. scannage de port et flooding : hping3
3. scan ids and firewall : Burp Suite, Tor, Zap
4. Banner grabbing : dmitry
5. Dessiner carte réseau : the dude
6. Document les découvertes

## ARP attack
1. mac flooding : yersinia
2. dhcp starvation : scapy
3. rogue server (faux serveur)
4. arp poisonig : etercap
5. mac spoofig : tmac

## Crack
* Mot de passe
  * Dictionnaire : https://crackstation.net/
  * Brute Force : john the ripper, hydra, medusa, cain & abel
  * Attaque basée sur des règles: 9 chars, 1 majuscule, 4 chiffres, etc.

## Session Hijacking
Voler le jeton d'authentification

## Wifi
* Evil Twin

## Mobile
* Hackode