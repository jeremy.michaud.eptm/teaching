<!--
Copyright (C) 2019-2020 - EPTM (École professionnelle technique et des métiers) - [Pascal Rapaz](mailto:pascal.rapaz@eptm.ch)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

A copy of the license is agreement is available at [GNU
Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.txt)".
-->

# Formation Kali Linux

Ce projet contient un ensemble de documents et exercices pour l'utilisation de Kali Linux
* [Support de cours](https://gitlab.com/pascal.rapaz/teaching/blob/master/cheatsheet/kali.md)
